"""
PROGRAM:
blender_gears_planet_spur.py

Copyright (C) 2024 Anders B. Thlenk

LICENSE:
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.


DESCRIPTION:
Utilize Blender <https://www.blender.org/> to auto generate a planet gears based on spur gears given various user inputs.  Refer to the "User Inputs" section of main() below.

"""

import bpy
import bmesh
import math
import mathutils

#########################################################
#########  Just error boxes
#########################################################

def e_mode_set(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with mode_set.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

def e_deslection(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with deselection.'
    
    self.layout.label(text=message, icon='INFO')
    
    return
    
def e_pg1_fr(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Condition: s_sun/s_carrier > 2 not met.'
    
    self.layout.label(text=message, icon='INFO')
    
    return
    
def e_pg1_fs(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Condition: 1 < s_ring/s_carrier< 2 not met.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

def e_pg1_nf(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Condition: Need either sun or ring fixed (-1)'
    
    self.layout.label(text=message, icon='INFO')
    
    return

def e_pg2_fs(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Condition: s_ring/s_carrier< 1 not met.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

def e_pg2_nf(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Condition: Need either sun or ring fixed (-1).'
    
    self.layout.label(text=message, icon='INFO')
    
    return

def e_pg_n12(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Condition: Planet Gear Number 1 or 2 not met.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

def e_rp_intf_chk(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Condition: N_p_act < N_p_min not met.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

#########################################################
#########  Starting Gear Stuff
#########################################################

def planetPossibleArms(r_s_p,r_p1_p,r_p1_a,N_s,N_r,npg):
    """
    
    """
    # Cosine of separation angle
    acos= ( 2.*(r_s_p+r_p1_p)*(r_s_p+r_p1_p) - 4.*r_p1_a*r_p1_a ) \
            / ( 2.*(r_s_p+r_p1_p)*(r_s_p+r_p1_p) )
    
    # This is the minimum separation angle to not touch two planet gears
    separation_angle= math.acos(acos)
    
    # This is the maximum possible planet gears which can fit in btw ring and sun
    max_arms= int(math.floor(2.*math.pi/separation_angle))
    
    # These are the angles from the center for one circular pitch
    angle_s_cp= 2.*math.pi/N_s
    angle_r_cp= 2.*math.pi/N_r
    
    # Factor to check for integral rotations of gears
    if npg == 1:
        factor= 1./angle_s_cp + 1/angle_r_cp
    else:
        factor= 1./angle_s_cp - 1/angle_r_cp
    
    # Factor for planet rotation based on known sun rotation
    rot_factor= angle_s_cp * r_s_p / r_p1_p
    
    # List providing information on possible arm arrangements
    lst_arms= []
    
    # Now for the checking
    for i in range(2,max_arms+1):
        # If this is an integer, this number of arms is possible
        valid= ( round(2.*math.pi*factor/i, 6) ).is_integer()
        
        if valid:
            angle= 2.*math.pi/i
            
            angles= []
            rotations= []
            
            for j in range(1,i):
                 curr_angle= j*angle
                 
                 angles.append(curr_angle * 180. / math.pi)
                 
                 dum= curr_angle/angle_s_cp
                 
                 rotation= (dum - math.floor(dum)) * rot_factor *180./math.pi
                 
                 rotations.append(rotation)
                 
            lst_arms.append( (i, angles, rotations) )     

    return lst_arms

def N_in_minEq(k,sr,pa):
    """ Determine the minimum number of teeth on input gear
    
        Inputs:
            k = Addendum factor on m for input gear (normally 1)
            sr = Speed Ratio (w.output/w.input)
            pa = Pressure Angle (degrees)
            
        Output:
            Minimum number of teeth to avoid interference with base circle
    """
    
    pa_rad = pa*math.pi/180.
    
    fac1 = 2.*k*sr / ((1.+2.*sr)*math.sin(pa_rad)*math.sin(pa_rad))
    fac2 = sr + math.sqrt(sr*sr + (1.+2.*sr)*math.sin(pa_rad)*math.sin(pa_rad))
    
    return fac1*fac2


def N_out_minEq(k,sr,pa):
    """ Determine the minimum number of teeth on output gear
    
        Inputs:
            k = Addendum factor on m for output gear (normally 1)
            sr = Speed Ratio (w.output/w.input)
            pa = Pressure Angle (degrees)
            
        Output:
            Minimum number of teeth to avoid interference with base circle
    """
    
    pa_rad = pa*math.pi/180.
    
    fac1 = 2.*k / ((2.+ sr)*sr*math.sin(pa_rad)*math.sin(pa_rad))
    fac2 = 1. + math.sqrt(1. + (2.+sr)*sr*math.sin(pa_rad)*math.sin(pa_rad))
    
    return fac1*fac2

def interfere_N_in_min(k_in,k_out,sr,pa):
    """ Determine the minimum number of teeth on input gear taking into 
        account the speed ratio and minimum output gear teeth required
    
        Inputs:
            k_in  = Addendum factor on m for input gear (normally 1)
            k_out = Addendum factor on m for output gear (normally 1)
            sr    = Speed Ration(w.output/w.input)
            pa    = Pressure Angle (degrees)
            
        Output:
            Minimum number of teeth to avoid interference with base circle
    """
    
    return max(N_in_minEq(k_in,sr,pa),sr*N_out_minEq(k_out,sr,pa))


def interfere_N_out_min(k_in,k_out,sr,pa):
    """ Determine the minimum number of teeth on output gear taking into
        account the speed ratio and minimum input gear teeth required
    
        Inputs:
            k_in  = Addendum factor on m for input gear (normally 1)
            k_out = Addendum factor on m for output gear (normally 1)
            sr    = Speed Ration(w.output/w.input)
            pa    = Pressure Angle (degrees)
            
        Output:
            Minimum number of teeth to avoid interference with base circle
    """
    
    return max(N_out_minEq(k_out,sr,pa),N_in_minEq(k_in,sr,pa)/sr)

def contactRatioEq(sr,pa,k_in,k_out,N_in):
    """ Calcuate the value for the contact ratio based on the inlet
        gear.  
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (degrees)
            cr      = Required contact ratio (dimensionless)
            k_in    = Addendum factor on m for input gear (normally 1)
            k_out   = Addendum factor on m for output gear (normally 1)
            N_in    = Number of teeth on the input gear
            
        Output:
            The contact ratio
    """

    pa_rad = pa*math.pi/180.
    
    fac1 = 0.25*N_in*N_in*math.sin(pa_rad)*math.sin(pa_rad) + N_in*k_in + k_in*k_in
    fac2 = 0.25*N_in*N_in*math.sin(pa_rad)*math.sin(pa_rad)/(sr*sr) \
            + N_in*k_out/sr+k_out*k_out
    
    return (math.sqrt(fac1)+math.sqrt(fac2)-0.5*math.sin(pa_rad)*N_in*(sr+1.)/sr) \
            / (math.pi*math.cos(pa_rad))

def cr_Nin_zeroFn(sr,pa,cr,k_in,k_out,N_in):
    """ Calcuate the value for the contact ratio zero function for the inlet
        gear.  This is the error in the solution.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (degrees)
            cr      = Required contact ratio (dimensionless)
            k_in    = Addendum factor on m for input gear (normally 1)
            k_out   = Addendum factor on m for output gear (normally 1)
            N_in    = Number of teeth on the input gear
            
        Output:
            Value of zero contact ratio function
    """

    pa_rad = pa*math.pi/180.
    
    fac1 = 0.25*N_in*N_in*math.sin(pa_rad)*math.sin(pa_rad)+N_in*k_in+k_in*k_in
    fac2 = 0.25*N_in*N_in*math.sin(pa_rad)*math.sin(pa_rad)/(sr*sr) \
            + N_in*k_out/sr+k_out*k_out
    
    return math.sqrt(fac1) + math.sqrt(fac2) - cr*math.pi*math.cos(pa_rad) \
            -0.5*math.sin(pa_rad)*N_in*(sr+1.)/sr


def cr_Nin_zeroFnPrime(sr,pa,cr,k_in,k_out,N_in):
    """ Calcuate the value for the contact ratio zero function first
        derivative for the inlet gear.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (degrees)
            cr      = Required contact ratio (dimensionless)
            k_in    = Addendum factor on m for input gear (normally 1)
            k_out   = Addendum factor on m for output gear (normally 1)
            N_in    = Number of teeth on the input gear
            
        Output:
            Value of zero contact ratio function first derivative
    """

    pa_rad = pa*math.pi/180.
    
    fac1 = 0.25*N_in*N_in*math.sin(pa_rad)*math.sin(pa_rad) + N_in*k_in + k_in*k_in
    fac2 = 0.25*N_in*N_in*math.sin(pa_rad)*math.sin(pa_rad)/(sr*sr) \
            + N_in*k_out/sr + k_out*k_out
    
    fac3 = (0.5*math.sin(pa_rad)*math.sin(pa_rad)*N_in + k_in)/math.sqrt(fac1)
    fac4 = (0.5*math.sin(pa_rad)*math.sin(pa_rad)*N_in/(sr*sr) + k_in/sr) \
            /math.sqrt(fac2)
    
    return fac3 + fac4 -(sr+1.)*math.sin(pa_rad)/sr

def cr_N_in_min(sr,pa,cr,k_in,k_out,tol_rel,N_in):
    """ Solve for the required number of input gear teeth to meet input 
        parameters using Newton's Method.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (degrees)
            cr      = Required contact ratio (dimensionless)
            k_in    = Addendum factor on m for input gear (normally 1)
            k_out   = Addendum factor on m for output gear (normally 1)
            tol_rel = Relative tolerance (percent)
            N_in    = Iniital guess for number of teeth
            
        Output:
            N_in    = Actual number of teeth to meet indicated conditions
    """

    for i in range(0,100):
        err_rel = cr_Nin_zeroFn(sr,pa,cr,k_in,k_out,N_in) \
                    / (N_in*cr_Nin_zeroFnPrime(sr,pa,cr,k_in,k_out,N_in))
        
        if(math.fabs(err_rel)<(tol_rel/100.)):
            return N_in
            
        N_in *= (1. - err_rel)

    return -10

def cr_Nout_zeroFn(sr,pa,cr,k_in,k_out,N_out):
    """ Calcuate the value for the contact ratio zero function for the outlet
        gear.  This is the error in the solution.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (degrees)
            cr      = Required contact ratio (dimensionless)
            k_in    = Addendum factor on m for input gear (normally 1)
            k_out   = Addendum factor on m for output gear (normally 1)
            N_out   = Number of teeth on the output gear
            
        Output:
            Value of zero contact ratio function
    """

    pa_rad = pa*math.pi/180.
    
    fac1 = 0.25*N_out*N_out*math.sin(pa_rad)*math.sin(pa_rad)*sr*sr \
            + sr*N_out*k_in + k_in*k_in
    fac2 = 0.25*N_out*N_out*math.sin(pa_rad)*math.sin(pa_rad) \
            + N_out*k_out + k_out*k_out
    
    return math.sqrt(fac1) + math.sqrt(fac2) - cr*math.pi*math.cos(pa_rad) \
            - 0.5*math.sin(pa_rad)*N_out*(sr+1.)

def cr_Nout_zeroFnPrime(sr,pa,cr,k_in,k_out,N_out):
    """ Calcuate the value for the contact ratio zero function derivative 
        for the outlet gear.  This is the error in the solution.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (degrees)
            cr      = Required contact ratio (dimensionless)
            k_in    = Addendum factor on m for input gear (normally 1)
            k_out   = Addendum factor on m for output gear (normally 1)
            N_out   = Number of teeth on the output gear
            
        Output:
            Value of zero contact ratio function derivative
    """

    pa_rad = pa*math.pi/180.
    
    fac1 = 0.25*N_out*N_out*math.sin(pa_rad)*math.sin(pa_rad)*sr*sr \
            + sr*N_out*k_in + k_in*k_in
    fac2 = 0.25*N_out*N_out*math.sin(pa_rad)*math.sin(pa_rad) \
            + N_out*k_out + k_out*k_out
    
    fac3 = (0.5*sr*sr*math.sin(pa_rad)*math.sin(pa_rad)*N_out + sr*k_in) \
            / math.sqrt(fac1)
    fac4 = (0.5*math.sin(pa_rad)*math.sin(pa_rad)*N_out + k_out) / math.sqrt(fac2)

    return fac3 + fac4 - (sr+1.)*math.sin(pa_rad)

def cr_N_out_min(sr,pa,cr,k_in,k_out,tol_rel,N_out):
    """ Solve for the required number of input gear teeth to meet input 
        parameters using Newton's Method.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (degrees)
            cr      = Required contact ratio (dimensionless)
            k_in    = Addendum factor on m for input gear (normally 1)
            k_out   = Addendum factor on m for output gear (normally 1)
            tol_rel = Relative tolerance (percent)
            N_out   = Iniital guess for number of teeth
            
        Output:
            N_out   = Actual number of teeth to meet indicated conditions
    """

    for i in range(0,100):
        err_rel = cr_Nout_zeroFn(sr,pa,cr,k_in,k_out,N_out) \
                    / (N_out*cr_Nout_zeroFnPrime(sr,pa,cr,k_in,k_out,N_out))
        
        if(math.fabs(err_rel)<(tol_rel/100.)):
            return N_out
            
        N_out *= (1. - err_rel)

    return -10

def contactRatio_N_in_min(sr,pa,cr,k_in,k_out,tol_rel):
    """ Solve for the required number of input gear teeth to meet input 
        parameters using Newton's Method.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (degrees)
            cr      = Required contact ratio (dimensionless)
            k_in    = Addendum factor on m for input gear (normally 1)
            k_out   = Addendum factor on m for output gear (normally 1)
            tol_rel = Relative tolerance (percent)
            
        Output:
            Minimum number of input gear teeth to meet contact ratio input
    """
    return max(cr_N_in_min(sr,pa,cr,k_in,k_out,tol_rel,2.), \
                    cr_N_out_min(sr,pa,cr,k_in,k_out,tol_rel,2.)*sr)

def internalGearInterferenceCheck1(pa,N_i,k_i):
    
    num= N_i*N_i*math.sin(pa)*math.sin(pa)-4.*N_i*k_i+4.*k_i*k_i
    
    return N_i - math.sqrt(num)/math.sin(pa)


def generateExteriorGearBlender(N,x,y,thk,fill,name):
    """ Generate a blender object for this one gear
    
        Inputs:
            N = Number of teeth for gear
            x = List of x-coordinate data for one tooth
            y = List of y-coordinate data for one tooth
            thk = If filled, provide some thickness. -1 if just keep as a plane
            fill = Fill in the gear, boolean
            name = String for Blender object name of gear
            
        Output:
            A Blender object:
            Float: Number of vertex points on the gear.
    """

    # Blender mesh name
    name_msh=   name+"_msh"
    
    # Make a new BMesh
    bm_gear= bmesh.new()
    
    # Create the single tooth outline
    ret=    bmesh.ops.create_vert(
                bm_gear,
                co= (x[0],y[0],0.0)
            )
            
    vert_pair= ret['vert']
    
    # Make edges as the new vertics are added
    for i in range(1,len(x)):
        ret=    bmesh.ops.create_vert(
                    bm_gear,
                    co= (x[i],y[i],0.0)
                )
        
        vert_pair.extend(ret['vert'])
        
        bmesh.ops.contextual_create(
            bm_gear,
            geom=       vert_pair,
            use_smooth= False
            )
            
        vert_pair= [vert_pair[-1]]

    del ret
    
    # Now remove doubles and merge ends to make loops
    bmesh.ops.remove_doubles(
        bm_gear,
        verts=  bm_gear.verts[:],
        dist=   0.0001
    )

    # Spin the tooth to make the gear
    ret = bmesh.ops.spin(
        bm_gear,
        geom=           bm_gear.verts[:] + bm_gear.edges[:],
        angle=          2.*math.pi*(N-1.)/N,
        steps=          int(N-1),
        axis=           (0.0, 0.0, 1.0),
        cent=           (0.0, 0.0, 0.0),
        dvec=           (0.0, 0.0, 0.0),
        use_merge=      True,
        use_duplicate=  True 
        )

    del ret
    
    # Now remove doubles and merge ends to make loops
    bmesh.ops.remove_doubles(
        bm_gear,
        verts=  bm_gear.verts[:],
        dist=   0.001)
   
    if fill:
        # Enclose the gear outline with a face
        bmesh.ops.contextual_create(
            bm_gear,
            geom=       bm_gear.edges[:],
            use_smooth= False
            )

        if thk>0.:
            # Extrude the face the required thickness
            ret= bmesh.ops.extrude_face_region(
                    bm_gear,
                    geom = bm_gear.verts[:] \
                            + bm_gear.edges[:] \
                            + bm_gear.faces[:],
                    edges_exclude=              set(),
                    use_keep_orig=              False,
                    use_normal_flip=            False,
                    use_normal_from_adjacent=   False,
                    use_dissolve_ortho_edges=   False,
                    use_select_history=         False
                    )
                
            geom_hold= ret['geom']
            del ret
            
            # Extract the extruded vertices
            extrude_verts= [ele for ele in geom_hold if \
                                    isinstance(ele, bmesh.types.BMVert)]

            del geom_hold

            bmesh.ops.translate(
                bm_gear,
                vec=    (0.0,0.0,thk),
                verts=  extrude_verts
                )

    # Flip all the face normal to point outside of the gear
    bmesh.ops.reverse_faces(
        bm_gear,
        faces=          bm_gear.faces[:],
        flip_multires=  False
        )
    
    vertex_count= len(bm_gear.verts[:])
    
    # Finish up, write the bmesh into a new mesh
    msh_gear= bpy.data.meshes.new(name_msh)
    bm_gear.to_mesh(msh_gear)
    bm_gear.free()

    # Add the mesh to the scene
    obj_gear= bpy.data.objects.new(name, msh_gear)
    bpy.context.collection.objects.link(obj_gear)

    return vertex_count

def modify_edgeloop(edgeloop_verts, head_type, r_head):

    if head_type == 'Box':
        ref_vertices= [
                        (r_head,r_head), 
                        (-r_head,r_head), 
                        (-r_head,-r_head), 
                        (r_head,-r_head)
                        ]
        
        for x, y in ref_vertices:
            # Reset the goal vertex and the minimum distance
            min_vert= None
            min_dist= 1e+20

            # Loop through the vertices to find the closest vertex
            for vert in edgeloop_verts:
                v_x=    vert.co.x
                v_y=    vert.co.y
                
                # Get distance from current vertext to the goal vertex
                dist= math.sqrt((x-v_x)*(x-v_x)+(y-v_y)*(y-v_y))
                
                # If this is the current minimum distance update distance and min vertex
                if dist<min_dist:
                    min_dist= dist
                    min_vert= vert
            
            # Make the min vertex equal to the goal vertex
            min_vert.co.x= x
            min_vert.co.y= y
        
        # Now that the fixed ponts are set, update all points based on geometry limits
        # For a box
        for vert in edgeloop_verts:
            v_x=    vert.co.x
            v_y=    vert.co.y
            angle= math.atan2(v_y,v_x)
            
            if angle>=-0.25*math.pi and angle<=0.0:
                vert.co.x=  r_head
                vert.co.y=  r_head * math.tan(angle)
            
            elif angle>0. and angle<0.25*math.pi:
                vert.co.x=  r_head
                vert.co.y=  r_head * math.tan(angle)
            
            elif angle>=0.25*math.pi and angle<0.5*math.pi:
                vert.co.y=  r_head
                vert.co.x=  r_head / math.tan(angle)
            
            elif angle>=0.5*math.pi and angle<0.75*math.pi:
                vert.co.y=  r_head
                vert.co.x=  r_head / math.tan(angle)
            
            elif angle>=0.75*math.pi and angle<=math.pi:
                vert.co.x=  -r_head
                vert.co.y=  -r_head * math.tan(angle)
            
            elif angle>-math.pi and angle<-0.75*math.pi:
                vert.co.x=  -r_head
                vert.co.y=  -r_head * math.tan(angle)
            
            elif angle>=-0.75*math.pi and angle<-0.5*math.pi:
                vert.co.y=  -r_head
                vert.co.x=  -r_head / math.tan(angle)
            
            else:
                vert.co.y=  -r_head
                vert.co.x=  -r_head / math.tan(angle)

    elif head_type == 'Oct': # Octagon housing    
        base_angle= math.pi/8.
        
        R= r_head
        A= R * math.tan(base_angle)
        
        ref_vertices= [ (R, A),
                        (A, R),
                        (-A, R),
                        (-R, A),
                        (-R, -A),
                        (-A, -R),
                        (A, -R),
                        (R, -A) 
                    ]
    
        for x, y in ref_vertices:
            # Reset the goal vertex and the minimum distance
            min_vert= None
            min_dist= 1e+20

            # Loop through the vertices to find the closest vertex
            for vert in edgeloop_verts:
                v_x=    vert.co.x
                v_y=    vert.co.y
                
                # Get distance from current vertext to the goal vertex
                dist= math.sqrt((x-v_x)*(x-v_x)+(y-v_y)*(y-v_y))
                
                # If this is the current minimum distance update distance and min vertex
                if dist<min_dist:
                    min_dist= dist
                    min_vert= vert
            
            # Make the min vertex equal to the goal vertex
            min_vert.co.x= x
            min_vert.co.y= y

        # Now that the fixed ponts are set, update all points based on geometry limits
        # For an Octagon
        
        for vert in edgeloop_verts:
            v_x=    vert.co.x
            v_y=    vert.co.y
            angle= math.atan2(v_y,v_x)
            
            if angle>=-base_angle and angle<base_angle:
                vert.co.x=  R
                vert.co.y=  R * math.tan(angle)
            
            elif angle>=base_angle and angle<3.*base_angle:
                vert.co.x= (R+A) / (1. + math.tan(angle) )
                vert.co.y= math.tan(angle) * vert.co.x
            
            elif angle>=3.*base_angle and angle<0.5*math.pi:
                vert.co.y=  R
                vert.co.x=  R / math.tan(angle)
            
            elif angle>=0.5*math.pi and angle<5.*base_angle:
                vert.co.y=  R
                vert.co.x=  R / math.tan(angle)
            
            elif angle>=5.*base_angle and angle<7.*base_angle:
                vert.co.x= (R+A) / ( math.tan(angle) - 1. )
                vert.co.y= math.tan(angle) * vert.co.x
            
            elif angle>7.*base_angle and angle<=math.pi:
                vert.co.x=  -R
                vert.co.y=  -R * math.tan(angle)
                
            elif angle>-math.pi and angle<-7*base_angle:
                vert.co.x=  -R
                vert.co.y=  -R * math.tan(angle)
                
            elif angle>=-7.*base_angle and angle<-5.*base_angle:
                vert.co.x= -(R+A)/(1.+math.tan(angle))                
                vert.co.y= math.tan(angle) * vert.co.x
                
            elif angle>=-5.*base_angle and angle<-0.5*math.pi:
                vert.co.y= -R
                vert.co.x= -R / math.tan(angle)
                
            elif angle>=-0.5*math.pi and angle<-3.*base_angle:
                vert.co.y= -R
                vert.co.x= -R / math.tan(angle)                

            else:
                vert.co.x= (R+A)/(1.-math.tan(angle))
                vert.co.y= math.tan(angle) * vert.co.x

    else: # Pdestal (i.e, Octagon top and box bottom)
        base_angle= math.pi/8.
        
        R= r_head
        A= R * math.tan(base_angle)
        
        ref_vertices= [ (R, A),
                        (A, R),
                        (-A, R),
                        (-R, A),
                        (-R, -R),
                        (R, -R) 
                    ]
    
        for x, y in ref_vertices:
            # Reset the goal vertex and the minimum distance
            min_vert= None
            min_dist= 1e+20

            # Loop through the vertices to find the closest vertex
            for vert in edgeloop_verts:
                v_x=    vert.co.x
                v_y=    vert.co.y
                
                # Get distance from current vertext to the goal vertex
                dist= math.sqrt((x-v_x)*(x-v_x)+(y-v_y)*(y-v_y))
                
                # If this is the current minimum distance update distance and min vertex
                if dist<min_dist:
                    min_dist= dist
                    min_vert= vert
            
            # Make the min vertex equal to the goal vertex
            min_vert.co.x= x
            min_vert.co.y= y

        # Now that the fixed ponts are set, update all points based on geometry limits
        # For a Pedestal
        
        for vert in edgeloop_verts:
            v_x=    vert.co.x
            v_y=    vert.co.y
            angle= math.atan2(v_y,v_x)
            
            if angle>=-0.25*math.pi and angle<base_angle:
                vert.co.x=  R
                vert.co.y=  R * math.tan(angle)

            elif angle>=base_angle and angle<3.*base_angle:
                vert.co.x= (R+A) / (1. + math.tan(angle) )
                vert.co.y= math.tan(angle) * vert.co.x
            
            elif angle>=3.*base_angle and angle<0.5*math.pi:
                vert.co.y=  R
                vert.co.x=  R / math.tan(angle)
            
            elif angle>=0.5*math.pi and angle<5.*base_angle:
                vert.co.y=  R
                vert.co.x=  R / math.tan(angle)
            
            elif angle>=5.*base_angle and angle<7.*base_angle:
                vert.co.x= (R+A) / ( math.tan(angle) - 1. )
                vert.co.y= math.tan(angle) * vert.co.x
            
            elif angle>7.*base_angle and angle<=math.pi:
                vert.co.x=  -R
                vert.co.y=  -R * math.tan(angle)

            elif angle>-math.pi and angle<-0.75*math.pi:
                vert.co.x=  -R
                vert.co.y=  -R * math.tan(angle)
            
            elif angle>=-0.75*math.pi and angle<-0.5*math.pi:
                vert.co.y=  -R
                vert.co.x=  -R / math.tan(angle)
            
            else:
                vert.co.y=  -R
                vert.co.x=  -R / math.tan(angle)

    return ref_vertices

def generateInteriorGearBlender(N,x,y,r_d,r_a,thk_tooth,thk_base, \
                                thk_edge,thk_inner,d_edge_user,r_shape, \
                                generate_blank,blank_gap,d_ri_user, \
                                thk_inset,h_inset,dh_tooth_top,name):
    """
    
    
    """
    # Determine the inset radius
    if thk_inset < 0.:   
        r_inset= r_d
        dh_inset= 0.
    else:
        r_inset= r_d + thk_inset
        dh_inset= h_inset
    
    if dh_tooth_top < 0.:
        dh_tooth_top= 0.0
    
    # The outer edge radius
    r_edge= r_inset + thk_edge
    
    # Revise r_or_od to be an even multiple of 0.2 or user value
    # Whichever is greater
    r_edge= math.ceil(10.*r_edge)
    
    if r_edge%2==0:
        r_edge /= 10.
    else:
        r_edge= (r_edge+1.)/10.
        
    r_edge= max(r_edge,0.5*d_edge_user)
    
    # The inner edge radius
    if thk_inner<0:
        r_inner= -1.
    else:
        r_inner= max(min(r_a-thk_inner,0.5*d_ri_user),1)
    
    # Blender mesh name
    name_msh=   name+"_msh"
    
    # Make a new BMesh
    bm_gear= bmesh.new()
    
    # Hold vertices for modification on outer edge if needed
    modify_verts= []
    
    # Create the single tooth outline
    ret=    bmesh.ops.create_vert(
                bm_gear,
                co= (x[0],y[0],0.0)
            )
            
    vert_pair= ret['vert']
    
    # Make edges as the new vertics are added
    for i in range(1,len(x)):
        ret=    bmesh.ops.create_vert(
                    bm_gear,
                    co= (x[i],y[i],0.0)
                )
        
        vert_pair.extend(ret['vert'])
        
        bmesh.ops.contextual_create(
            bm_gear,
            geom=       vert_pair,
            use_smooth= False
            )
            
        vert_pair= [vert_pair[-1]]

    del ret
    
    # Spin the tooth to make the gear
    ret = bmesh.ops.spin(
        bm_gear,
        geom=           bm_gear.verts[:] + bm_gear.edges[:],
        angle=          2.*math.pi*(N-1.)/N,
        steps=          int(N-1),
        axis=           (0.0, 0.0, 1.0),
        cent=           (0.0, 0.0, 0.0),
        dvec=           (0.0, 0.0, 0.0),
        use_merge=      True,
        use_duplicate=  True 
        )

    del ret
    
    # Now remove doubles and merge ends to make loops
    bmesh.ops.remove_doubles(
        bm_gear,
        verts=  bm_gear.verts[:],
        dist=   0.001)
   
    #################################################################
    ########### Complete with generating interior gear outline  #####
    #################################################################
    
    # Store the edges to perform the final closure
    edges_base = bm_gear.edges[:]
    
    # Extrude edges the thickness of the tooth
    ret = bmesh.ops.extrude_edge_only(
            bm_gear,
            edges=  edges_base
            )
            
    geom_hold = ret["geom"]
    del ret

    # Extract the extruded edges and vertices
    verts_2 = [ele for ele in geom_hold if isinstance(ele, bmesh.types.BMVert)]
    edges_2 = [ele for ele in geom_hold if isinstance(ele, \
                            bmesh.types.BMEdge) and ele.is_boundary]

    del geom_hold

    # Translate these edges upward the thickness of the tooth
    bmesh.ops.translate(
        bm_gear,
        vec = (0.0,0.0,thk_tooth),
        verts = verts_2
        )
    
    if dh_tooth_top > 0.:
        # Extrude to top tooth outline vertically 
        ret = bmesh.ops.extrude_edge_only(
                bm_gear,
                edges=  edges_2
                )
                
        geom_hold = ret["geom"]
        
        del ret
        del edges_2

        # Extract the extruded edges and vertices
        verts_2 = [ele for ele in geom_hold if isinstance(ele, bmesh.types.BMVert)]
        edges_2 = [ele for ele in geom_hold if isinstance(ele, \
                                bmesh.types.BMEdge) and ele.is_boundary]

        del geom_hold
        
        # Translate these edges the indicated height
        bmesh.ops.translate(
            bm_gear,
            vec = (0.0,0.0,dh_tooth_top),
            verts = verts_2
            )
            
        # Clean up a litle
        del verts_2
    
    if thk_inset >= 0.: 
        # Generate an inset before extruding to outer edge
        ret = bmesh.ops.extrude_edge_only(
                bm_gear,
                edges=  edges_2
                )
                
        geom_hold = ret["geom"]
        del ret
        del edges_2

        # Extract the extruded edges and vertices
        verts_3 = [ele for ele in geom_hold if isinstance(ele, bmesh.types.BMVert)]
        edges_3 = [ele for ele in geom_hold if isinstance(ele, \
                                bmesh.types.BMEdge) and ele.is_boundary]

        del geom_hold
        
        # Take each vertex and extend to outer circle at same angle
        for vert in verts_3:
            angle= math.atan2(vert.co.y,vert.co.x)
            vert.co.x= r_inset*math.cos(angle)
            vert.co.y= r_inset*math.sin(angle)
        
        # Extrude to the inset circle the indicated height
        ret = bmesh.ops.extrude_edge_only(
                bm_gear,
                edges=  edges_3
                )
                
        geom_hold = ret["geom"]
        
        del ret
        del verts_3
        del edges_3

        # Extract the extruded edges and vertices
        verts_4 = [ele for ele in geom_hold if isinstance(ele, bmesh.types.BMVert)]
        edges_4 = [ele for ele in geom_hold if isinstance(ele, \
                                bmesh.types.BMEdge) and ele.is_boundary]

        del geom_hold
        
        # Translate these edges the indicated height
        bmesh.ops.translate(
            bm_gear,
            vec = (0.0,0.0,dh_inset),
            verts = verts_4
            )
            
        # Redefine edges_2 to continue
        edges_2= edges_4
        del verts_4
    
    # Extrude to the outer edge
    ret = bmesh.ops.extrude_edge_only(
            bm_gear,
            edges=  edges_2
            )
            
    geom_hold = ret["geom"]
    del ret
    #del verts_2
    del edges_2

    # Extract the extruded edges and vertices
    verts_3 = [ele for ele in geom_hold if isinstance(ele, bmesh.types.BMVert)]
    edges_3 = [ele for ele in geom_hold if isinstance(ele, \
                            bmesh.types.BMEdge) and ele.is_boundary]

    del geom_hold
    
    # Take each vertex and extend to outer circle at same angle
    for vert in verts_3:
        angle= math.atan2(vert.co.y,vert.co.x)
        vert.co.x= r_edge*math.cos(angle)
        vert.co.y= r_edge*math.sin(angle)
    
    # Store these vertices for potential modification
    modify_verts.append(verts_3)
    
    # Extrude to the outer circle to the bottom of the base
    ret = bmesh.ops.extrude_edge_only(
            bm_gear,
            edges=  edges_3
            )
            
    geom_hold = ret["geom"]
    
    del ret
    del verts_3
    del edges_3

    # Extract the extruded edges and vertices
    verts_4 = [ele for ele in geom_hold if isinstance(ele, bmesh.types.BMVert)]
    edges_4 = [ele for ele in geom_hold if isinstance(ele, \
                            bmesh.types.BMEdge) and ele.is_boundary]

    del geom_hold
    
    # Translate these edges down to the bottom of the base
    bmesh.ops.translate(
        bm_gear,
        vec = (0.0,0.0,-(thk_tooth+dh_tooth_top+dh_inset+thk_base)),
        verts = verts_4
        )
    
    # Store these vertices for potential modification
    modify_verts.append(verts_4)

    if thk_base == 0.0:
        # Close up the original front edge
        bmesh.ops.bridge_loops( bm_gear,
                                edges= edges_base + edges_4
                            )
    else:
        if thk_inner<0:
            # This closes the extruded edges at the bottom
            bmesh.ops.edgeloop_fill(
                bm_gear,
                edges= edges_4)

            del verts_4
            del edges_4

            # Now go back and fill the edge along the teeth
            bmesh.ops.edgeloop_fill(
                bm_gear,
                edges= edges_base)
                
        else:   # Create the inset 
            ret = bmesh.ops.extrude_edge_only(
                    bm_gear,
                    edges=  edges_4
                    )
                    
            geom_hold = ret["geom"]
            
            del ret
            del verts_4
            del edges_4

            # Extract the extruded edges and vertices
            verts_5 = [ele for ele in geom_hold if isinstance(ele, \
                                    bmesh.types.BMVert)]
            edges_5 = [ele for ele in geom_hold if isinstance(ele, \
                                    bmesh.types.BMEdge) and ele.is_boundary]

            del geom_hold
            
            scale= r_inner/r_edge
            
            # Scale the vertices to the outer shell
            bmesh.ops.scale(
                bm_gear,
                vec = (scale,scale,1.0),  # @ inner of inset
                verts = verts_5
                )
                
            # Extrude back up to the top of the base
            ret = bmesh.ops.extrude_edge_only(
                    bm_gear,
                    edges=  edges_5
                    )
                    
            geom_hold = ret["geom"]
            
            del ret
            del verts_5
            del edges_5

            # Extract the extruded edges and vertices
            verts_6 = [ele for ele in geom_hold if isinstance(ele, \
                                    bmesh.types.BMVert)]
            edges_6 = [ele for ele in geom_hold if isinstance(ele, \
                                    bmesh.types.BMEdge) and ele.is_boundary]

            del geom_hold
            
            bmesh.ops.translate(
                bm_gear,
                vec = (0.0,0.0,thk_base),
                verts = verts_6
                )
            
            # Close up the original front edge
            bmesh.ops.bridge_loops(
                bm_gear,
                edges=edges_base + edges_6)
                
            del verts_6
            del edges_6

    del edges_base

    # Modify external housing if needed.
    if r_shape != 'Circle':
    
        verts_r_front_face= modify_edgeloop(
                                modify_verts[0], 
                                r_shape, 
                                r_edge
                            )
    
        verts_r_back_face=  modify_edgeloop(
                                modify_verts[1], 
                                r_shape, 
                                r_edge
                            )
    
    else:
    
        verts_r_front_face= modify_verts[0]

    if generate_blank:
        # Create a blank mesh for the other housing
        bm_blank_r= bmesh.new()
        
        height= thk_tooth + dh_tooth_top + dh_inset + blank_gap
    
        if r_shape == 'Circle':
        
            bmesh.ops.create_circle(    
                bm_blank_r,
                cap_ends=   False,
                segments=   180,
                radius=     r_edge + blank_gap,
                matrix=     mathutils.Matrix.Translation((0.0,0.0,height))
            )

        else:        
            x, y = verts_r_front_face[0]
            
            # Create outline with the first vertex
            ret=    bmesh.ops.create_vert(
                        bm_blank_r,
                        co= (x, y, 0.0)
                    )
                    
            vert_start= ret['vert'][0]
            
            vert_pair= ret['vert']
                    
            # Make edges as the new vertics are added
            for i in range(1,len(verts_r_front_face)):
                
                x, y = verts_r_front_face[i]
                
                ret=    bmesh.ops.create_vert(  
                            bm_blank_r,
                            co= (x, y, 0.0)
                        )
                
                vert_pair.extend(ret['vert'])
                
                bmesh.ops.contextual_create(    
                    bm_blank_r,
                    geom=       vert_pair,
                    use_smooth= False
                )
                                            
                vert_pair= [vert_pair[-1]]

            del ret
            
            vert_pair.append(vert_start)
                   
            del vert_start
            
            # Now close the gap between the first and last
            bmesh.ops.contextual_create(    
                bm_blank_r,
                geom=       vert_pair,
                use_smooth= False
            )

            del vert_pair
            
            # Determine new positions for remaining points
            # Move vertex along its normal gap/ cos(1/2 edge angle)
            new_positions= []

            for vert in bm_blank_r.verts:
                
                len_factor= blank_gap / math.cos(0.5*vert.calc_edge_angle())

                x_new= vert.co.x + len_factor * vert.normal[0]

                y_new= vert.co.y + len_factor * vert.normal[1]
                
                new_positions.append((x_new, y_new, vert))

            # With new positions determined, modify the vertex positions
            for x_new, y_new, vert in new_positions:

                vert.co.x= x_new

                vert.co.y= y_new
                
            del new_positions
        
            # Now move the blank to position
            bmesh.ops.translate(    
                bm_blank_r,
                vec=    (0.0,0.0,height), 
                verts=  bm_blank_r.verts
            )
                            
        # Enclose this with a face.
        bmesh.ops.contextual_create(    
            bm_blank_r,
            geom=       bm_blank_r.edges[:],
            use_smooth= False
        )

        # Extrude the edges down
        ret=    bmesh.ops.extrude_edge_only(   
                    bm_blank_r,
                    edges=  bm_blank_r.edges[:]
                )
            
        geom_hold= ret["geom"]
        del ret

        # Extract the extruded edges and vertices
        verts_r= [ele for ele in geom_hold if isinstance(ele, bmesh.types.BMVert)]
        edges_r= [ele for ele in geom_hold if isinstance(ele, \
                                bmesh.types.BMEdge) and ele.is_boundary]

        del geom_hold
        
        height= -thk_tooth - dh_tooth_top - dh_inset - thk_base - 2.*blank_gap
        
        bmesh.ops.translate(    
            bm_blank_r,
            vec=    (0.0,0.0,height), 
            verts=  verts_r
        )

        # Enclose this with a face.
        bmesh.ops.contextual_create(    
            bm_blank_r,
            geom=       edges_r,
            use_smooth= False
        )
                                    
        # Now flip the normals if oct or box
        if r_shape=='Box' or r_shape=='Pedestal	':
        
            bmesh.ops.reverse_faces(    
                bm_blank_r,
                faces = bm_blank_r.faces[:]
            )

        # Create an empty mesh and move the bmesh into it
        brg_msh_r_blank= bpy.data.meshes.new(name+'_r_blank_msh')
        bm_blank_r.to_mesh(brg_msh_r_blank)
        bm_blank_r.free()

        # Place the mesh into an object and then into the collection
        brg_r_blank= bpy.data.objects.new(name+'_r_blank', brg_msh_r_blank)
        bpy.context.collection.objects.link(brg_r_blank)
    
    # Get vertex count
    vertex_count= len(bm_gear.verts[:])
    
    # Finish up, write the bmesh into a new mesh
    msh_gear= bpy.data.meshes.new(name_msh)
    bm_gear.to_mesh(msh_gear)
    bm_gear.free()

    # Add the mesh to the scene
    obj_gear= bpy.data.objects.new(name, msh_gear)
    bpy.context.collection.objects.link(obj_gear)

    return (r_edge, r_inner, r_inset, vertex_count)
    
def gearOneToothGeom_interior(pa,N,m,k_a,k_d,bl,pts_i,pts_a,pts_d,thk_tooth, \
                                thk_base,thk_edge,thk_inner,d_edge_user, \
                                r_shape,generate_blank,blank_gap,d_ri_user, \
                                thk_inset,h_inset,dh_tooth_top,name):
    """ Return the gear radii noted below for the interior gear
    
        Inputs:
            pa  = Pressure angle, radians
            N   = Number of teeth
            m   = Module, mm/N
            k_a = Addendum height factor, h_a = k_a*m
            k_d = Dedendum height factor, h_d = k_d*m
            bl  = Backlash associated with this tooth, mm
            pts_i = Number of points for involue
            pts_a = Number of points for addendum
            pts_d = Number of points for dedendum
            thk_tooth = Thickness to extrude the gear teeth, mm
            thk_base  = Thickness to extrude the gear base, mm
            thk_edge  = Thickness of outer edge of gear, mm
            thk_inner = Thickness of inner ledge from addendum, mm
            name      = Blender object name for gear

        Outputs:
            Tuple, (r_p,r_a,r_d,r_b,r_edge,vertex_count)
            
        Note, total tooth height h = h_a + h_d
        Note, the addendum is always above the base so this check is not done.
        Note, Blender mesh name for gear will be name+'_msh'
        
        Circle Involute Parametric Equation from:
            Weisstein, Eric W. "Circle Involute." From MathWorld--A Wolfram Web Resource. 
            https://mathworld.wolfram.com/CircleInvolute.html
        
    """
    
    # Determine the relevant radii
    r_p= 0.5*m*N
    r_a= r_p - k_a*m
    r_d= r_p + k_d*m
    r_b= r_p*math.cos(pa)    

    # Determine the circle angles to generate the involute
    theta_a= math.sqrt((r_a*r_a)/(r_b*r_b)-1.)
    theta_p= math.sqrt((r_p*r_p)/(r_b*r_b)-1.)
    theta_d= math.sqrt((r_d*r_d)/(r_b*r_b)-1.)
    
    # Translate these circle angles to involute angles
    alpha_a= theta_a - math.atan(theta_a)
    alpha_p= theta_p - math.atan(theta_p)
    alpha_d= theta_d - math.atan(theta_d)
    
    # Determine the backlash angle. Backlash basis is at the pitch
    alpha_bl= bl/r_p
    
    # Determine the tooth thickness.
    # Hard coded value of 0.5 circular pitch without backlash
    # The backlash increases the tooth thickness
    del_alpha_tooth= math.pi/(2.*N) + alpha_bl
    
    # Generate the actual start and end angles around the involute angles
    # Prior to rotating, 0 = start point for involute on the base
    alpha_end=      alpha_p + del_alpha_tooth
    alpha_start=    alpha_end - math.pi/N

    # Begin genrating the tooth profile
    x_hold= []
    y_hold= []
    
    # Generate the addendum
    angle_step= (alpha_a-alpha_start)/(pts_a-1.)
    for i in range(0,int(pts_a)):
        angle= alpha_start + i*angle_step
        x_hold.append(r_a*math.cos(angle))
        y_hold.append(r_a*math.sin(angle))

    # Generate the involute
    # Determine constant arc length - 20230603
    d_arc= r_b * ( theta_d*theta_d - theta_a*theta_a ) / ( 2. * ( pts_i - 1. ) )

    # First point
    angle= theta_a
    x_hold.append(r_b*(math.cos(angle) + angle*math.sin(angle)))
    y_hold.append(r_b*(math.sin(angle) - angle*math.cos(angle)))
    
    for i in range(1, int(pts_i) ):
        angle= math.sqrt( angle*angle + 2.*d_arc/r_b )
        x_hold.append(r_b*(math.cos(angle) + angle*math.sin(angle)))
        y_hold.append(r_b*(math.sin(angle) - angle*math.cos(angle)))
    
    # Generate the dedendum
    angle_step= (alpha_end-alpha_d)/(pts_d-1.)
    for i in range(0,int(pts_d)):
        angle= alpha_d + i*angle_step
        x_hold.append(r_d*math.cos(angle))
        y_hold.append(r_d*math.sin(angle))

    # Shift to align along the x-axis to facilitate the mirror
    x_coord= []
    y_coord= []
    for i in range(0,len(x_hold)):
        x_coord.append(x_hold[i]*math.cos(-alpha_end) \
                            - y_hold[i]*math.sin(-alpha_end) )
        
        y_coord.append(x_hold[i]*math.sin(-alpha_end) \
                            + y_hold[i]*math.cos(-alpha_end) )
    
    del x_hold
    del y_hold

    # Perform the mirror on reversed set of data to make contiguous for Blender
    x_top= [x for x in x_coord]
    x_top.reverse()
    
    y_top= [-x for x in y_coord]
    y_top.reverse()
    
    # Combine everything into a single tooth outline
    x_coord+= x_top
    y_coord+= y_top
    
    del x_top
    del y_top
    
    # Generate the interior gear    
    r_edge, r_inner, r_inset, vertex_count= generateInteriorGearBlender(
                                                                N,
                                                                x_coord,
                                                                y_coord,
                                                                r_d,
                                                                r_a,
                                                                thk_tooth,
                                                                thk_base,
                                                                thk_edge,
                                                                thk_inner,
                                                                d_edge_user,
                                                                r_shape,
                                                                generate_blank,
                                                                blank_gap,
                                                                d_ri_user,
                                                                thk_inset,
                                                                h_inset,
                                                                dh_tooth_top,
                                                                name
                                                            )
    
    return (r_p,r_a,r_d,r_b,r_edge,r_inner,r_inset,vertex_count)

def gearOneToothGeom_exterior(
        pa,
        N,
        m,
        k_a,
        k_d,
        bl,
        pts_i,
        pts_a,
        pts_d,
        pts_d_b,
        pts_c,
        F_cmfr,
        thk,
        name
    ):
    
    """ Return the gear radii and Blender vertex count noted below for
        the external gear
    
        Inputs:
            pa  = Pressure angle, radians
            N   = Number of teeth
            m   = Module, mm/N
            k_a = Addendum height factor, h_a = k_a*m
            k_d = Dedendum height factor, h_d = k_d*m
            bl  = Backlash associated with this tooth, mm
            pts_i = Number of points for involue
            pts_a = Number of points for addendum
            pts_d = Number of points for dedendum
            pts_d_b= Number of points for dedendum -> base (if dedendum < base )
            thk   = Thickness to extrude the gear, mm
            thk_steps= Number of steps to break up the thickness (aspect ratio on triangles)
            name  = Blender object name for gear
            
        Outputs:
            Tuple, (r_p,r_a,r_d,r_b,vertex_count)
            
        Note, total tooth height h = h_a + h_d = (k_a + k_d)*m
        Note, Blender mesh name for gear will be name+'_msh'
        
        
        Circle Involute Parametric Equation from:
            Weisstein, Eric W. "Circle Involute." From MathWorld--A Wolfram Web Resource. 
            https://mathworld.wolfram.com/CircleInvolute.html
        
        
    """
    
    # Determine the relevant radii
    r_p= 0.5*m*N
    
    r_a= r_p + k_a*m
    
    r_d= r_p - k_d*m
    
    r_b= r_p*math.cos(pa)

    # Set chamber factor (value must be between 0 -> 1 )
    F_cmfr_used= max(min(F_cmfr, 1.0), 0.0)

    # Check bottom starting point. Base or dedendum
    r_start= r_b
    
    if r_b<r_d:
        
        r_start= r_d
        
        F_cmfr_used= 0.
    
    # Determine the involute angles to generate the involute
    theta_a= math.sqrt((r_a*r_a)/(r_b*r_b)-1.)
    
    theta_p= math.sqrt((r_p*r_p)/(r_b*r_b)-1.)
    
    theta_d= math.sqrt((r_start*r_start)/(r_b*r_b)-1.)
    
    # Translate these involute angles to circle angles
    alpha_a= theta_a - math.atan(theta_a)
    
    alpha_p= theta_p - math.atan(theta_p)
    
    alpha_d= theta_d - math.atan(theta_d)
    
    # Determine the backlash angle. Backlash basis is at the pitch
    alpha_bl= bl/r_p
    
    # Determine the tooth thickness.
    # Hard coded value of 0.5 circular pitch without backlash
    # The backlash reduces the tooth thickness
    del_alpha_tooth= math.pi/(2.*N) - alpha_bl
    
    # Generate the actual start and end angles around the involute angles
    # Prior to rotating, 0 = start point for involute on the base
    alpha_end=      alpha_p + del_alpha_tooth
    
    alpha_start=    alpha_end - math.pi/N

    # Check for chamfer
    alpha_c= alpha_d

    r_c= r_d

    x_cc= 0.0       # x-location of circle center
    
    y_cc= 0.0       # y-location of circle center
    
    r_cc= 0.0       # Chamfer circle radius
    
    beta_cc= 0.0    # Angle to chamfer center
    
    x_c_pts= []
    
    y_c_pts= []
    
    if F_cmfr_used > 0.:    # Add a chamfer
        # set a constant for the calculations
        K_hold= F_cmfr_used * ( r_b - r_d )
        
        # Chamfer dimensions
        r_cc= K_hold * ( 1. + K_hold / ( 2. * r_d ) )   
        
        y_cc= -r_cc  
        
        x_cc= r_d + K_hold  
        
        # Angle of line to the chamfer circle center
        beta_cc= math.atan2( y_cc, x_cc )
        
        # Limit to alpha start as needed
        if beta_cc < alpha_start:
        
            beta_cc= alpha_start 
            
            x_cc= r_d * (
                            math.sqrt(
                                         math.tan(beta_cc)*math.tan(beta_cc) \
                                       + 1. 
                                      ) \
                          - math.tan(beta_cc) 
                        )
                        
            F_cmfr_used= ( x_cc - r_d ) / ( r_b - r_d )
            
            if F_cmfr_used > 1.0:
            
                F_cmfr_used= 0.0
                
                x_cc= 0.0
                
                y_cc= 0.0
                
                r_cc= 0.0
                
                beta_cc= 0.0
                
            else:
            
                y_cc= x_cc * math.tan(beta_cc)
                
                r_cc= -y_cc
                        
        if F_cmfr_used <= 1.:
            
            angle_step_c= ( 0.5 * math.pi + beta_cc ) / ( pts_c - 1. )
            
            for i in range(0, int(pts_c)):
            
                angle_curr= math.pi + beta_cc - i*angle_step_c
                
                x_c_pts.append( r_cc * math.cos(angle_curr) + x_cc )
                
                y_c_pts.append( r_cc * math.sin(angle_curr) + y_cc )
            
            alpha_c= beta_cc

            r_c= x_cc
    
    # Begin genrating the tooth profile
    x_hold= []

    y_hold= []
    
    # Generate the dedendum
    angle_step= (alpha_c-alpha_start)/(pts_d-1.) 
    
    for i in range(0,int(pts_d)):
    
        angle= alpha_start + i*angle_step
    
        x_hold.append(r_d*math.cos(angle))
    
        y_hold.append(r_d*math.sin(angle))

    # Add the chamfer
    x_hold.extend(x_c_pts)
    
    y_hold.extend(y_c_pts)
    
    # If base > dedendum add points 
    if r_b > r_d:
    
        d_len= ( r_b - r_c ) / ( pts_d_b - 1. ) 
    
        for i in range(0, int(pts_d_b)):
        
            x_hold.append( r_c + i*d_len )  
        
            y_hold.append( y_hold[-1] )
    
    # Generate the involute
    # Determine constant arc length
    d_arc= r_b * ( theta_a*theta_a - theta_d*theta_d ) / ( 2. * ( pts_i - 1. ) )

    # First point
    angle= theta_d
    
    x_hold.append(r_b*(math.cos(angle) + angle*math.sin(angle)))
    
    y_hold.append(r_b*(math.sin(angle) - angle*math.cos(angle)))
    
    for i in range(1, int(pts_i) ):
    
        angle= math.sqrt( angle*angle + 2.*d_arc/r_b )
    
        x_hold.append(r_b*(math.cos(angle) + angle*math.sin(angle)))
    
        y_hold.append(r_b*(math.sin(angle) - angle*math.cos(angle)))
    
    # Generate the addendum
    angle_step= (alpha_end-alpha_a)/(pts_a-1.)
    
    for i in range(0,int(pts_a)):
    
        angle= alpha_a + i*angle_step
    
        x_hold.append(r_a*math.cos(angle))
    
        y_hold.append(r_a*math.sin(angle))
    
    # Shift to align along the x-axis to facilitate the mirror
    x_coord= []

    y_coord= []

    for i in range(0,len(x_hold)):

        x_coord.append(
                          x_hold[i]*math.cos(-alpha_end) \
                        - y_hold[i]*math.sin(-alpha_end) \
                      )
        
        y_coord.append(
                          x_hold[i]*math.sin(-alpha_end) \
                        + y_hold[i]*math.cos(-alpha_end) \
                      )
    
    del x_hold
    
    del y_hold

    # Perform the mirror on reversed set of data to make contiguous for Blender
    x_top= [x for x in x_coord]
    
    x_top.reverse()
    
    y_top= [-x for x in y_coord]
    
    y_top.reverse()
    
    # Combine everything into a single tooth outline
    x_coord.extend(x_top)
    
    y_coord.extend(y_top)
    
    del x_top
    
    del y_top

    # Generate the external gear in Blender
    vertex_count=   generateExteriorGearBlender(
                        N,
                        x_coord,
                        y_coord,
                        thk,
                        1,
                        name
                    )

    # Exit and return geometry information
    return (r_p,r_a,r_d,r_b,vertex_count, (F_cmfr_used,r_cc,x_cc,y_cc,beta_cc))

def main():

    ############################
    ### User input parameters 
    ############################
    
    # Gear arrangment
    planet_gears=   1   # Number of planet gears per arm (1 or 2 only)
    s_s=            3  # Speed of the sun gear (-1 if fixed) (must be an integer)
    s_r=            -1.  # Speed of the ring gear (-1 if fixed) (must be an integer)
    s_c=            1.  # Speed of carrier (cannot be fixed) (must be an integer)
    arms_used=      500  # Input very high value to get maximum numbr of arms
    
    # Common parameters for all gears
    m=          1.          # Normal Module to utilize
    k_a=        1.0         # Gear addendum factor, standard= 1.0
    k_d=        1.25        # Gear dedendum factor, standard= 1.25
    pa_deg=     20.         # Normal pressure angle, degrees (standard=20)
    cr_min=     1.4         # Minimum required total contact ratio, normal=1.4
    bl_tot=     0.15        # Total backlash, evenly split between the gears
    F_cmfr=     0.5         # Chamfer factor.  Must be 0.0 <= F_cmfr <= 1.0
    
    # Note these points are used to generate only one tooth.
    # Not the complete gear. 
    pts_involute=   55    # Points used to generate half the involute
    pts_addendum=   15    # Points used to generate half the addendum
    pts_dedendum=   15    # Points used to generate half the dedendum
    pts_ded_base=   10    # Points used split from dedendum to base (if dedendum < base)
    pts_chamfer=    10    # Points used to generte the chamfer
    
    # Ring gear specific
    thk_r_tooth=    6.       # Axial height of the tooth (opposite or base)
    thk_r_base=     6.0      # Axial height from base of tooth to bottom
    thk_r_edge=     3.0      # Radial thickness from inset or ded to outer edge
    thk_r_inner=    -1.      # Radial thickness from addendum to interior
    thk_r_inset=    4.       # Radial top insert from dedendum prior to outer edge
    h_r_inset=      5.       # Axial height of inset
    h_r_tooth_top=  1.0      # Axial extrution of top of tooth before inset or going to outer edge
    do_r_user=       5.0       # User requested outer ring OD
    di_r_user=      200.       # User requested inner ring OD
    r_shape=        'O'       # Shape of outside of outer ring.
                                # C = Circle
                                # B = Box
                                # O = Octagon
                                # P = Pedestal
    generate_blank=     False      # If an outer housing blank is to be made
    blank_gap=          1.000     # Gap between blank and permanent outer housing
    
    name_base=         "all_gear"   # Base/start portion of object names

    ### NOTE:   No check of existing Blendeer object/mesh names done.
    ###         Confirm no conflicts or unexpected behavior may occur 

    # Planet gear specific
    thk_p1=      6.         # Thickness of Planet 1 gear
    
    thk_p2=      6.         # Thickness of Planet 2 gear
    
    # Sun gear specific
    thk_s=      6.          # Thickness of sun gear
    N_s_min_user=    -1.     # Minimum number of sun teeth
    
    write_file=     False
    file_name=      'absolute_file_path'

    ############################
    ### Internal Calculations
    ############################

    # Generate Blender object names
    name_r=     name_base + "_ring"

    name_p1=    name_base + "_planet_1"

    name_p2=    name_base + "_planet_2"

    name_s=     name_base + "_sun"
    
    if write_file:
        with open(file_name, mode='at') as fileout:
            fileout.write(f'\n\n{"*"*50}')
            fileout.write(f'\n**** Starting New Planetary Spur Gear Run ({name_base}) ****')

    # Set the type of outer housing
    if r_shape.upper()=='C':

        r_shape_output= 'Circle'
    
    elif r_shape.upper()=='B':

        r_shape_output= 'Box'
    
    elif r_shape.upper()=='O':

        r_shape_output= 'Oct'
    
    else:

        r_shape_output= 'Pedestal'
    
    # Internal variable
    planet_gear_x=  1.  # For 2 planet gears, factor (N_p2= x*N_p1) usually 1

    tol_rel=    0.01  # Iteration relative tolerance in percent

    pa=         math.radians(pa_deg)
    
    # Convert to backlash per tooth
    bl= 0.5*bl_tot

    # Placeholders for psuedo speeds for standard spur gear analysis
    s_out_val=  0.

    s_in_val=   0.
    
    # Check for consistent inputs
    if planet_gears==1:

        if s_r==-1:  # Fixed ring

            if s_s/s_c<= 2.:
            
                bpy.context.window_manager.popup_menu(
                    e_pg1_fr, 
                    title=  "Oops, Sorry! Planet Gear= 1, Fixed Ring", 
                    icon=   'ERROR'
                )
            
                if write_file:
                    with open(file_name, mode='at') as fileout:
                        fileout.write(f'\n\n***** Error: Planet Gear= 1, Fixed Ring ******')
                        fileout.write(f'\n\t*** Condition: s_sun/s_carrier > 2 not met. ***')

                return 0

            else:

                s_out_val=  2.*s_c

                s_in_val=   s_s - 2.*s_c
                
        elif s_s==-1:   # Fixed sun

            if s_r/s_c<=1. or s_r/s_c>=2.:
            
                bpy.context.window_manager.popup_menu(
                    e_pg1_fs, 
                    title=  "Oops, Sorry! Planet Gear= 1, Fixed Sun", 
                    icon=   'ERROR'
                )
            
                if write_file:
                    with open(file_name, mode='at') as fileout:
                        fileout.write(f'\n\n***** Error: Planet Gear= 1, Fixed Sun ******')
                        fileout.write(f'\n\t*** Condition: 1 < s_ring/s_carrier< 2 not met. ***')

                return 0

            else:

                s_out_val=  2.*(s_r-s_c)

                s_in_val=   2.*s_c - s_r
                
        else:

            bpy.context.window_manager.popup_menu(
                e_pg1_nf, 
                title=  "Oops, Sorry! Planet Gear= 1, No Fixed Sun or Ring", 
                icon=   'ERROR'
            )            

            if write_file:

                with open(file_name, mode='at') as fileout:
                    fileout.write(f'\n\n**** Error: Planet Gear= 1, No Fixed Sun or Ring ****')
                    fileout.write(f'\n\t*** Condition: Need either sun or ring fixed (-1) ***')

            return 0

    elif planet_gears==2:

        if s_r==-1:  # Fixed ring

            s_out_val=  2.*(1.+planet_gear_x)*s_c

            s_in_val=   s_s
            
        elif s_s==-1:   # Fixed sun

            if s_r/s_c>1.:
            
                bpy.context.window_manager.popup_menu(
                    e_pg2_fs, 
                    title=  "Oops, Sorry! Planet Gear= 2, Fixed Sun", 
                    icon=   'ERROR'
                )
            
                if write_file:
                    with open(file_name, mode='at') as fileout:
                        fileout.write(f'\n\n***** Error: Planet Gear= 2, Fixed Sun ******')
                        fileout.write(f'\n\t*** Condition: s_ring/s_carrier< 1 not met. ***')

                return 0

            else:

                s_out_val=  2.*(1+planet_gear_x)*math.fabs(s_r-s_c)

                s_in_val=   s_r
        else:
            
            bpy.context.window_manager.popup_menu(
                e_pg2_nf, 
                title=  "Oops, Sorry! Planet Gear= 2, No Fixed Sun or Ring", 
                icon=   'ERROR'
            )
            
            if write_file:
            
                with open(file_name, mode='at') as fileout:
                    fileout.write(f'\n\n**** Error: Planet Gear= 2, No Fixed Sun or Ring ****')
                    fileout.write(f'\n\t*** Condition: Need either sun or ring fixed (-1) ***')

            return 0
    else:

        bpy.context.window_manager.popup_menu(
            e_pg_n12, 
            title=  "Oops, Sorry! Planet Gear != 1 or 2", 
            icon=   'ERROR'
        )

        if write_file:
        
            with open(file_name, mode='at') as fileout:
                fileout.write(f'\n\n***** Error: Planet Gear != 1 or 2, ******')
                fileout.write(f'\n\t*** Condition: Planet Gear Number 1 or 2 not met ***')
        
        return 0

    # Calculated parmaters
    sr= s_out_val/s_in_val  # speed ratio

    N_s_min_interference=   interfere_N_in_min(
                                k_a,
                                k_a,
                                sr,
                                pa_deg
                            )

    N_s_min_contactRatio=   contactRatio_N_in_min(
                                sr,
                                pa_deg,
                                cr_min,
                                k_a,
                                k_a,
                                tol_rel
                            )

    N_s_min= max(
                    N_s_min_interference,
                    N_s_min_contactRatio,
                    N_s_min_user
                 )

    sr_gcd= math.gcd(int(s_in_val),int(s_out_val))
    
    N_s_base= math.ceil(N_s_min*sr_gcd/s_out_val)

    N_s= N_s_base*s_out_val/sr_gcd

    N_p1= N_s/sr

    cd_s_p1= 0.5*m*N_s*(sr+1.)/sr

    cr_act= contactRatioEq(
                sr,
                pa_deg,
                k_a,
                k_a,
                N_s
            )
    
    N_p2= planet_gear_x*N_p1

    N_r= N_s + 2.*N_p1

    N_i_check= N_p1
    
    if planet_gears==2:

        N_r= N_s + 2.*(N_p1+N_p2)

        N_i_check= N_p2

    N_px_min=   internalGearInterferenceCheck1(
                    pa,
                    N_r,
                    k_a
                )
    
    if N_i_check<N_px_min:
        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write(f'\n\n***** Error: Ring/Planet Interference Check ******')
                fileout.write(f'\n\t*** Condition: N_p_min= {N_px_min:10.6f} ***')
                fileout.write(f'\n\t*** Condition: N_p_act= {N_i_check:10.6f} ***')
        
        # Update teeth to meet criteria
        if planet_gears==2:

            N_px_min/= planet_gear_x
    
        N_p1_base=  math.ceil(N_px_min*sr_gcd/s_in_val)

        N_p1=       N_p1_base*s_in_val/sr_gcd

        N_s=        N_p1*sr
        
        cd_s_p1 = 0.5*m*N_s*(sr+1.)/sr

        cr_act= contactRatioEq(
                    sr,
                    pa_deg,
                    k_a,
                    k_a,
                    N_s
                )
        
        N_p2= planet_gear_x*N_p1

        N_r= N_s + 2.*N_p1

        N_i_check= N_p1
        
        if planet_gears==2:

            N_r= N_s + 2.*(N_p1+N_p2)

            N_i_check= N_p2

        N_px_min= internalGearInterferenceCheck1(pa,N_r,k_a)
        
        if N_i_check<N_px_min:

            bpy.context.window_manager.popup_menu(
                e_rp_intf_chk, 
                title=  "Oops, Sorry! Ring/Planet Interference Check", 
                icon=   'ERROR'
            )

            if write_file:
            
                with open(file_name, mode='at') as fileout:
                    fileout.write(f'\n\n***** Error: Ring/Planet Interference Check ******')
                    fileout.write(f'\n\t*** Condition: N_p_min= {N_px_min:10.6f} ***')
                    fileout.write(f'\n\t*** Condition: N_p_act= {N_i_check:10.6f} ***')

            return 0    # Only try one update

        else:

            if write_file:
                with open(file_name, mode='at') as fileout:
                    fileout.write(f'\n\n***** Updated: Ring/Planet Interference Check ******')
                    fileout.write(f'\n\t*** Condition: N_p_min= {N_px_min:10.6f} ***')
                    fileout.write(f'\n\t*** Condition: N_p_act= {N_i_check:10.6f} ***')

    # Determine the final speed ratios
    ratio_sr_sc= 1. + N_s/N_r

    ratio_ss_sc= 1. + N_r/N_s

    ratio_ss_sr= -N_r/N_s
    
    if planet_gears==2:

        ratio_sr_sc= 1. - N_s/N_r

        ratio_ss_sc= 1. - N_r/N_s

        ratio_ss_sr= N_r/N_s
        
    # Generate the various gears
    r_r_p,r_r_a,r_r_d,r_r_b,r_r_edge,r_r_inner,r_r_inset,pts_inner= gearOneToothGeom_interior(
                                                                        pa,
                                                                        N_r,
                                                                        m,
                                                                        k_a,
                                                                        k_d,
                                                                        bl,
                                                                        pts_involute,
                                                                        pts_addendum,
                                                                        pts_dedendum,
                                                                        thk_r_tooth,
                                                                        thk_r_base,
                                                                        thk_r_edge,
                                                                        thk_r_inner,
                                                                        do_r_user,
                                                                        r_shape_output,
                                                                        generate_blank,
                                                                        blank_gap,
                                                                        di_r_user,
                                                                        thk_r_inset,
                                                                        h_r_inset,
                                                                        h_r_tooth_top,
                                                                        name_r
                                                                    )

    # Planet Gear #1 Generation
    r_p1_p,r_p1_a,r_p1_d,r_p1_b,pts_p1,c_p1=    gearOneToothGeom_exterior(
                                                    pa,
                                                    N_p1,
                                                    m,
                                                    k_a,
                                                    k_d,
                                                    bl,
                                                    pts_involute,
                                                    pts_addendum,
                                                    pts_dedendum,
                                                    pts_ded_base,
                                                    pts_chamfer,
                                                    F_cmfr,
                                                    thk_p1,
                                                    name_p1
                                                )
    
    # Extract the chamfer information
    F_cmfr_used_p1,r_cc_p1,x_cc_p1,y_cc_p1,beta_cc_p1= c_p1    

    # Sun Gear Generation
    r_s_p,r_s_a,r_s_d,r_s_b,pts_s,c_s=  gearOneToothGeom_exterior(
                                            pa,
                                            N_s,
                                            m,
                                            k_a,
                                            k_d,
                                            bl,
                                            pts_involute,
                                            pts_addendum,
                                            pts_dedendum,
                                            pts_ded_base,
                                            pts_chamfer,
                                            F_cmfr,
                                            thk_s,
                                            name_s
                                        )

    # Extract the chamfer information
    F_cmfr_used_s,r_cc_s,x_cc_s,y_cc_s,beta_cc_s= c_s    

    # Planet Gear #2 Generation
    if planet_gears==2:
        r_p2_p,r_p2_a,r_p2_d,r_p2_b,pts_p2,c_p2=    gearOneToothGeom_exterior(
                                                        pa,
                                                        N_p2,
                                                        m,
                                                        k_a,
                                                        k_d,
                                                        bl,
                                                        pts_involute,
                                                        pts_addendum,
                                                        pts_dedendum,
                                                        pts_ded_base,
                                                        pts_chamfer,
                                                        F_cmfr,
                                                        thk_p2,
                                                        name_p2
                                                    )
        cd_s_p2= r_s_p+2.*r_p1_p+r_p2_p
    
        # Extract the chamfer information
        F_cmfr_used_p2,r_cc_p2,x_cc_p2,y_cc_p2,beta_cc_p2= c_p2    

    sun_rotation=   0.

    p1_rotation=    0.
    
    if planet_gears==1:

        if N_p1%2==0:

            sun_rotation= math.pi*m/(2.*r_s_p)
            
    else:

        if N_p2%2==0:

            p1_rotation= math.pi*m/(2.*r_p1_p)
        
        d_shaft_max_p2= 2.0*min(r_p2_d,r_p2_b)
    
    # Determine the number of possible arms
    possible_arms=  planetPossibleArms(
                        r_s_p,
                        r_p1_p,
                        r_p1_a,
                        N_s,
                        N_r,
                        planet_gears
                    )
    
    # Determine final arms to print
    arms_shown= 1

    arm_select= 0
    
    for i in range(1,len(possible_arms)+1):

        curr_arms, temp, temp1 = possible_arms[-i]
        
        if arms_used >= curr_arms:

            arms_shown= curr_arms

            arm_select= -i

            break
    
    # Determine maximum shaft diameters
    d_shaft_max_s= 2.0*min(r_s_d,r_s_b)

    d_shaft_max_p1= 2.0*min(r_p1_d,r_p1_b)
    
    ################# Blender output information ############
    if write_file:
        h_top_extrude= max(0.0,h_r_tooth_top)
        h_inset= 0.0 if thk_r_inset < 0. else h_r_inset
        
        with open(file_name, mode='at') as fileout:
            fileout.write(f'\n\n############# New Planetary Spur Gear Run = {name_base} ##############')
            fileout.write(f'\nUSER INPUTS:\n')
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Number of planet gears =', planet_gears))

            if s_s==-1:
                fileout.write('\t{0:<37}{1:>12s}\n'.format('Speed Sun Gear =', 'Fixed'))
            else:
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Speed Sun Gear =', s_s))
            
            if s_r==-1:
                fileout.write('\t{0:<37}{1:>12s}\n'.format('Speed Ring Gear =', 'Fixed'))
            else:
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Speed Ring Gear =', s_r))
            
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Speed Carrier =', s_c))
            fileout.write('\t{0:<37}{1:>12.4f}\n\n'.format('Second Plant Gear Factor =', planet_gear_x))

            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Pressure Angle (deg) =', pa_deg))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Module =', m))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('k_a =', k_a))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('k_d =', k_d))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Chamfer Factor =', F_cmfr))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Contact Ratio Minimum =', cr_min))
            fileout.write('\t{0:<37}{1:>12.4f}\n\n'.format('Total Backlash =', bl_tot))

            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Number of Points: Involue =', pts_involute))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Number of Points: Addendum =', pts_addendum))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Number of Points: Dedendum =', pts_dedendum))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Number of Points: Ded->Base =', pts_ded_base))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Number of Points: Chamfer =', pts_chamfer))
            fileout.write('\t{0:<37}{1:>12.4f}\n\n'.format('Number of Arms to Show =', arms_used))
            
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Sun Gear Thickness =', thk_s))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Plant Gear #1 Thickness =', thk_p1))
            
            if planet_gears>1:
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Plant Gear #2 Thickness =', thk_p1))
            
            if thk_r_inset < 0.:
                fileout.write('\t{0:<37}{1:>12s}\n'.format('Ring Gear Thickness: Inset =', 'None'))
                fileout.write('\t{0:<37}{1:>12s}\n'.format('Ring Gear Height: Inset =', 'None'))
            else:
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Ring Gear Thickness: Inset =', thk_r_inset))
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Ring Gear Height: Inset =', h_r_inset))

            if thk_r_inset < 0.:
                fileout.write('\t{0:<37}{1:>12s}\n'.format('Ring Gear Height: Top Extrude =', 'None'))
            else:
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Ring Gear Height: Top Extrude =', h_r_tooth_top))
            
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Ring Gear Thickness: Tooth =', thk_r_tooth))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Ring Gear Thickness: Base =', thk_r_base))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Ring Gear Thickness: Outer Edge =', thk_r_edge))
            
            if thk_r_inner==-1:
                fileout.write('\t{0:<37}{1:>12s}\n'.format('Ring Gear Thickness: Inner =', 'None'))
            else:
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Ring Gear Thickness: Inner =', thk_r_inner))

            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('RG Thickness: User Inner  =', 0.5*di_r_user))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('RG Thickness: User Outer =', 0.5*do_r_user))
            
            fileout.write(f'\nINTERNAL CALCULATIONS:\n')
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Interference =', N_s_min_interference))
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Contact Ratio =', N_s_min_contactRatio))
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('User Input =', N_s_min_user))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Pseudo Speed Out =', s_out_val))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Pseudo Speed In =', s_in_val))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Pseudo Speed Ratio =', s_out_val/s_in_val))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Minimum Outer Planet Teeth =', N_px_min))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Actual Speed Ratio: Ring/Carrier =', ratio_sr_sc))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Actual Speed Ratio: Sun/Carrier =', ratio_ss_sc))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Actual Speed Ratio: Sun/Ring =', ratio_ss_sr))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Actual Contact Ratio =', cr_act))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Number of Arms Shown =', arms_shown))
            
            fileout.write('\t{0:<37}{1:>12.4f}\n\n'.format('Possible Arms Arrangements =', len(possible_arms)))
            
            if len(possible_arms)>0:
                for arms, angles, rotations in possible_arms:
                    fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Arms =', arms))
                
                    if planet_gears == 1:
                        for i in range(len(angles)):
                            fileout.write('\t\t\t{0:<29}{1:>12.4f}\n'.format('Angle =', angles[i]))
                            fileout.write('\t\t\t\t{0:<25}{1:>12.4f}\n'.format('Rotation =', rotations[i]))
                            fileout.write('\t\t\t\t{0:<25}{1:>12.4f}\n\n'.format('Final Angle =', angles[i] + rotations[i]))
                            
                    else:
                        for i in range(len(angles)):
                            fileout.write('\t\t\t{0:<29}{1:>12.4f}\n'.format('Angle =', angles[i]))
                            fileout.write('\t\t\t\t{0:<25}{1:>12.4f}\n'.format('Rotation =', rotations[i]))
                            fileout.write('\t\t\t\t{0:<25}{1:>12.4f}\n'.format('Final P1 Angle =', angles[i] + rotations[i]))
                            fileout.write('\t\t\t\t{0:<25}{1:>12.4f}\n\n'.format('Final P2 Angle =', angles[i] - rotations[i]))
                            
            fileout.write(f'Sun Gear Information:\n')
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Pitch Radius =', r_s_p))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Pitch Diameter =', 2.*r_s_p))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Base Diameter =', 2.*r_s_b))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Addendum Diameter =', 2.0*r_s_a))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Dedendum Diameter =', 2.0*r_s_d))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Number of Teeth =', N_s))
            fileout.write(f'\tChamfer Information:\n')
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Factor =', F_cmfr_used_s))
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Radius =', r_cc_s))
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Center: X-dir =', x_cc_s))
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Center: Y-dir =', y_cc_s))
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Angle to Center =', beta_cc_s*180./math.pi))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Thickness =', thk_s))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Maximum Shaft Diameter =', d_shaft_max_s))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Mating Rotation (deg) =', sun_rotation*180./math.pi))
            
            fileout.write(f'\nPlanet Gear #1 Information:\n')            
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Pitch Radius =', r_p1_p))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Pitch Diameter =', 2.*r_p1_p))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Base Diameter =', 2.*r_p1_b))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Addendum Diameter =', 2.0*r_p1_a))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Dedendum Diameter =', 2.0*r_p1_d))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Number of Teeth =', N_p1))
            fileout.write(f'\tChamfer Information:\n')
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Factor =', F_cmfr_used_p1))
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Radius =', r_cc_p1))
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Center: X-dir =', x_cc_p1))
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Center: Y-dir =', y_cc_p1))
            fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Angle to Center =', beta_cc_p1*180./math.pi))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Thickness =', thk_p1))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Center Location from Sun =', cd_s_p1))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Maximum Shaft Diameter =', d_shaft_max_p1))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Mating Rotation (deg) =', p1_rotation*180./math.pi))
            
            if planet_gears==2:
                fileout.write(f'\nPlanet Gear #2 Information:\n')
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Pitch Radius =', r_p2_p))
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Pitch Diameter =', 2.*r_p2_p))
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Base Diameter =', 2.*r_p2_b))
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Addendum Diameter =', 2.0*r_p2_a))
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Dedendum Diameter =', 2.0*r_p2_d))
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Number of Teeth =', N_p2))
                fileout.write(f'\tChamfer Information:\n')
                fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Factor =', F_cmfr_used_p2))
                fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Radius =', r_cc_p2))
                fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Center: X-dir =', x_cc_p2))
                fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Center: Y-dir =', y_cc_p2))
                fileout.write('\t\t{0:<33}{1:>12.4f}\n'.format('Angle to Center =', beta_cc_p2*180./math.pi))
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Thickness =', thk_p2))
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Center Location from Sun =', cd_s_p2))
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Maximum Shaft Diameter =', d_shaft_max_p2))
            
            fileout.write(f'\nRing Gear Information:\n')
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Pitch Radius =', r_r_p))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Pitch Diameter =', 2.*r_r_p))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Base Diameter =', 2.*r_r_b))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Addendum Diameter =', 2.0*r_r_a))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Dedendum Diameter =', 2.0*r_r_d))

            if thk_r_inset < 0.:
                fileout.write('\t{0:<37}{1:>12s}\n'.format('Inset Radius =', 'None'))
                fileout.write('\t{0:<37}{1:>12s}\n'.format('Inset Diameter =', 'None'))
            else:
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Inset Radius =', r_r_inset))
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Inset Diameter =', 2.0*r_r_inset))

            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Outer End Diameter =', 2.0*r_r_edge))
            
            if thk_r_base > 0.:
                fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Inner Inset Diameter =', (2.0*r_r_a if thk_r_inner==-1 else 2.0*r_r_inner)))
            else:
                fileout.write('\t{0:<37}{1:>12s}\n'.format('Inner Inset Diameter =', 'None'))

            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Number of Teeth =', N_r))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Thickness Tooth =', thk_r_tooth))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Thickness Base =', thk_r_base))
            fileout.write('\t{0:<37}{1:>12.4f}\n'.format('Thickness Total =', thk_r_tooth+thk_r_base))
            
            fileout.write(f'\n############ End Planetary Spur Gear Data = {name_base} ##############\n')
    
    ################# Blender output information ############
    
    # Duplicate, rename, and move gears to requested configuration
    # Rename the first gears
    
    name_p1_new=    name_p1 + '_' + str(0)
    
    bpy.data.objects[name_p1].data.name=    name_p1 + '_msh_' + str(0)

    bpy.data.objects[name_p1].name=         name_p1_new
    
    if planet_gears == 2:

        name_p2_new=    name_p2 + '_' + str(0)
        
        bpy.data.objects[name_p2].data.name=    name_p2 + '_msh_' + str(0)

        bpy.data.objects[name_p2].name=         name_p2_new
    
    # Setup for duplicating and moving
    view_layer= bpy.context.view_layer

    activeOject= bpy.data.objects[name_p1_new]

    view_layer.objects.active = activeOject

    # Ensure in OBJECT mode
    if bpy.ops.object.mode_set.poll():

        bpy.ops.object.mode_set(
            mode = 'OBJECT',
            toggle = False
        )

    else:

        bpy.context.window_manager.popup_menu(
            e_mode_set, 
            title=  "Oops, Sorry! Moving Gears:", 
            icon=   'ERROR'
        )

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nMoving Gears: Issue with mode_set.\n#####\n')

    # Deselect everything for a clean slate
    if bpy.ops.object.select_all.poll():

        bpy.ops.object.select_all(action='DESELECT')

    else:

        bpy.context.window_manager.popup_menu(
            e_deslection, 
            title=  "Oops, Sorry! Moving Gears:", 
            icon=   'ERROR'
        )

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nMoving Gears: Issue with deselection.\n#####\n')

    if sun_rotation > 0.:
        # Select the sun gear object to rotate as needed
        activeOject= bpy.data.objects[name_s]

        view_layer.objects.active = activeOject

        activeOject.select_set(state=True)
        
        bpy.ops.transform.rotate(   
            value= sun_rotation,
            orient_axis='Z', 
            orient_type='GLOBAL', 
            orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
            orient_matrix_type='GLOBAL', 
            constraint_axis=(False, False, True), 
            release_confirm=False, 
            use_accurate=True
        )

        activeOject.select_set(state=False)
    
    # Select the first plant gear object to duplicate as needed
    activeOject= bpy.data.objects[name_p1_new]

    view_layer.objects.active = activeOject

    activeOject.select_set(state=True)

    bpy.context.object.location[0]= cd_s_p1
    
    # Perform mating rotation if needed
    if p1_rotation > 0.:
        bpy.ops.transform.rotate(   
            value= p1_rotation,
            orient_axis='Z', 
            orient_type='GLOBAL', 
            orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
            orient_matrix_type='GLOBAL', 
            constraint_axis=(False, False, True), 
            release_confirm=False, 
            use_accurate=True
        )

    if arms_shown > 1:
        # Obtain the data for the selected number of arms
        s_arms, s_angles, s_rotations= possible_arms[arm_select]
        
        # Loop through the arms
        for i in range(len(s_angles)):
            # Duplicate the zero angle gear
            bpy.ops.object.duplicate(linked=False, mode='ROTATION')
            
            # Rotate the gear around the center to the correct location
            bpy.ops.transform.rotate(   
                value= s_angles[i]*math.pi/180.,
                orient_axis='Z', 
                orient_type='GLOBAL', 
                orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
                orient_matrix_type='GLOBAL', 
                constraint_axis=(False, False, True), 
                center_override=(0.0, 0.0, 0.0), 
                release_confirm=False, 
                use_accurate=True
            )
                                    
            # Rotate the gear around its own center to mesh properly
            bpy.ops.transform.rotate(   
                value= s_rotations[i]*math.pi/180.,
                orient_axis='Z', 
                orient_type='GLOBAL', 
                orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
                orient_matrix_type='GLOBAL', 
                constraint_axis=(False, False, True), 
                release_confirm=False, 
                use_accurate=True
            )
                                    
            # Get the selected object which is the duplicated object
            dup_obj= bpy.context.selected_editable_objects[0]
            
            # Revise names
            dup_obj.data.name= name_p1 + '_msh_' + str(int(s_angles[i]))

            dup_obj.name= name_p1 + '_' + str(int(s_angles[i]))

            # Unselect duplicated object to prepare for next duplication
            dup_obj.select_set(state=False)
            
            # Go back to zero angle gear as active and selected object
            view_layer.objects.active = activeOject

            activeOject.select_set(state=True)

    if planet_gears == 2:
        # Select the second plant gear object to duplicate as needed
        activeOject.select_set(state=False)
        
        activeOject= bpy.data.objects[name_p2_new]

        view_layer.objects.active = activeOject

        activeOject.select_set(state=True)

        bpy.context.object.location[0]= cd_s_p2
        
        if arms_shown > 1:
            # Obtain the data for the selected number of arms
            s_arms, s_angles, s_rotations= possible_arms[arm_select]
            
            # Loop through the arms
            for i in range(len(s_angles)):
                # Duplicate the zero angle gear
                bpy.ops.object.duplicate(linked=False, mode='ROTATION')
                
                # Rotate the gear around the center to the correct location
                bpy.ops.transform.rotate(   
                    value= s_angles[i]*math.pi/180.,
                    orient_axis='Z', 
                    orient_type='GLOBAL', 
                    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
                    orient_matrix_type='GLOBAL', 
                    constraint_axis=(False, False, True), 
                    center_override=(0.0, 0.0, 0.0), 
                    release_confirm=False, 
                    use_accurate=True
                )
                                        
                # Rotate the gear around its own center to the correct location
                bpy.ops.transform.rotate(   
                    value= -s_rotations[i]*math.pi/180.,
                    orient_axis='Z', 
                    orient_type='GLOBAL', 
                    orient_matrix=((1, 0, 0), (0, 1, 0), (0, 0, 1)), 
                    orient_matrix_type='GLOBAL', 
                    constraint_axis=(False, False, True), 
                    release_confirm=False, 
                    use_accurate=True
                )
                                        
                # Get the selected object which is the duplicated object
                dup_obj= bpy.context.selected_editable_objects[0]
                
                # Revise names
                dup_obj.data.name= name_p2 + '_msh_' + str(int(s_angles[i]))

                dup_obj.name= name_p2 + '_' + str(int(s_angles[i]))

                # Unselect duplicated object to prepare for next duplication if needed
                dup_obj.select_set(state=False)
                
                # Go back to zero angle gear as active and selected object
                view_layer.objects.active = activeOject

                activeOject.select_set(state=True)

    # Deselect for a clean slate. Active Object is first P2 gear
    activeOject.select_set(state=False)
    
    return 0

# Call main if this file is executed as a script
if __name__ == '__main__':
    main()

