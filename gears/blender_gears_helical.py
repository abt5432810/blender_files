"""
PROGRAM:
blender_gears_helical.py

Copyright (C) 2024 Anders B. Thlenk

LICENSE:
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.


DESCRIPTION:
Utilize Blender <https://www.blender.org/> to auto generate a pair of helical gears given various user inputs.  Refer to the "User Inputs" section of main() below.

"""

import bpy
import bmesh
import math

#########################################################
#########  Just error boxes
#########################################################

def e_mode_set(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with mode_set.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

def e_deslection(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with deselection.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

#########################################################
#########  Starting Gear Stuff
#########################################################


def N_in_minEq(k_a,sr,pa):
    """ Determine the minimum number of teeth on input gear
    
        Inputs:
            k_a = Addendum factor on m for input gear (normally 1)
            sr = Speed Ratio (w.output/w.input)
            pa = Pressure Angle (radians)
            
        Output:
            Minimum number of teeth to avoid interference with base circle
    """
    
    fac1= 2.*k_a*sr / ((1.+2.*sr)*math.sin(pa)*math.sin(pa))
    fac2= sr + math.sqrt(sr*sr + (1.+2.*sr)*math.sin(pa)*math.sin(pa))
    
    return fac1*fac2


def N_out_minEq(k_a,sr,pa):
    """ Determine the minimum number of teeth on output gear
    
        Inputs:
            k_a = Addendum factor on m for output gear (normally 1)
            sr = Speed Ratio (w.output/w.input)
            pa = Pressure Angle (radians)
            
        Output:
            Minimum number of teeth to avoid interference with base circle
    """
    
    fac1= 2.*k_a / ((2.+ sr)*sr*math.sin(pa)*math.sin(pa))
    fac2= 1. + math.sqrt(1. + (2.+sr)*sr*math.sin(pa)*math.sin(pa))
    
    return fac1*fac2

def interfere_N_in_min(k_a_in,k_a_out,sr,pa):
    """ Determine the minimum number of teeth on input gear taking into 
        account the speed ratio and minimum output gear teeth required
    
        Inputs:
            k_a_in  = Addendum factor on m for input gear (normally 1)
            k_a_out = Addendum factor on m for output gear (normally 1)
            sr    = Speed Ration(w.output/w.input)
            pa    = Pressure Angle (radians)
            
        Output:
            Minimum number of teeth to avoid interference with base circle
    """
    
    return max(N_in_minEq(k_a_in,sr,pa),sr*N_out_minEq(k_a_out,sr,pa))


def contactRatioEq(sr,pa,k_a_in,k_a_out,N_in):
    """ Calcuate the value for the contact ratio based on the inlet
        gear.  
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            N_in    = Number of teeth on the input gear
            
        Output:
            The contact ratio
    """
    
    fac1 = 0.25*N_in*N_in*math.sin(pa)*math.sin(pa)+N_in*k_a_in + k_a_in*k_a_in
    fac2 = 0.25*N_in*N_in*math.sin(pa)*math.sin(pa)/(sr*sr) \
            + N_in*k_a_out/sr+k_a_out*k_a_out
    
    return (math.sqrt(fac1)+math.sqrt(fac2)-0.5*math.sin(pa)*N_in*(sr+1.)/sr) \
            / (math.pi*math.cos(pa))

def cr_Nin_zeroFn(sr,pa,cr,k_a_in,k_a_out,N_in):
    """ Calcuate the value for the contact ratio zero function for the inlet
        gear.  This is the error in the solution.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            N_in    = Number of teeth on the input gear
            
        Output:
            Value of zero contact ratio function
    """
    
    fac1 = 0.25*N_in*N_in*math.sin(pa)*math.sin(pa)+N_in*k_a_in+k_a_in*k_a_in
    fac2 = 0.25*N_in*N_in*math.sin(pa)*math.sin(pa)/(sr*sr) \
            + N_in*k_a_out/sr+k_a_out*k_a_out
    
    return math.sqrt(fac1) + math.sqrt(fac2) - cr*math.pi*math.cos(pa) \
            -0.5*math.sin(pa)*N_in*(sr+1.)/sr


def cr_Nin_zeroFnPrime(sr,pa,cr,k_a_in,k_a_out,N_in):
    """ Calcuate the value for the contact ratio zero function first
        derivative for the inlet gear.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            N_in    = Number of teeth on the input gear
            
        Output:
            Value of zero contact ratio function first derivative
    """
    
    fac1 = 0.25*N_in*N_in*math.sin(pa)*math.sin(pa)+N_in*k_a_in + k_a_in*k_a_in
    fac2 = 0.25*N_in*N_in*math.sin(pa)*math.sin(pa)/(sr*sr) \
            + N_in*k_a_out/sr + k_a_out*k_a_out
    
    fac3 = (0.5*math.sin(pa)*math.sin(pa)*N_in + k_a_in)/math.sqrt(fac1)
    fac4 = (0.5*math.sin(pa)*math.sin(pa)*N_in/(sr*sr) + k_a_in/sr) \
            /math.sqrt(fac2)
    
    return fac3 + fac4 -(sr+1.)*math.sin(pa)/sr

def cr_N_in_min(sr,pa,cr,k_a_in,k_a_out,tol_rel,N_in):
    """ Solve for the required number of input gear teeth to meet input 
        parameters using Newton's Method.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            tol_rel = Relative tolerance (percent)
            N_in    = Iniital guess for number of teeth
            
        Output:
            N_in    = Actual number of teeth to meet indicated conditions
    """

    for i in range(0,100):
        err_rel = cr_Nin_zeroFn(sr,pa,cr,k_a_in,k_a_out,N_in) \
                    / (N_in*cr_Nin_zeroFnPrime(sr,pa,cr,k_a_in,k_a_out,N_in))
        
        if(math.fabs(err_rel)<(tol_rel/100.)):
            return N_in
            
        N_in *= (1. - err_rel)

    return -10

def cr_Nout_zeroFn(sr,pa,cr,k_a_in,k_a_out,N_out):
    """ Calcuate the value for the contact ratio zero function for the outlet
        gear.  This is the error in the solution.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            N_out   = Number of teeth on the output gear
            
        Output:
            Value of zero contact ratio function
    """
    
    fac1 = 0.25*N_out*N_out*math.sin(pa)*math.sin(pa)*sr*sr \
            + sr*N_out*k_a_in + k_a_in*k_a_in
    fac2 = 0.25*N_out*N_out*math.sin(pa)*math.sin(pa) \
            + N_out*k_a_out + k_a_out*k_a_out
    
    return math.sqrt(fac1) + math.sqrt(fac2) - cr*math.pi*math.cos(pa) \
            - 0.5*math.sin(pa)*N_out*(sr+1.)



def cr_Nout_zeroFnPrime(sr,pa,cr,k_a_in,k_a_out,N_out):
    """ Calcuate the value for the contact ratio zero function derivative 
        for the outlet gear.  This is the error in the solution.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            N_out   = Number of teeth on the output gear
            
        Output:
            Value of zero contact ratio function derivative
    """
    
    fac1 = 0.25*N_out*N_out*math.sin(pa)*math.sin(pa)*sr*sr \
            + sr*N_out*k_a_in + k_a_in*k_a_in
    fac2 = 0.25*N_out*N_out*math.sin(pa)*math.sin(pa) \
            + N_out*k_a_out + k_a_out*k_a_out
    
    fac3 = (0.5*sr*sr*math.sin(pa)*math.sin(pa)*N_out + sr*k_a_in) \
            / math.sqrt(fac1)
    fac4 = (0.5*math.sin(pa)*math.sin(pa)*N_out + k_a_out) / math.sqrt(fac2)

    return fac3 + fac4 - (sr+1.)*math.sin(pa)

def cr_N_out_min(sr,pa,cr,k_a_in,k_a_out,tol_rel,N_out):
    """ Solve for the required number of input gear teeth to meet input 
        parameters using Newton's Method.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            tol_rel = Relative tolerance (percent)
            N_out   = Iniital guess for number of teeth
            
        Output:
            N_out   = Actual number of teeth to meet indicated conditions
    """

    for i in range(0,100):
        err_rel = cr_Nout_zeroFn(sr,pa,cr,k_a_in,k_a_out,N_out) \
                   / (N_out*cr_Nout_zeroFnPrime(sr,pa,cr,k_a_in,k_a_out,N_out))
        
        if(math.fabs(err_rel)<(tol_rel/100.)):
            return N_out
            
        N_out *= (1. - err_rel)

    return -10

def contactRatio_N_in_min(sr,pa,cr,k_a_in,k_a_out,tol_rel):
    """ Solve for the required number of input gear teeth to meet input 
        parameters using Newton's Method.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            tol_rel = Relative tolerance (percent)
            
        Output:
            Minimum number of input gear teeth to meet contact ratio input
    """
    return max(cr_N_in_min(sr,pa,cr,k_a_in,k_a_out,tol_rel,2.), \
                    cr_N_out_min(sr,pa,cr,k_a_in,k_a_out,tol_rel,2.)*sr)


def generateHelicalExteriorGearBlender(
                                    N,
                                    x,
                                    y,
                                    spin_angle,
                                    steps,
                                    face,
                                    hand,
                                    isHerringbone,
                                    name
                                ):
    """ Generate a blender object for this one gear
    
        Inputs:
            N = Number of teeth for gear
            x = List of x-coordinate data for one tooth
            y = List of y-coordinate data for one tooth
            thk = If filled, provide some thickness. -1 if just keep as a plane
            fill = Fill in the gear, boolean
            name = String for Blender object name of gear
            
        Output:
            A Blender object:
            Float: Number of vertex points on the gear.
    """

    # Blender mesh name
    name_msh=   name+"_msh"
    
    # Make a new BMesh
    bm_gear= bmesh.new()
    
    # Create the single tooth outline
    ret=    bmesh.ops.create_vert(
                bm_gear,
                co= (x[0],y[0],0.0)
            )
            
    vert_pair= ret['vert']
    
    # Make edges as the new vertics are added
    for i in range(1,len(x)):
        ret=    bmesh.ops.create_vert(
                    bm_gear,
                    co= (x[i],y[i],0.0)
                )
        
        vert_pair.extend(ret['vert'])
        
        bmesh.ops.contextual_create(
            bm_gear,
            geom=       vert_pair,
            use_smooth= False
            )
            
        vert_pair= [vert_pair[-1]]

    del ret
    
    # Now remove doubles and merge ends to make loops
    bmesh.ops.remove_doubles(
        bm_gear,
        verts=  bm_gear.verts[:],
        dist=   0.0001)
    
    # Spin the tooth to make the gear
    ret = bmesh.ops.spin(
        bm_gear,
        geom=           bm_gear.verts[:] + bm_gear.edges[:],
        angle=          2.*math.pi*(N-1.)/N,
        steps=          int(N-1),
        axis=           (0.0, 0.0, 1.0),
        cent=           (0.0, 0.0, 0.0),
        dvec=           (0.0, 0.0, 0.0),
        use_merge=      True,
        use_duplicate=  True 
        )

    del ret
    
    # Now remove doubles and merge ends to make loops
    bmesh.ops.remove_doubles(
        bm_gear,
        verts=  bm_gear.verts[:],
        dist=   0.0001
    )
   
    # Enclose the gear outline with a face
    bmesh.ops.contextual_create(
        bm_gear,
        geom=       bm_gear.edges[:],
        use_smooth= False
    )
    
    # Spin gear to make the helical gear
    ret=    bmesh.ops.spin(
                bm_gear,
                geom=   bm_gear.verts[:] + bm_gear.edges[:],
                angle=  spin_angle,
                steps=  int(steps),
                axis=   (0.0, 0.0, hand),
                cent=   (0.0, 0.0, 0.0),
                dvec=   (0.0, 0.0, face/steps) 
            )

    geom_last = ret['geom_last']
    del ret

    # Extract the last edges at the top to fill
    edges_end = [ele for ele in geom_last if isinstance(ele, \
                    bmesh.types.BMEdge) and ele.is_boundary]

    if isHerringbone: # Continue spinning in the opposite hand
        verts = [ele for ele in geom_last if isinstance(ele, \
                    bmesh.types.BMVert)]
        
        ret=    bmesh.ops.spin(
                    bm_gear,
                    geom=   verts + edges_end,
                    angle=  spin_angle,
                    steps=  int(steps),
                    axis=   (0.0, 0.0, -hand),
                    cent=   (0.0, 0.0, 0.0),
                    dvec=   (0.0, 0.0, face/steps) 
                )

        geom_last = ret['geom_last']
    
        del ret
        
        edges_end = [ele for ele in geom_last if isinstance(ele, \
                    bmesh.types.BMEdge) and ele.is_boundary]
    
    del geom_last
    
    # This closes the extruded edges at the top to a face
    bmesh.ops.edgeloop_fill(
                            bm_gear,
                            edges=  edges_end
                            )
    
    vertex_count= len(bm_gear.verts[:])
    
    # Finish up, write the bmesh into a new mesh
    msh_gear= bpy.data.meshes.new(name_msh)
    bm_gear.to_mesh(msh_gear)
    bm_gear.free()

    # Add the mesh to the scene
    obj_gear= bpy.data.objects.new(name, msh_gear)
    bpy.context.collection.objects.link(obj_gear)

    return vertex_count

def generateHelicalGear_external(
        pa,
        N,
        m,
        k_a,
        k_d,
        bl,
        pts_i,
        pts_a,
        pts_d,
        pts_d_b,
        pts_c,
        F_cmfr,
        thk,
        face_min,
        hand,
        p_angleHelix,
        baseSteps,
        isHerringbone,
        name
    ):

    """ Return the gear radii and Blender vertex count noted below for
        the external gear
    
        Inputs:
            pa  = Transverse pressure angle, radians
            N   = Number of teeth
            m   = Module, mm/N
            k_a = Addendum height factor, h_a = k_a*m
            k_d = Dedendum height factor, h_d = k_d*m
            bl  = Backlash associated with this tooth, mm
            pts_i = Number of points for involue
            pts_a = Number of points for addendum
            pts_d = Number of points for dedendum
            pts_d_b= Number of points for dedendum -> base (if dedendum < base )
            thk   = Thickness to extrude the gear, mm
            name  = Blender object name for gear
            
        Outputs:
            Tuple, (r_p,r_a,r_d,r_b,vertex_count)
            
        Note, total tooth height h = h_a + h_d = (k_a + k_d)*m
        Note, Blender mesh name for gear will be name+'_msh'

        Circle Involute Parametric Equation from:
            Weisstein, Eric W. "Circle Involute." From MathWorld--A Wolfram Web Resource. 
            https://mathworld.wolfram.com/CircleInvolute.html

    """
    
    # Determine the relevant radii
    r_p= 0.5*m*N
    
    r_a= r_p + k_a*m
    
    r_d= r_p - k_d*m
    
    r_b= r_p*math.cos(pa)

    # Set chamber factor (value must be between 0 -> 1 )
    F_cmfr_used= max(min(F_cmfr, 1.0), 0.0)

    # Check bottom starting point. Base or dedendum
    r_start= r_b
    
    if r_b<r_d:
        
        r_start= r_d
        
        F_cmfr_used= 0.
    
    # Determine the involute angles to generate the involute
    theta_a= math.sqrt((r_a*r_a)/(r_b*r_b)-1.)
    
    theta_p= math.sqrt((r_p*r_p)/(r_b*r_b)-1.)
    
    theta_d= math.sqrt((r_start*r_start)/(r_b*r_b)-1.)
    
    # Translate these involute angles to circle angles
    alpha_a= theta_a - math.atan(theta_a)
    
    alpha_p= theta_p - math.atan(theta_p)
    
    alpha_d= theta_d - math.atan(theta_d)
    
    # Determine the backlash angle. Backlash basis is at the pitch
    alpha_bl= bl/r_p
    
    # Determine the tooth thickness.
    # Hard coded value of 0.5 circular pitch without backlash
    # The backlash reduces the tooth thickness
    del_alpha_tooth= math.pi/(2.*N) - alpha_bl
    
    # Generate the actual start and end angles around the involute angles
    # Prior to rotating, 0 = start point for involute on the base
    alpha_end=      alpha_p + del_alpha_tooth
    
    alpha_start=    alpha_end - math.pi/N

    # Check for chamfer
    alpha_c= alpha_d

    r_c= r_d

    x_cc= 0.0       # x-location of circle center
    
    y_cc= 0.0       # y-location of circle center
    
    r_cc= 0.0       # Chamfer circle radius
    
    beta_cc= 0.0    # Angle to chamfer center
    
    x_c_pts= []
    
    y_c_pts= []
    
    if F_cmfr_used > 0.:    # Add a chamfer
        # set a constant for the calculations
        K_hold= F_cmfr_used * ( r_b - r_d )
        
        # Chamfer dimensions
        r_cc= K_hold * ( 1. + K_hold / ( 2. * r_d ) )   
        
        y_cc= -r_cc  
        
        x_cc= r_d + K_hold  
        
        # Angle of line to the chamfer circle center
        beta_cc= math.atan2( y_cc, x_cc )
        
        # Limit to alpha start as needed
        if beta_cc < alpha_start:
        
            beta_cc= alpha_start 
            
            x_cc= r_d * (
                            math.sqrt(
                                         math.tan(beta_cc)*math.tan(beta_cc) \
                                       + 1. 
                                      ) \
                          - math.tan(beta_cc) 
                        )
                        
            F_cmfr_used= ( x_cc - r_d ) / ( r_b - r_d )
            
            if F_cmfr_used > 1.0:
            
                F_cmfr_used= 0.0
                
                x_cc= 0.0
                
                y_cc= 0.0
                
                r_cc= 0.0
                
                beta_cc= 0.0
                
            else:
            
                y_cc= x_cc * math.tan(beta_cc)
                
                r_cc= -y_cc
                        
        if F_cmfr_used <= 1.:
            
            angle_step_c= ( 0.5 * math.pi + beta_cc ) / ( pts_c - 1. )
            
            for i in range(0, int(pts_c)):
            
                angle_curr= math.pi + beta_cc - i*angle_step_c
                
                x_c_pts.append( r_cc * math.cos(angle_curr) + x_cc )
                
                y_c_pts.append( r_cc * math.sin(angle_curr) + y_cc )
            
            alpha_c= beta_cc

            r_c= x_cc
    
    # Begin genrating the tooth profile
    x_hold= []

    y_hold= []
    
    # Generate the dedendum
    angle_step= (alpha_c-alpha_start)/(pts_d-1.) 
    
    for i in range(0,int(pts_d)):
    
        angle= alpha_start + i*angle_step
    
        x_hold.append(r_d*math.cos(angle))
    
        y_hold.append(r_d*math.sin(angle))

    # Add the chamfer
    x_hold.extend(x_c_pts)
    
    y_hold.extend(y_c_pts)
    
    # If base > dedendum add points 
    if r_b > r_d:
    
        d_len= ( r_b - r_c ) / ( pts_d_b - 1. ) 
    
        for i in range(0, int(pts_d_b)):
        
            x_hold.append( r_c + i*d_len )  
        
            y_hold.append( y_hold[-1] )
    
    # Generate the involute
    # Determine constant arc length
    d_arc= r_b * ( theta_a*theta_a - theta_d*theta_d ) / ( 2. * ( pts_i - 1. ) )

    # First point
    angle= theta_d
    
    x_hold.append(r_b*(math.cos(angle) + angle*math.sin(angle)))
    
    y_hold.append(r_b*(math.sin(angle) - angle*math.cos(angle)))
    
    for i in range(1, int(pts_i) ):
    
        angle= math.sqrt( angle*angle + 2.*d_arc/r_b )
    
        x_hold.append(r_b*(math.cos(angle) + angle*math.sin(angle)))
    
        y_hold.append(r_b*(math.sin(angle) - angle*math.cos(angle)))
    
    # Generate the addendum
    angle_step= (alpha_end-alpha_a)/(pts_a-1.)
    
    for i in range(0,int(pts_a)):
    
        angle= alpha_a + i*angle_step
    
        x_hold.append(r_a*math.cos(angle))
    
        y_hold.append(r_a*math.sin(angle))
    
    # Shift to align along the x-axis to facilitate the mirror
    x_coord= []

    y_coord= []

    for i in range(0,len(x_hold)):

        x_coord.append(
                          x_hold[i]*math.cos(-alpha_end) \
                        - y_hold[i]*math.sin(-alpha_end) \
                      )
        
        y_coord.append(
                          x_hold[i]*math.sin(-alpha_end) \
                        + y_hold[i]*math.cos(-alpha_end) \
                      )
    
    del x_hold
    
    del y_hold

    # Perform the mirror on reversed set of data to make contiguous for Blender
    x_top= [x for x in x_coord]
    
    x_top.reverse()
    
    y_top= [-x for x in y_coord]
    
    y_top.reverse()
    
    # Combine everything into a single tooth outline
    x_coord.extend(x_top)
    
    y_coord.extend(y_top)
    
    del x_top
    
    del y_top
    
    #############################################
    ################ Helical gear extras ########
    #############################################
    
    face = math.ceil(max(face_min,thk)*10.)
    
    # Make thickness an even number for a 0.2 layer thickness
    if face%2!=0:

        face += 1
    
    face /= 10.

    # Rotation information
    spin_angle= face*math.tan(p_angleHelix)/r_p
    
    steps= max(1,math.ceil(spin_angle*baseSteps/(2.*math.pi)))
    
    # Generate the external gear in Blender
    vertex_count=   generateHelicalExteriorGearBlender(
                        N,
                        x_coord,
                        y_coord,
                        spin_angle,
                        steps,
                        face,
                        hand,
                        isHerringbone,
                        name
                    )

    # Exit and return geometry information
    return (
                r_p,
                r_a,
                r_d,
                r_b,
                face,
                vertex_count,
                (F_cmfr_used,r_cc,x_cc,y_cc,beta_cc)  
            )

def get_k_a_max(N, m, pa, bl, tol_rel): 

    # Determine the relevant radii
    r_p= 0.5*m*N
    r_b= r_p*math.cos(pa)

    # Determine the circle angles to generate the involute
    theta_p= math.sqrt((r_p*r_p)/(r_b*r_b)-1.)
    
    # Translate these circle angles to involute angles
    alpha_p= theta_p - math.atan(theta_p)
    
    # Determine half the normal tooth thickness
    alpha_half_tooth= 0.5*math.pi/N
    
    # Determine the backlash angle. Backlash basis is at the pitch
    alpha_bl= bl/r_p
    
    # Maximum allowed alpha to not cross each other on tooth generation
    alpha_max= alpha_p + alpha_half_tooth - alpha_bl
    
    # Find the maximum allowed involute angle
    theta_max= theta_p
    
    for i in range(0,100):
        f=  theta_max - math.atan(theta_max) - alpha_max
        fp= (theta_max*theta_max) / (1. + theta_max*theta_max)
        
        err_rel = f / (theta_max * fp)
        
        if(math.fabs(err_rel)<(tol_rel/100.)):
            break
            
        theta_max *= (1. - err_rel)

    r_a_max= r_b * math.sqrt(theta_max*theta_max + 1.)
    
    k_a_max= (r_a_max - r_p)/m
    
    return (k_a_max, r_a_max)


def main():
    
    ############################
    ### User input parameters 
    ############################
    
    s_in=               10    # Input speed, rpm
    s_out=              20    # Output speed, rpm
    n_pa_deg=           20.0  # Normal pressure angle, degrees (standard=20)
    p_angleHelix_deg=   35.   # Pitch helical angle, degrees
    cr_min=             1.4   # Minimum required total contact ratio, normal=1.4
    mf_min=             1.20  # Minimum face contact ratio
    m_system=           1.    # Transverse Module to utilize
    N_in_min_user=      -1    # Minimum number of teeth on input gear

    bl=                 0.20  # Total backlash, evenly split between the gears
    k_a_in=             1.0   # Input gear addendum factor, standard= 1.0
    k_a_out=            1.0   # Output gear addendum factor, standard= 1.0
    k_d_in=             1.25  # Input gear dedendum factor, standard= 1.25
    k_d_out=            1.25  # Output gear dedendum factor, standard= 1.25
    
    F_cmfr=             1.0   # Chamfer factor.  Must be 0.0 <= F_cmfr <= 1.0
    
    # Note these points are used to generate only one tooth.
    # Not the complete gear. 
    pts_involute=       20    # Points used to generate half the involute
    pts_addendum=       10    # Points used to generate half the addendum
    pts_dedendum=       10    # Points used to generate half the dedendum
    pts_ded_base=       10    # Points used split from dedendum to base (if dedendum < base)
    pts_chamfer=        10    # Points used to generte the chamfer

    thick_in=           8.0   # Thickness of the input gear
    thick_out=          8.0   # Thickness of the output gear
    
    baseSteps=          180   # Number of rotational steps on 360 degree spin
    spin_in=            'R'   # Hand of input gear.
    isHerringbone=      False # Make a herringbone gear

    name_base=          'helical'   # Base name for Blender objects/meshes

    ### NOTE:   No check of existing Blendeer object/mesh names done.
    ###         Confirm no conflicts or unexpected behavior may occur 

    write_file=         False
    file_name=          'absolute_file_path'

    ############################
    ### Internal Calculations
    ############################

    name_in=            name_base + '_in'    # Blender object name for input gear

    name_out=           name_base + '_out'   # Blender object name for output gear    

    if write_file:

        with open(file_name, mode='at') as fileout:
            fileout.write(f'\n\n{"*"*50}\n')
            fileout.write(f'******** Starting New Helical Gear ({name_base}) *******\n\n')
    
    # Internal program constants
    tol_rel=        0.01  # Iteration relative tolerance in percent

    n_pa=           n_pa_deg*math.pi/180.    # Normal pressure angle in radians

    p_angleHelix=   p_angleHelix_deg*math.pi/180.
    
    # Convert from normal conditions to transverse conditions
    pa=             math.atan(math.tan(n_pa)/math.cos(p_angleHelix))

    b_angleHelix=   math.atan(math.tan(p_angleHelix)*math.cos(pa))
    
    # Minimum transverse contact ratio
    cr_min_transverse= max(1.0,cr_min-mf_min)

    # Adjust thickness if herringbone as will need to reverse spin 
    # half way through the total thickness
    gear_type = 'Helical'

    thk_factor = 1.
    
    if isHerringbone:

        thk_factor = 0.5      # Reduce input height by two

        gear_type = 'Herringbone'

    # Translate spin information
    hand_in= 1.                     # Assume right handed input gear
    
    if spin_in=="L":

        hand_in= -1.

    hand_out= -hand_in              # Make the output gear opposite hand
    
    # Set minimu face width (i.e, thickness)
    face_min=   mf_min*math.pi*m_system*thk_factor/math.tan(p_angleHelix)

    # Calculated parmaters
    sr= s_out/s_in  # speed ratio
    
    # Determine the minimum number of input gear teeth
    N_in_min_interference=  interfere_N_in_min(
                                k_a_in,
                                k_a_out,
                                sr,
                                pa
                            )

    N_in_min_contactRatio=  contactRatio_N_in_min(
                                sr,
                                pa,
                                cr_min_transverse,
                                k_a_in,
                                k_a_out,
                                tol_rel
                            )

    N_in_min=   max(
                    N_in_min_interference,
                    N_in_min_contactRatio, 
                    N_in_min_user
                )
    
    # Translate that minimum number to the speed ratio and determine actual
    # gear pair number of teeth
    sr_gcd=         math.gcd(s_in,s_out)

    N_in_base=      math.ceil(N_in_min*sr_gcd/s_out)

    N_in_actual=    N_in_base*s_out/sr_gcd

    N_out_actual=   N_in_actual/sr
    
    # Check for maximum addendum
    k_a_in_max, r_a_in_max= get_k_a_max(
                                N_in_actual, 
                                m_system, 
                                pa, 
                                0.5*bl, 
                                tol_rel
                            )

    k_a_out_max, r_a_out_max=   get_k_a_max(
                                    N_out_actual, 
                                    m_system, 
                                    pa, 
                                    0.5*bl, 
                                    tol_rel
                                )
    
    k_a_in_max*= 0.999

    k_a_out_max*= 0.999
    
    k_a_in_used= k_a_in

    k_a_out_used= k_a_out
    
    for i in range(10):
        
        # Check that not above the maximum k_a values
        if k_a_in_used > k_a_in_max or k_a_out_used > k_a_out_max:

            if k_a_in_used > k_a_in_max:

                k_a_in_used= k_a_in_max
            
            if k_a_out_used > k_a_out_max:

                k_a_out_used= k_a_out_max

            # Determine the minimum number of input gear teeth
            N_in_min_interference=  interfere_N_in_min(
                                        k_a_in_used,
                                        k_a_out_used,
                                        sr,
                                        pa
                                    )
            
            N_in_min_contactRatio=  contactRatio_N_in_min(
                                        sr,
                                        pa,
                                        cr_min_transverse,
                                        k_a_in_used,
                                        k_a_out_used,
                                        tol_rel
                                    )
                                    
            N_in_min= max(
                            N_in_min_interference,
                            N_in_min_contactRatio, 
                            N_in_min_user
                          )
            
            # Translate that minimum number to the speed ratio and determine actual
            # gear pair number of teeth
            sr_gcd=         math.gcd(s_in,s_out)

            N_in_base=      math.ceil(N_in_min*sr_gcd/s_out)

            N_in_actual=    N_in_base*s_out/sr_gcd

            N_out_actual=   N_in_actual/sr
            
            # Check for maximum addendum
            k_a_in_max, r_a_in_max= get_k_a_max(
                                        N_in_actual, 
                                        m_system, 
                                        pa, 
                                        0.5*bl, 
                                        tol_rel
                                    )

            k_a_out_max, r_a_out_max=   get_k_a_max(
                                            N_out_actual, 
                                            m_system, 
                                            pa, 
                                            0.5*bl, 
                                            tol_rel
                                        )
        
            k_a_in_max*= 0.999

            k_a_out_max*= 0.999
        
        else:
            break
    
    # Calculate the contact ratio just for the heck of it
    cr_act= contactRatioEq(
                sr,
                pa,
                k_a_in_used,
                k_a_out_used,
                N_in_actual
            )
    
    # Calculate the center-to-center distance
    cd= 0.5*m_system*N_in_actual*(sr+1.)/sr
    
    # Generate the input gear
    r_p_in,r_a_in,r_d_in,r_b_in,face_in,pts_in,c_in=    generateHelicalGear_external(
                                                            pa,
                                                            N_in_actual,
                                                            m_system,
                                                            k_a_in_used,
                                                            k_d_in,
                                                            0.5*bl,
                                                            pts_involute,
                                                            pts_addendum,
                                                            pts_dedendum,
                                                            pts_ded_base,
                                                            pts_chamfer,
                                                            F_cmfr,
                                                            thk_factor*thick_in,
                                                            face_min,
                                                            hand_in,
                                                            p_angleHelix,
                                                            baseSteps,
                                                            isHerringbone,
                                                            name_in
                                                        )

    mf_in= face_in*math.tan(p_angleHelix)/(math.pi*m_system*thk_factor)

    cr_in= cr_act + mf_in
    
    # Extract the chamfer information
    F_cmfr_used_in,r_cc_in,x_cc_in,y_cc_in,beta_cc_in= c_in    
    
    # Generate the output gear
    r_p_out,r_a_out,r_d_out,r_b_out,face_out,pts_out,c_out= generateHelicalGear_external(
                                                                pa,
                                                                N_out_actual,
                                                                m_system,
                                                                k_a_out_used,
                                                                k_d_out,
                                                                0.5*bl,
                                                                pts_involute,
                                                                pts_addendum,
                                                                pts_dedendum,
                                                                pts_ded_base,
                                                                pts_chamfer,
                                                                F_cmfr,
                                                                thk_factor*thick_out,
                                                                face_min,
                                                                hand_out,
                                                                p_angleHelix,
                                                                baseSteps,
                                                                isHerringbone,
                                                                name_out
                                                            )

    mf_out= face_out*math.tan(p_angleHelix)/(math.pi*m_system*thk_factor)

    cr_out= cr_act + mf_out
    
    # Extract the chamfer information
    F_cmfr_used_out,r_cc_out,x_cc_out,y_cc_out,beta_cc_out= c_out    

    # Determine the required input gear rotation to mesh
    # If output gear has even number of teeth, rotate 1/2 circular pitch
    if N_out_actual%2==0:

        rot_in= math.pi*m_system/(2.*r_p_in)

    else:

        rot_in= 0.0
    
    # Determine maximum shaft diameters
    d_shaft_max_in= 2.0*min(r_d_in,r_b_in)

    d_shaft_max_out= 2.0*min(r_d_out,r_b_out)
    
    # Output file information, if specified
    if write_file:
        with open(file_name, mode='at') as fileout:
            ################# Blender output information ############
            fileout.write(f'{"*"*10} New Helical Gear Run = {name_base} {"*"*10}\n\n')
            fileout.write(f'USER INPUTS:\n')
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Input Speed (rpm) =', s_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Output Speed (rpm) =', s_out))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Normal Pressure Angle (deg) =', n_pa_deg))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Pitch Helical Angle (deg) =', p_angleHelix_deg))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Total Contact Ratio Minimum =', cr_min))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Contact Face Ratio Minimum =', mf_min))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Transverse Module =', m_system))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Number of Teeth Minimum =', N_in_min_user))

            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Backlash Total =', bl))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Input Gear k_a =', k_a_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Output Gear k_a =', k_a_out))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Input Gear k_d =', k_d_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Output Gear k_d =', k_d_out))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Chamfer Factor =', F_cmfr))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Number of Points: Involue =', pts_involute))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Number of Points: Addendum =', pts_addendum))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Number of Points: Dedendum =', pts_dedendum))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Number of Points: Ded->Base =', pts_ded_base))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Number of Points: Chamfer =', pts_chamfer))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Input Gear Thickness =', thick_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Output Gear Thickness =', thick_out))
            fileout.write('\t{0:<35}{1:>12}\n\n'.format('Gear Type =', gear_type))
            
            fileout.write(f'INTERNAL CALCULATIONS:\n')
            fileout.write(f'\tMinimum Number of Teeth Check:\n')
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('Interference =', N_in_min_interference))
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('Contact Ratio =', N_in_min_contactRatio))
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('User Input =', N_in_min_user))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Transverse Contact Ratio, Req =', cr_min_transverse))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Transverse Contact Ratio, Actual =', cr_act))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Speed Ratio =', sr))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Center distance =', cd))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Transverse Pressure Angle (deg) =', pa*180./math.pi))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Normal Module =', m_system*math.cos(p_angleHelix)))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Base Helical Angle (deg) =', b_angleHelix*180./math.pi))
            fileout.write('\t{0:<35}{1:>12.4f}\n\n'.format('Minimum Face Width =', face_min/thk_factor))
            
            fileout.write(f'Input Gear Information:\n')
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Pitch radius =', r_p_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Pitch diameter =', 2.*r_p_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Base diameter =', 2.*r_b_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Addendum Diameter =', 2.0*r_a_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Dedendum Diameter =', 2.0*r_d_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Number of teeth =', N_in_actual))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('k_a =', k_a_in_used))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('k_a_max =', k_a_in_max))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('k_d =', k_d_in))
            fileout.write(f'\tChamfer Information:\n')
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('Factor =', F_cmfr_used_in))
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('Radius =', r_cc_in))
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('Center: X-dir =', x_cc_in))
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('Center: Y-dir =', y_cc_in))
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('Angle to Center =', beta_cc_in*180./math.pi))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Requested Thickness =', thick_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Actual Thickness =', face_in/thk_factor))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Axial Contact Ratio =', mf_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Total Contact Ratio =', cr_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Maximum Shaft Diameter =', d_shaft_max_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Number of vertex points =', pts_in))
            fileout.write('\t{0:<35}{1:>12.4f}\n\n'.format('Rotation to mesh (deg) =', rot_in*180./math.pi))
            
            fileout.write(f'Output Gear Information:\n')
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Pitch radius =', r_p_out))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Pitch diameter =', 2.*r_p_out))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Base diameter =', 2.*r_b_out))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Addendum Diameter =', 2.0*r_a_out))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Dedendum Diameter =', 2.0*r_d_out))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Number of teeth =', N_out_actual))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('k_a =', k_a_out_used))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('k_a_max =', k_a_out_max))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('k_d =', k_d_out))
            fileout.write(f'\tChamfer Information:\n')
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('Factor =', F_cmfr_used_out))
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('Radius =', r_cc_out))
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('Center: X-dir =', x_cc_out))
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('Center: Y-dir =', y_cc_out))
            fileout.write('\t\t{0:<31}{1:>12.4f}\n'.format('Angle to Center =', beta_cc_out*180./math.pi))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Requested Thickness =', thick_out))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Actual Thickness =', face_out/thk_factor))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Axial Contact Ratio =', mf_out))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Total Contact Ratio =', cr_out))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Maximum Shaft Diameter =', d_shaft_max_out))
            fileout.write('\t{0:<35}{1:>12.4f}\n\n'.format('Number of vertex points =', pts_out))

            fileout.write(f'********** End Helical Gear Data = {name_base} ************\n')    
            ################# Blender output information ############

    ##############################
    ##############################
    ####### USE BPY TO ALIGN GEARS
    ##############################
    ##############################
    
    # Set the veiew layer to get a context
    view_layer = bpy.context.view_layer
    view_layer.objects.active = bpy.data.objects[name_out]
    
    # Ensure in OBJECT mode
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(
            mode = 'OBJECT',
            toggle = False
            )
    else:
        
        bpy.context.window_manager.popup_menu(
            e_mode_set, 
            title=  "Oops, Sorry! Aligning Gears:", 
            icon=   'ERROR'
        )

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with mode_set.\n#####\n')
        
    # Deselect everything for a clean slate
    if bpy.ops.object.select_all.poll():
        bpy.ops.object.select_all(action='DESELECT')
    else:
        
        bpy.context.window_manager.popup_menu(
            e_deslection, 
            title=  "Oops, Sorry! Aligning Gears:", 
            icon=   'ERROR'
        )

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with deselection.\n#####\n')
    
    # Set the output gear as the selected object
    bpy.data.objects[name_out].select_set(state=True)
 
    # Move the output gear to the center distance
    bpy.context.object.location[0] = cd
 
    # Deselect everything for a clean slate
    if bpy.ops.object.select_all.poll():
        bpy.ops.object.select_all(action='DESELECT')
    else:
        
        bpy.context.window_manager.popup_menu(
            e_deslection, 
            title=  "Oops, Sorry! Aligning Gears:", 
            icon=   'ERROR'
        )

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with deselection.\n#####\n')

    # Rotate the input gear if needed
    if rot_in>0.:
        view_layer.objects.active = bpy.data.objects[name_in]
        bpy.data.objects[name_in].select_set(state=True)
        bpy.context.object.rotation_euler[2] = rot_in

    # Deselect everything for a clean slate
    if bpy.ops.object.select_all.poll():
        bpy.ops.object.select_all(action='DESELECT')
    else:
        
        bpy.context.window_manager.popup_menu(
            e_deslection, 
            title=  "Oops, Sorry! Aligning Gears:", 
            icon=   'ERROR'
        )

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with deselection.\n#####\n')
 
    # Indicate complete with gear generation    
    if write_file:
        with open(file_name, mode='at') as fileout:
            fileout.write(f'\n******** Ending New Spur Gear ({name_base}) *******\n')
            fileout.write(f'**********************************************************\n\n')

    return 0
    
# Call main if this file is executed as a script
if __name__ == '__main__':
    main()
