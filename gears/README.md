# Gears - General

# Description
These files generate different gear types as discussed in [Types](#types).  Except for the planetary gears, each file generates a pair of gears based on the user input and output speeds.  The designation of input or output gear is added to the base object name provided by the user.  For example, if the base name from the user is `main_gear` the [Blender](https://www.blender.org/) object names will be:
- Input = `main_gear_in`
- Output = `main_gear_out`

Technical explanations of the user inputs are not provided herein as there is good information on these items elsewhere (e.g, [from SDP/SI](https://www.sdp-si.com/resources/elements-of-metric-gear-technology/).  Only special information on how these parameters are implemented is provided, when needed.

The input of [Blender](https://www.blender.org/) object/mesh names and writing of output information to a file is as described in the [main README.md of this repository](/README.md).

As described in the [main README.md of this repository](/README.md), this is not a [Blender](https://www.blender.org/) Add-on and instead these files are utilized through the Scripting Workspace.  

Each file is completely self-contained.  Therefore, there is no special extraction, file location, etc. needed to use these files.  Just download the one you want and then open it in the [Blender](https://www.blender.org/) Scripting Workspace to use.

# Types

Files are provided for generating the following types of gears. Visuals are provided after the list.

- Spur
- Helical (also allows for Herringbone style)
- Bevel
    - Straight
    - Sprial
- Rack and Pinion
    - Spur
    - Helical (also allows for Herringbone style)
- Planetary
    - Spur
    - Helical (also allows for Herringbone style)

The Herringbone style of helical gear is when the tooth rotation (i.e., right/left hand) is reversed at the mid point thickness of the gear.

Spur Gears:

![Spur Gears](/gears/snapshots/gears_spur_type.png "Spur Gears"){width=50% height=50%}

Helical Gears:

![Helical Gears](/gears/snapshots/gears_helical_type.png "Helical Gears (Herringbone Style)"){width=50% height=50%}

Bevel Gears - Straight:

![Bevel Gears - Straight](/gears/snapshots/gears_bg_straight_type.png "Bevel Gears - Straight"){width=30% height=30%}

Bevel Gears - Spiral:

![Bevel Gears - Spiral](/gears/snapshots/gears_bg_spiral_type.png "Bevel Gears - Spiral"){width=30% height=30%}

Rack and Pinion Gears - Spur:

![Rack and Pinion Gears - Spur](/gears/snapshots/gears_rp_spur_type.png "Rack and Pinion Gears - Spur"){width=50% height=50%}

Rack and Pinion Gears - Helical:

![Rack and Pinion Gears - Helical](/gears/snapshots/gears_rp_helical_type.png "Rack and Pinion Gears - Helical"){width=50% height=50%}

Planetary Gears - Spur:

![Planetary Gears - Spur](/gears/snapshots/gears_pg_spur_type.png "Planetary Gears - Spur"){width=30% height=30%}

Planetary Gears - Helical:

![Planetary Gears - Helical](/gears/snapshots/gears_pg_helical_type.png "Planetary Gears - Helical (Herringbone Style)"){width=30% height=30%}

# Usage

The following provides a brief description of the various user inputs available.  The most basic gear type is the spur gear, and all of its inputs apply to the other gear types, except when noted.  The sections on the other gears will note the new specific inputs for that type of gear and not repeat the information common with the spur gear.

# Spur Gears

## Gear Speeds

The input speed (`s_in`) and output speed (`s_out`) are to be provided.  These inputs must be integers due to the use of the greatest common denominator function (`math.gcd`) when determining the number of teeth to use for the given speed ratio.  The speed ratio is `s_out/s_in`.  From a practical perspective, the actual speeds are not important, it is their ratio.  Therefore, the input and output speeds can be used to represent the speed ratio.  For example, if the gears are to have in input speed of 60 rpm and an output speed of 40 rpm, the following combination of speed values all provide the same gear set as the speed ratio is the same for all cases:
- `s_in= 60`, `s_out= 40`
- `s_in= 6`, `s_out= 4`
- `s_in= 3`, `s_out= 2`
- `s_in= 15`, `s_out= 10`

## Pressure Angle

This input (`pa_deg`) is the normal pressure angle. The input is to be in degrees and not radians.  The standard value is 20°.  This input as normal pressure angle is consistent even for other gear types (e.g, [helical](#helical-gears)) which actually generate the gear based on the transverse pressure angle.  The conversion from normal to transverse is done internally.

## Contact Ratio

This input (`cr_min`) is used in verifying interference between the input and output gear teeth.

## Module

This input (`m_system`) is the module of the gear set.  It is a type of scaling factor.  Smaller gears are generated with smaller values.  The value should be `≥ 1`.

## Minimum Input Gear Teeth

This input (`N_in_min_user`) sets the minimum number of teeth to utilize on the input gear.  The script checks for interferences between the input and output gear teeth and utilizes the smallest value to satisfy these interference checks.  This input can be used to set a higher value if desired.  To use the smallest number of teeth to satisfy the interference checks set this value to `-1`.

## Backlash

This input (`bl`) sets the gap between the gear teeth at the pitch diameter.  A value of `0` will have the teeth touch and can create binding in the gear set.  The backlash is based on the pitch diameter and larger values will decrease the tooth width at the pitch diameter. The input is the total backlash for the gear set.  Therefore, each gear (i.e, input and output) will use half this value when generating the teeth for the specific gear. Examples are shown in the following:

Backlash = 0:
![Backlash = 0](/gears/snapshots/gears_backlash_00.png "Backlash = 0"){width=50% height=50%}

Backlash = 0.25:
![Backlash = 0.25](/gears/snapshots/gears_backlash_25.png "Backlash = 0.25"){width=50% height=50%}

Backlash = 0.50:
![Backlash = 0.50](/gears/snapshots/gears_backlash_50.png "Backlash = 0.50"){width=50% height=50%}

## Tooth Addendum Factor

These factors (`k_a_in` and `k_a_out`) define the height of the addendum from the pitch diameter.  The value is a factor on the system [module](#module).  The standard value is `1.0`.  Normally the input and output gear have the same value.  However, they can be different, but this has not be fully tested.

## Tooth Dedendum Factor

These factors (`k_d_in` and `k_d_out`) define the height of the dedendum from the pitch diameter.  The value is a factor on the system [module](#module).  The standard value is `1.25`.  Normally the input and output gear have the same value.  However, they can be different, but this has not be fully tested.

## Chamfer Factor

This input (`F_cmfr`) indicates whether or not to place a chamfer at the base of the tooth at the intersection with the base circle.  The value must be between 0.0 ≤ `F_cmfr` ≤ 1.0.  This is only applied when the base circle radius is larger than the dedendum circle radius.  Therefore, setting to unity may not always add a chamfer.  The factor indicates what percentage of the distance between the dedendum to base circle radius is used to set the starting point for the chamfer.  A value of zero sets the distance at the dedendum radius (and therefore generates no chamfer) and a value of unity will set is to the base circle radius (the largest possible chamfer).  The following examples show the concept:

Multiple Tooth View:
![Chamfer - Multiple Teeth](/gears/snapshots/gears_fcmfr_far.png "Chamfer Multi-Tooth"){width=50% height=50%}

Single Tooth View:
![Chamfer - Single Tooth](/gears/snapshots/gears_fcmfr_close.png "Chamfer Single Tooth"){width=50% height=50%}

## Model Detail

The following factors allow adjustment of the model detail by setting the number of points (vertices) used to generate the various portions of the gear:

- `pts_involute`: Used to generate the tooth involute on one side of the tooth.
- `pts_addendum`: Used to generate the tooth addendum on one side of the tooth.
- `pts_dedendum`: Used to generate the tooth dedendum on one side of the tooth.
- `pts_ded_base`: Used to segment the vertical line from the dedendum to the base circle.
- `pts_chamfer` : Used to generate the [chamfer](#chamfer-factor), if used.

The higher the number of points used, the larger the model geometry and file size of any generated STL file.

## Gear Thickness

These inputs (`thick_in` and `thick_out`) set the thickness or height of each gear.

## Thickness Steps

This input (`thick_steps`) can be used break up the vertical height into segments.  This can be used to set a more uniform aspect ratio when generating an STL of the model.  If no intermediate segments are needed, set to `1` to simplfy the model geometry.  This input is only applicable to the Spur Gear.

# Helical Gears

In addition to the [Spur Gear](#spur-gears) inputs above, the following additional inputs are needed for a helical gear.

## Pitch Helical Angle

This input (`p_angleHelix_deg`) is the helical angle at the pitch diameter. This is used to set the amount of twist for the gear.  The input is to be in degrees and not radians.  The higher the value, the higher the twist.  Typically, 45° is the maximum value before the addendum starts to get cut off.  However, play around with it to see what happens.  The higher the value, the higher the value of [face contact ratio](#face-contact_ratio).  The lower the value, the thicker (i.e, taller) the gear set will need to be meet the [face contact ratio](#face-contact_ratio) input.  The script utilizes the larger of the user input thickness or the minimum thickness to meet the [face contact ratio](#face-contact_ratio) when setting the final thickness of the gear set. 

![Helical Angles](/gears/snapshots/gears_hel_angle.png "Different Helical Angles"){width=50% height=50%}

## Face Contact Ratio

This input (`mf_min`) is similar to the [contact ratio](#contact-ratio) above, except this applies to the minimum amount of contact along the tooth face due to the user input [pitch helical angle](#pitch-helical-angle).  Higher `mf_min` values will require the thicker (i.e, taller) gear sets.

## Rotation Steps

This input (`baseSteps`) indicates how many steps are to be used when making the rotations.  The higher the number of steps, the higher the model detail and the more time and memory it will take to generate the rotary lobes.  The input value is the number of steps in a full rotation (i.e, 360°).  For example, if a value of 180 is used, this will generate a detail every 2° of rotation.

## Helical Spin

This input (`spin_in`) indicates the spin rotation of the input gear. The output gear will have the opposite spin.  The inputs are right (`'R'`) or left (`'L'`).

## Herringbone Style

This input (`isHerringbone`) indicates whether to make the helical gear herringbone style or not.  The inputs are boolean `True` or `False`.  The following shows the difference between not herringbone (`False`) and herringbone (`True`) style gears.

![Herringbone](/gears/snapshots/gears_hel_herringbone.png "Herringbone Example"){width=50% height=50%}

# Bevel Gears - Straight

In addition to the [Spur Gear](#spur-gear) inputs above, the following additional inputs are needed for straight bevel gears.

## Shaft Angle

This input (`shaftAngle_deg`) indicates the angle between the two shafts.  Typically this value is 90°, but can be higher or lower as shown in the following:

![Bevel Shaft Angle](/gears/snapshots/gears_bevel_angle.png "Bevel Shaft Angle"){width=50% height=50%}

## Face

This input (`face`) is used to set a face width smaller than the internally calculated value.  It cannot be used to make the face width larger.  This parameter is similar to [gear thickness](#gear-thickness) above, but instead of a vertical distance, it is a radial distance for the width of the tooth.

## Face Factor

This input (`faceFactor`) is used to limit the maximum face width internally calculated by the script.  A typical value is to limit the maximum face with to 95% by setting this value to `0.95`.

## Inset Thickness

These inputs (`t_front_in` and `t_front_out`) set the depth of the inset on the top side of the gear.  The top surface of the gear will be down from the base circle by this amount.  Different values can be used for the input and output gears.  In the following example, the inset is 2.

![Bevel Inset](/gears/snapshots/gears_bevel_inset.png "Bevel Inset"){width=50% height=50%}

## Base Thickness

These inputs (`base_thk_in` and `base_thk_out`) are used to set the thickness of the gear.  This is defined as the distance from the top inset surface and the bottom face of the gear.  Due to the angle on the gears, this distance is limited by back cone distance (i.e, the point where the bevel angles on the teeth meet to a single point.  As such, if the user requested base thickness is larger than this possible value (set by the back cone distance), the depth is limited to this value.  Different values can be used for the input and output gears.  In the following example, the base thickness is 10.

![Bevel Base Thickness](/gears/snapshots/gears_bevel_base_thk.png "Bevel Base Thickness"){width=50% height=50%}

# Bevel Gears - Spiral

In addition to the [Straight Bevel Gear](#bevel-gears-straight) and [Spur Gear](#spur-gears) inputs above, the following additional inputs are needed for spiral bevel gears.

## Bm Angle

This input (`Bm_deg`) sets the angle of the spiral at the center of the gear tooth (i.e, the center of the face width).  The value is to be input in degrees and not radians.  The normal value is `35` but can be set to zero.

## End Angle

This input (`end_angle_deg`) sets the angle at the radial outer end of the gear.  This value must be greater than the [Bm Angle](#bm-angle). The larger the value compared to the [Bm Angle](#bm-angle) the higher the sprial bevel curvature.  This value is to be input in degrees and not radians.

## Bevel Steps

This input (`steps`) indicates how smooth to make the sprial bevel.  A higher value adds more geometry for a smoother transition at the expense of time to generate the gear and overall memory.

# Rack and Pinion Gears

In addition to the [Spur Gear](#spur-gears) and [Helical Gear](#helical-gears) inputs above, the following additional inputs are needed for rack and pinion gears.

## Number of Pinion Teeth

As there is no speed ratio for a rack and pinion gear, this input (`N_pin_min_user`) sets the minimum number of teeth on the pinion.  If the interference checks require a larger number of teeth, a larger number of teeth than input by the user will be used.

## Number of Rack Teeth

Similar to the pinion teeth, this input (`N_rack`) sets the number of rack teeth generated as this quantity is independent from the number of pinion teeth.  The more teeth, the longer the length of the rack.

## Depth of Rack

This input (`depth_rack`) sets the depth of the rack which is the distance from the base of the tooth to the bottom of the rack.  In the following example, the depth was set to 10.

![Rack Depth](/gears/snapshots/gears_rp_depth.png "Rack Depth"){width=50% height=50%}

## Rack Extensions

This input (`end_extension`) sets amount the rack will extend past the last rack tooth.  The extensions are rounded to the next whole integer to assist in 3D printing.  In the following example, the extension was set to 10.  However, to make the end point of the rack an integer value, a final extension of 10.4704 was used.

![Rack Extension](/gears/snapshots/gears_rp_extension.png "Rack Extension"){width=50% height=50%}

# Planetary Gears

Most of these inputs are already discussed in the [Spur Gear](#spur-gears) and [Helical Gear}(#helical-gears) sections above, the following additional inputs are needed for planetary gears.  The terminology used below for the gears is:
- Sun.  The single gear in the middle of the planetary gear.
- Ring. The large outer gear that encloses all the other gears.
- Planet.  The gears in between the sun and ring gears.  These are also called carrier (c) gears.

## Speed Inputs

There are three input speeds:
- Sun (`s_s`)
- Ring (`s_r`)
- Planet/Carrier (`s_c`)

Similar to the other gears discussed, these inputs must be integers.

For a particular planetary gear arrangement, either the sun or the ring must be fixed, but not both.  To fix one of the gears, set its speed value to `-1`.  The planet/carrier gears can never be fixed.  An error will be generated if the above conditions are not met.

Also, there are restrictions on the gear ratios depending on the number of [Planet Gears Per Arm](#planet-gears-per-arm) that are input. 

When `planet_gears= 1`, the following are the speed ratio restrictions:
- When the ring is fixed (`s_r= -1`):

    - The ratio of the sun to planet/carrier must be greater than two (`s_s/s_c > 2`).

- When the sun is fixed (`s_s= -1`):

    - The ratio of the ring to planet/carrier must be between one and two (`1 < s_r/s_c < 2`).
    
When `planet_gears= 2`, the following are the speed ratio restrictions:

- When the ring is fixed (`s_r= -1`):

    - There are no restrictions.

- When the sun is fixed (`s_s= -1`)

    - The ratio of the ring to the planet/carrier must be less than one (`s_r/s_c < 1`)

If the above restrictions are not met, an error is generated.

## Planetary Gear Arms Used

Typically, a planetary gear can fit different numbers of planet gears around the sun for any given speed ratio.  This input (`arms_used`) can be used to indicate the maximum number of planet gears to be generated. To generate the maximum number of planet gears use a very high value (e.g, `500`).  Not all planetary gears have the same number of allowable planet gear configurations. Some may allow five planet gears, some may not.  The script will provide the highest number of planet gears up to and including the value input by the user.  The following visualizes the configurations for a particular set of planetary gear inputs.  The inputs for this planetary gear allowed four potential arm configurations: 2, 3, 6, or 7.  No other configuration were possible.

![Arms Used](/gears/snapshots/pg_no_planet_gears.png "Arms Used"){width=50% height=50%}

## Planet Gears Per Arm

Per the [Arms Used](#planetary-gear-arms-used) discussion above, each planetary gears will have multiple arms.  This input (`planet_gears`) indicates how many planet gears are to be used per arm.  The only options are `1` and `2`.  The following visual shows the differences between the two options:

![Gears Per Arm](/gears/snapshots/pg_no_arms.png "Gears Per Arm"){width=50% height=50%}

## Ring Gear Options

The ring gear has various options for its outer geometry, insets, radial thickness, etc.  The following provides a visual of some of the inputs.

![Ring Dimensions](/gears/snapshots/pg_dimensions.png "Ring Dimensions"){width=50% height=50%}

### Planet Tooth Thickness

This input (`thk_r_tooth`) is the same as the [Gear Thickness](#gear-thickness) as discussed above.

### Base Thickness

This input (`thk_r_base`) allows for the generation of additional thickness at the bottom of the tooth to act as a base or foundation for the ring gear. A value of `0` can be input to not utilize this feature.

### Outer Edge Radial Thickness

This input (`thk_r_edge`) sets the radial thickness of the ring gear outer edge.  If a [Radial Inset](#radial-inset-thickness) is used, this edge thickness starts from the end of the inset to the outer edge.  If no [Radial Inset](#radial-inset-thickness) is used, this sets the radial thickness from the ring gear dedendum to the outer edge.

### Radial Inset Thickness

This input (`thk_r_inset`) can be used to establish a ledge in the ring gear to be used as a support for an arm connecting the planet/carrier gears or other components.  When used, this thickness starts at the ring gear dedendum and extends radially outward. 

### Axial Inset Thickness

This input (`h_r_inset`) can be used to add axial height to the top of the gear tooth.  This height can start after the [Radial Inset](#radial-inset-thicknees) or can start right at the end of the ring gear dedendum as shown in the following (i.e, when `thk_r_inset= 0`).

![Axial Inset Thickness](/gears/snapshots/pg_h_inset.png "Axial Inset Thickness"){width=50% height=50%}

### Axial Tooth Extension

This input (`h_r_tooth_top`) can be used to extend the height of a tooth.  The total tooth thickness is [Planet Tooth Thickness](#planet-tooth-thickness) indicated above plus this value.  However, this extension is always a normal extension to the tooth surface.  Therefore, when used with helical gears, the [Helical Angle](#pitch-helical-angle) is no longer used in this extension portion.

### Base Ring Radial Extension                  

This input (`thk_r_inner`) can be used to extend the base thickness radially from the ring gear addendum to the center of the ring gear.  This does not set the diameter of a hole in the ring base.  That option is done by setting the [Ring Inner Diameter](#ring-inner-diameter).  If a value of `-1` is used, the base portion is fully enclosed. The final hole diameter in the ring gear base will be the smaller of this value or the [Ring Inner Diameter](#ring-inner-diameter) but can never be less than one, unless this value is set to `-1`.  The following shows different ring base configuration with this parameter and the [Base Thickness](#base-thickness):

![Ring Base Types](/gears/snapshots/pg_base_types.png "Ring Base Types"){width=50% height=50%}

### Ring Outer Diameter

This input (`do_r_user`) can be used to set a user defined outer diameter for the ring gear.  The final ring gear outer diameter will be the larger of this value or the internally calculated value required to meet the input speed ratios.  Therefore, to disable this feature use a small value such as `1`. 

### Ring Inner Diameter

This input (`di_r_user`) can be used to set a specific hole diameter in the ring gear base.  The final hole diameter will be the smaller of this value or the diameter calculated using the [Base Ring Radial Extension](#base-ring-radial-extension).  Therefore, to disable this feature use a larger value such as `500`. 

### Shape of Outer Ring Gear

This input (`r_shape`) is used to set the shape of the outer ring gear housing.  The available options are:
- Circle (`C`)
- Pedestal (`P`)
- Octagon (`O`)
- Box (`B`)

This options are visualized in the following:

![Outer Ring Shapes](/gears/snapshots/pg_ring_shapes.png "Outer Ring Shapes"){width=50% height=50%}

## Insertion Blank

Sometimes it may be useful to insert the planetary gear into another component.  This insertion blank can be used with the Boolean - Difference operation in [Blender](https://www.blender.org/) to make a cutout in the other geometry to fit the outer ring gear.  

### Generate Blank

This input (`generate_blank`) indicates wether to generate an insertion blank (`True`) or not (`False`).  When this is generated, it completely encloses the planetary gear and it may appear the planetary gear was not generated.  This shows a generated planetary gear with an insertion blank.

![Generated Insertion Blank](/gears/snapshots/pg_blank_overall.png "Generated Insertion Blank"){width=50% height=50%}

Fear not, simply hide or move the insertion black and the planetary gear will appear.

### Insertion Blank Gap

This input (`blank_gap`) sets the gap between the generated blank and the actual ring gear housing.  This gap is applied in each plane.  The lower the gap, the tighter the tolerance between the ring gear the component it will be placed into.

![Blank Gap section](/gears/snapshots/pg_blank_section.png "Blank Gap section"){width=50% height=50%}

## The End

That summarizes the user inputs for these files.  Hopefully they will provide some value in your projects.
