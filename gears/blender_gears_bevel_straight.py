"""
PROGRAM:
blender_gears_bevel_straight.py

Copyright (C) 2024 Anders B. Thlenk

LICENSE:
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.


DESCRIPTION:
Utilize Blender <https://www.blender.org/> to auto generate a pair of bevel straight gears given various user inputs.  Refer to the "User Inputs" section of main() below.

"""


import bpy
import bmesh
import mathutils
import math

#########################################################
#########  Just error boxes
#########################################################

def e_mode_set(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with mode_set.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

def e_deslection(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with deselection.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

#########################################################
#########  Starting Gear Stuff
#########################################################


def N_in_minEq(k_a,sr,pa):
    """ Determine the minimum number of teeth on input gear
    
        Inputs:
            k_a = Addendum factor on m for input gear (normally 1)
            sr = Speed Ratio (w.output/w.input)
            pa = Pressure Angle (radians)
            
        Output:
            Minimum number of teeth to avoid interference with base circle
    """
    
    fac1= 2.*k_a*sr / ((1.+2.*sr)*math.sin(pa)*math.sin(pa))
    fac2= sr + math.sqrt(sr*sr + (1.+2.*sr)*math.sin(pa)*math.sin(pa))
    
    return fac1*fac2


def N_out_minEq(k_a,sr,pa):
    """ Determine the minimum number of teeth on output gear
    
        Inputs:
            k_a = Addendum factor on m for output gear (normally 1)
            sr = Speed Ratio (w.output/w.input)
            pa = Pressure Angle (radians)
            
        Output:
            Minimum number of teeth to avoid interference with base circle
    """
    
    fac1= 2.*k_a / ((2.+ sr)*sr*math.sin(pa)*math.sin(pa))
    fac2= 1. + math.sqrt(1. + (2.+sr)*sr*math.sin(pa)*math.sin(pa))
    
    return fac1*fac2

def interfere_N_in_min(k_a_in,k_a_out,sr,pa):
    """ Determine the minimum number of teeth on input gear taking into 
        account the speed ratio and minimum output gear teeth required
    
        Inputs:
            k_a_in  = Addendum factor on m for input gear (normally 1)
            k_a_out = Addendum factor on m for output gear (normally 1)
            sr    = Speed Ration(w.output/w.input)
            pa    = Pressure Angle (radians)
            
        Output:
            Minimum number of teeth to avoid interference with base circle
    """
    
    return max(N_in_minEq(k_a_in,sr,pa),sr*N_out_minEq(k_a_out,sr,pa))


def contactRatioEq(sr,pa,k_a_in,k_a_out,N_in):
    """ Calcuate the value for the contact ratio based on the inlet
        gear.  
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            N_in    = Number of teeth on the input gear
            
        Output:
            The contact ratio
    """
    
    fac1 = 0.25*N_in*N_in*math.sin(pa)*math.sin(pa)+N_in*k_a_in + k_a_in*k_a_in
    fac2 = 0.25*N_in*N_in*math.sin(pa)*math.sin(pa)/(sr*sr) \
            + N_in*k_a_out/sr+k_a_out*k_a_out
    
    return (math.sqrt(fac1)+math.sqrt(fac2)-0.5*math.sin(pa)*N_in*(sr+1.)/sr) \
            / (math.pi*math.cos(pa))

def cr_Nin_zeroFn(sr,pa,cr,k_a_in,k_a_out,N_in):
    """ Calcuate the value for the contact ratio zero function for the inlet
        gear.  This is the error in the solution.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            N_in    = Number of teeth on the input gear
            
        Output:
            Value of zero contact ratio function
    """
    
    fac1 = 0.25*N_in*N_in*math.sin(pa)*math.sin(pa)+N_in*k_a_in+k_a_in*k_a_in
    fac2 = 0.25*N_in*N_in*math.sin(pa)*math.sin(pa)/(sr*sr) \
            + N_in*k_a_out/sr+k_a_out*k_a_out
    
    return math.sqrt(fac1) + math.sqrt(fac2) - cr*math.pi*math.cos(pa) \
            -0.5*math.sin(pa)*N_in*(sr+1.)/sr


def cr_Nin_zeroFnPrime(sr,pa,cr,k_a_in,k_a_out,N_in):
    """ Calcuate the value for the contact ratio zero function first
        derivative for the inlet gear.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            N_in    = Number of teeth on the input gear
            
        Output:
            Value of zero contact ratio function first derivative
    """
    
    fac1 = 0.25*N_in*N_in*math.sin(pa)*math.sin(pa)+N_in*k_a_in + k_a_in*k_a_in
    fac2 = 0.25*N_in*N_in*math.sin(pa)*math.sin(pa)/(sr*sr) \
            + N_in*k_a_out/sr + k_a_out*k_a_out
    
    fac3 = (0.5*math.sin(pa)*math.sin(pa)*N_in + k_a_in)/math.sqrt(fac1)
    fac4 = (0.5*math.sin(pa)*math.sin(pa)*N_in/(sr*sr) + k_a_in/sr) \
            /math.sqrt(fac2)
    
    return fac3 + fac4 -(sr+1.)*math.sin(pa)/sr

def cr_N_in_min(sr,pa,cr,k_a_in,k_a_out,tol_rel,N_in):
    """ Solve for the required number of input gear teeth to meet input 
        parameters using Newton's Method.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            tol_rel = Relative tolerance (percent)
            N_in    = Iniital guess for number of teeth
            
        Output:
            N_in    = Actual number of teeth to meet indicated conditions
    """

    for i in range(0,100):
        err_rel = cr_Nin_zeroFn(sr,pa,cr,k_a_in,k_a_out,N_in) \
                    / (N_in*cr_Nin_zeroFnPrime(sr,pa,cr,k_a_in,k_a_out,N_in))
        
        if(math.fabs(err_rel)<(tol_rel/100.)):
            return N_in
            
        N_in *= (1. - err_rel)

    return -10

def cr_Nout_zeroFn(sr,pa,cr,k_a_in,k_a_out,N_out):
    """ Calcuate the value for the contact ratio zero function for the outlet
        gear.  This is the error in the solution.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            N_out   = Number of teeth on the output gear
            
        Output:
            Value of zero contact ratio function
    """
    
    fac1 = 0.25*N_out*N_out*math.sin(pa)*math.sin(pa)*sr*sr \
            + sr*N_out*k_a_in + k_a_in*k_a_in
    fac2 = 0.25*N_out*N_out*math.sin(pa)*math.sin(pa) \
            + N_out*k_a_out + k_a_out*k_a_out
    
    return math.sqrt(fac1) + math.sqrt(fac2) - cr*math.pi*math.cos(pa) \
            - 0.5*math.sin(pa)*N_out*(sr+1.)



def cr_Nout_zeroFnPrime(sr,pa,cr,k_a_in,k_a_out,N_out):
    """ Calcuate the value for the contact ratio zero function derivative 
        for the outlet gear.  This is the error in the solution.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            N_out   = Number of teeth on the output gear
            
        Output:
            Value of zero contact ratio function derivative
    """
    
    fac1 = 0.25*N_out*N_out*math.sin(pa)*math.sin(pa)*sr*sr \
            + sr*N_out*k_a_in + k_a_in*k_a_in
    fac2 = 0.25*N_out*N_out*math.sin(pa)*math.sin(pa) \
            + N_out*k_a_out + k_a_out*k_a_out
    
    fac3 = (0.5*sr*sr*math.sin(pa)*math.sin(pa)*N_out + sr*k_a_in) \
            / math.sqrt(fac1)
    fac4 = (0.5*math.sin(pa)*math.sin(pa)*N_out + k_a_out) / math.sqrt(fac2)

    return fac3 + fac4 - (sr+1.)*math.sin(pa)

def cr_N_out_min(sr,pa,cr,k_a_in,k_a_out,tol_rel,N_out):
    """ Solve for the required number of input gear teeth to meet input 
        parameters using Newton's Method.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            tol_rel = Relative tolerance (percent)
            N_out   = Iniital guess for number of teeth
            
        Output:
            N_out   = Actual number of teeth to meet indicated conditions
    """

    for i in range(0,100):
        err_rel = cr_Nout_zeroFn(sr,pa,cr,k_a_in,k_a_out,N_out) \
                   / (N_out*cr_Nout_zeroFnPrime(sr,pa,cr,k_a_in,k_a_out,N_out))
        
        if(math.fabs(err_rel)<(tol_rel/100.)):
            return N_out
            
        N_out *= (1. - err_rel)

    return -10

def contactRatio_N_in_min(sr,pa,cr,k_a_in,k_a_out,tol_rel):
    """ Solve for the required number of input gear teeth to meet input 
        parameters using Newton's Method.
        
        Inputs:
            sr      = Speed Ration(w.output/w.input)
            pa      = Pressure Angle (radians)
            cr      = Required contact ratio (dimensionless)
            k_a_in    = Addendum factor on m for input gear (normally 1)
            k_a_out   = Addendum factor on m for output gear (normally 1)
            tol_rel = Relative tolerance (percent)
            
        Output:
            Minimum number of input gear teeth to meet contact ratio input
    """
    return max(cr_N_in_min(sr,pa,cr,k_a_in,k_a_out,tol_rel,2.), \
                    cr_N_out_min(sr,pa,cr,k_a_in,k_a_out,tol_rel,2.)*sr)


def bevelCoordBack(vert, r_p, angleBevel):
    """ Convert plane spur gear at bevel cone end to angled bevel
    
        angleBevel in radians
    
    """
    # Get current vertex coordinates
    x_o = vert.co.x
    y_o = vert.co.y
    z_o = vert.co.z
    
    # Get current radial offset from pitch
    r_o = math.sqrt(x_o*x_o + y_o*y_o)
    dr_o = r_o - r_p
    r_n = r_p + dr_o*math.cos(angleBevel)
    z_n = z_o + dr_o*math.sin(angleBevel)
    
    theta = math.atan2(y_o,x_o)
    
    x_n = r_n*math.cos(theta)
    y_n = r_n*math.sin(theta)
    
    return (x_n, y_n, z_n)


def bevelCoordFront(vert, r_p, angleBevel, Re, face, z_base, r_p_base):
    """ Convert plane spur gear at bevel cone front to angled bevel
    
        angleBevel in radians
    
    """
    # Get current vertex coordinates based on end cone
    x_o = vert.co.x
    y_o = vert.co.y
    
    # Get current radial offset from pitch
    r_o = math.sqrt(x_o*x_o + y_o*y_o)
    dr_o = (r_o - r_p_base)*(Re - face)/Re
    r_n = r_p + dr_o*math.cos(angleBevel)
    z_n = z_base + dr_o*math.sin(angleBevel)
    
    theta = math.atan2(y_o,x_o)
    
    x_n = r_n*math.cos(theta)
    y_n = r_n*math.sin(theta)
    
    return (x_n, y_n, z_n)


def modifyBevelFace(bm,z_circle,elev,scale,normal):
    """ Modifying bevel faces """
    
    # Extrude the center area down the required amount
    face_found = 0
    
    if normal>0:
        for face in bm.faces:
            if face.normal[2]>0.99: 
                face_found = face
    else:
        for face in bm.faces:
            if face.normal[2]<-0.99: 
                face_found = face

    ret=    bmesh.ops.extrude_face_region(
                bm,
                geom = face_found.verts[:]+face_found.edges[:]+[face_found],
                edges_exclude= set(),
                use_keep_orig= False,
                use_normal_flip= False,
                use_normal_from_adjacent= False,
                use_dissolve_ortho_edges= False,
                use_select_history= False
                )
            
    geom_hold = ret['geom']
    del ret
    
    # Extract the extruded vertices
    extrude_verts = [ele for ele in geom_hold if \
                            isinstance(ele, bmesh.types.BMVert)]

    del geom_hold

    dz = z_circle - elev

    mat_loc = mathutils.Matrix.Translation((0.0, 0.0, dz))
    mat_sca = mathutils.Matrix.Scale(scale, 4,(1.0, 0.0, 0.0)) \
                @ mathutils.Matrix.Scale(scale, 4, (0.0, 1.0, 0.0))

    mat_out = mat_loc @ mat_sca

    # Transform these vertices to the proper location
    bmesh.ops.transform(
        bm,
        matrix = mat_out,
        verts = extrude_verts
        )
        
    return 0


def generateExteriorGearBlender(N,
                                r_p,
                                ba,
                                x,
                                y,
                                edge,
                                Re,
                                face_used,
                                r_p_front,
                                z_p_front,
                                z_circle_back,
                                z_d_back,
                                scale_circle_back,
                                z_circle_front,
                                z_d_front,
                                scale_circle_front,
                                ang_pts,
                                name
                                ):
    """ Generate a blender object for this one gear
    
        Inputs:
            N = Number of teeth for gear
            r_p = Pitch radius, mm
            ba  = Bevel angle, radians
            x = List of x-coordinate data for one tooth
            y = List of y-coordinate data for one tooth
            edge= List with the start [0] and end [1] or the bevel at base


            name = String for Blender object name of gear
            
        Output:
            A Blender object:
            Float: Number of vertex points on the gear.
    """

    # Blender mesh name
    name_msh=   name+"_msh"
    
    # Make a new BMesh
    bm_gear= bmesh.new()
    
    # Create the single tooth outline
    ret=    bmesh.ops.create_vert(
                bm_gear,
                co= (x[0],y[0],0.0)
            )
            
    vert_pair= ret['vert']
    
    # Make edges as the new vertics are added
    for i in range(1,len(x)):
        ret=    bmesh.ops.create_vert(
                    bm_gear,
                    co= (x[i],y[i],0.0)
                )
        
        vert_pair.extend(ret['vert'])
        
        bmesh.ops.contextual_create(
            bm_gear,
            geom=       vert_pair,
            use_smooth= False
            )
            
        vert_pair= [vert_pair[-1]]

    del ret

    # Fill in the bottom of the gear with a circle
    x_start, y_start= edge[0]
    
    ret=    bmesh.ops.create_vert(
                bm_gear,
                co= ( x_start, y_start, 0.0)
            )
    
    vert_start= ret['vert'][0]

    x_end, y_end= edge[1]
    
    ret=    bmesh.ops.create_vert(
                bm_gear,
                co= ( x_end, y_end, 0.0)
            )
    
    vert_end= ret['vert'][0]
    
    del ret
    
    radius= math.sqrt( 
                          vert_start.co.x*vert_start.co.x \
                        + vert_start.co.y*vert_start.co.y \
                     )
    
    ang_start= math.atan2(vert_start.co.y, vert_start.co.x)
    
    del_angle= -2.*ang_start / ( ang_pts + 1 )
    
    vert_prev= vert_start
    
    for i in range(1, int(ang_pts + 1) ):
        ang_curr= ang_start + i * del_angle
        
        ret=    bmesh.ops.create_vert(
                    bm_gear,
                    co= ( 
                          radius * math.cos(ang_curr) ,
                          radius * math.sin(ang_curr) ,
                          0.0
                         )
                )

        vert_new= ret['vert'][0]

        bmesh.ops.contextual_create(
            bm_gear,
            geom=       [ vert_prev, vert_new ],
            use_smooth= False
        )

        vert_prev= vert_new

    # Generate the last edge        
    bmesh.ops.contextual_create(
        bm_gear,
        geom=       [ vert_prev, vert_end ],
        use_smooth= False
    )
    
    # Now remove doubles and merge ends to make loops
    bmesh.ops.remove_doubles(
        bm_gear,
        verts=  bm_gear.verts[:],
        dist=   0.0001
    )
    
    ## This completes the single tooth profile to spin
    
    # Obtain the edges in the involute tab to fill prior to spin
    tab_edges_set= set()
    
    for vert in bm_gear.verts:
        
        angle= math.atan2(vert.co.y, vert.co.x)
        
        if angle > ang_start-0.001 and angle < -ang_start+0.001:

            for edge in vert.link_edges:

                tab_edges_set.add(edge)

    # Fill in the single involute tab
    bmesh.ops.contextual_create(
        bm_gear,
        geom=       list(tab_edges_set),
        use_smooth= False
    )
    
    del tab_edges_set
        
    # Spin the tooth to make the gear
    ret=    bmesh.ops.spin(
                bm_gear,
                geom=             bm_gear.verts[:] 
                                + bm_gear.edges[:] \
                                + bm_gear.faces[:],
                angle=          2.*math.pi*(N-1.)/N,
                steps=          int(N-1),
                axis=           (0.0, 0.0, 1.0),
                cent=           (0.0, 0.0, 0.0),
                dvec=           (0.0, 0.0, 0.0),
                use_merge=      True,
                use_duplicate=  True 
            )

    del ret
    
    # Now remove doubles and merge ends to make loops
    bmesh.ops.remove_doubles(
        bm_gear,
        verts=  bm_gear.verts[:],
        dist=   0.0001
    )

    # Update the normals
    for edge in bm_gear.edges[:]:

        edge.normal_update()

    # Obtain all the vertices on the circle
    circle_edges_set= set()
    
    for vert in bm_gear.verts:
        
        vert_radius= math.sqrt( vert.co.x*vert.co.x + vert.co.y*vert.co.y )
        
        if vert_radius < radius+0.001:

            for edge in vert.link_edges:

                circle_edges_set.add(edge)

    # Fill in the interior circle
    bmesh.ops.contextual_create(
        bm_gear,
        geom=       list(circle_edges_set),
        use_smooth= False
    )
    
    del circle_edges_set
        
    # Store the back side verts
    verts_in_back = bm_gear.verts[:]
    
    # Extrude the back side to make the front side
    ret=    bmesh.ops.extrude_face_region(
                bm_gear,
                geom=                       bm_gear.verts[:] + \
                                            bm_gear.edges[:] + \
                                            bm_gear.faces[:],
                edges_exclude=              set(),
                use_keep_orig=              False,
                use_normal_flip=            False,
                use_normal_from_adjacent=   False,
                use_dissolve_ortho_edges=   False,
                use_select_history=         False
            )
            
    geom_hold = ret['geom']
    del ret
    
    # Get the front side vertices
    verts_in_front = [ele for ele in geom_hold if isinstance(ele, \
                        bmesh.types.BMVert)]
    
    # Transform the spur gears to bevel gear on the back cone.
    for vert in verts_in_back:

        x_n, y_n, z_n = bevelCoordBack(vert, r_p, ba)

        vert.co.x = x_n

        vert.co.y = y_n

        vert.co.z = z_n
    
    # Transform the spur gears to bevel gear on the front cone.
    for vert in verts_in_front:

        x_n, y_n, z_n = bevelCoordFront(
                            vert, 
                            r_p_front,
                            ba,
                            Re,
                            face_used,
                            z_p_front,
                            r_p
                        )

        vert.co.x = x_n

        vert.co.y = y_n

        vert.co.z = z_n

    # Modify the faces to make the insets and outsets as needed
    modifyBevelFace(
        bm_gear,
        z_circle_back,
        z_d_back,
        scale_circle_back,
        -1.
    )
    
    modifyBevelFace(
        bm_gear,
        z_circle_front,
        z_d_front,
        scale_circle_front,
        1.
    )

    # Now remove doubles and merge ends to make loops
    bmesh.ops.remove_doubles(
        bm_gear,
        verts=  bm_gear.verts[:],
        dist=   0.0001
    )
    
    # Get vertex count
    vertex_count= len(bm_gear.verts[:])
    
    # Finish up, write the bmesh into a new mesh
    msh_gear= bpy.data.meshes.new(name_msh)

    bm_gear.to_mesh(msh_gear)

    bm_gear.free()

    # Add the mesh to the scene
    obj_gear= bpy.data.objects.new(name, msh_gear)

    bpy.context.collection.objects.link(obj_gear)

    return vertex_count

def generateGear_external(
        pa,
        N,
        m,
        k_a,
        k_d,
        bl,
        ba,
        face,
        face_factor,
        t_front,
        base_thk,
        pts_i,
        pts_a,
        pts_d,
        pts_d_b,
        pts_c,
        F_cmfr,
        name
    ):

    """ Return the gear radii and Blender vertex count noted below for
        the external gear
    
        Inputs:
            pa  = Pressure angle, radians
            N   = Number of teeth
            m   = Module, mm/N
            k_a = Addendum height factor, h_a = k_a*m
            k_d = Dedendum height factor, h_d = k_d*m
            bl  = Backlash associated with this tooth, mm
            ba  = Bevel angle, radians
            face  = Requested face width
            face_factor = Factor of maximum recommeded face
            pts_i = Number of points for involue
            pts_a = Number of points for addendum
            pts_d = Number of points for dedendum
            pts_d_b= Number of points for dedendum -> base (if dedendum < base )
            name  = Blender object name for gear
            
        Outputs:
            Tuple, (r_p,r_a,r_d,r_b,vertex_count)
            
        Note, total tooth height h = h_a + h_d = (k_a + k_d)*m
        Note, Blender mesh name for gear will be name+'_msh'

        Circle Involute Parametric Equation from:
            Weisstein, Eric W. "Circle Involute." From MathWorld--A Wolfram Web Resource. 
            https://mathworld.wolfram.com/CircleInvolute.html

    """
    
    # Determine the relevant radii
    r_p= 0.5*m*N

    r_a= r_p + k_a*m

    r_d= r_p - k_d*m

    r_b= r_p*math.cos(pa)

    # Set chamber factor (value must be between 0 -> 1.0 )
    F_cmfr_used= max(min(F_cmfr, 1.0), 0.0)

    # Check bottom starting point. Base or dedendum
    r_start= r_b
    
    if r_b<r_d:
        
        r_start= r_d
        
        F_cmfr_used= 0.
    
    # Determine the circle angles to generate the involute
    theta_a= math.sqrt((r_a*r_a)/(r_b*r_b)-1.)

    theta_p= math.sqrt((r_p*r_p)/(r_b*r_b)-1.)

    theta_d= math.sqrt((r_start*r_start)/(r_b*r_b)-1.)
    
    # Translate these circle angles to involute angles
    alpha_a= theta_a - math.atan(theta_a)

    alpha_p= theta_p - math.atan(theta_p)

    alpha_d= theta_d - math.atan(theta_d)
    
    # Determine the backlash angle. Backlash basis is at the pitch
    alpha_bl= bl/r_p
    
    # Determine the tooth thickness.
    # Hard coded value of 0.5 circular pitch without backlash
    # The backlash reduces the tooth thickness
    del_alpha_tooth= math.pi/(2.*N) - alpha_bl
    
    # Generate the actual start and end angles around the involute angles
    # Prior to rotating, 0 = start point for involute on the base
    alpha_end=      alpha_p + del_alpha_tooth

    alpha_start=    alpha_end - math.pi/N

    # Check for chamfer
    alpha_c= alpha_d

    r_c= r_d

    x_cc= 0.0       # x-location of circle center
    
    y_cc= 0.0       # y-location of circle center
    
    r_cc= 0.0       # Chamfer circle radius
    
    beta_cc= 0.0    # Angle to chamfer center
    
    x_c_pts= []
    
    y_c_pts= []
    
    if F_cmfr_used > 0.:    # Add a chamfer
        # set a constant for the calculations
        K_hold= F_cmfr_used * ( r_b - r_d )
        
        # Chamfer dimensions
        r_cc= K_hold * ( 1. + K_hold / ( 2. * r_d ) )   
        
        y_cc= -r_cc  
        
        x_cc= r_d + K_hold  
        
        # Angle of line to the chamfer circle center
        beta_cc= math.atan2( y_cc, x_cc )
        
        # Limit to alpha start as needed
        # To avoid issues with generating an inset, limit to 95% of angle start
        if beta_cc < 0.95*alpha_start:
        
            beta_cc= 0.95*alpha_start 
            
            x_cc= r_d * (
                            math.sqrt(
                                         math.tan(beta_cc)*math.tan(beta_cc) \
                                       + 1. 
                                      ) \
                          - math.tan(beta_cc) 
                        )
                        
            F_cmfr_used= ( x_cc - r_d ) / ( r_b - r_d )
            
            if F_cmfr_used > 1.0:
            
                F_cmfr_used= 0.0
                
                x_cc= 0.0
                
                y_cc= 0.0
                
                r_cc= 0.0
                
                beta_cc= 0.0
                
            else:
            
                y_cc= x_cc * math.tan(beta_cc)
                
                r_cc= -y_cc
                        
        if F_cmfr_used <= 1.:
            
            angle_step_c= ( 0.5 * math.pi + beta_cc ) / ( pts_c - 1. )
            
            for i in range(0, int(pts_c)):
            
                angle_curr= math.pi + beta_cc - i*angle_step_c
                
                x_c_pts.append( r_cc * math.cos(angle_curr) + x_cc )
                
                y_c_pts.append( r_cc * math.sin(angle_curr) + y_cc )
            
            alpha_c= beta_cc

            r_c= x_cc
    
    # Begin generating the tooth profile
    x_hold= []

    y_hold= []
    
    # Generate the dedendum
    angle_step= (alpha_c-alpha_start)/(pts_d-1.)

    for i in range(0,int(pts_d)):

        angle= alpha_start + i*angle_step

        x_hold.append(r_d*math.cos(angle))

        y_hold.append(r_d*math.sin(angle))

    # Add the chamfer
    x_hold.extend(x_c_pts)
    
    y_hold.extend(y_c_pts)
    
    # If base > dedendum add points 
    if r_b > r_d:

        d_len= ( r_b - r_c ) / ( pts_d_b - 1. )

        for i in range(0, int(pts_d_b)):

            x_hold.append( r_c + i*d_len )

            y_hold.append( y_hold[-1] )
        
    # Generate the involute
    # Determine constant arc length
    d_arc= r_b * ( theta_a*theta_a - theta_d*theta_d ) / ( 2. * ( pts_i - 1. ) )

    # First point
    angle= theta_d

    x_hold.append(r_b*(math.cos(angle) + angle*math.sin(angle)))

    y_hold.append(r_b*(math.sin(angle) - angle*math.cos(angle)))
    
    for i in range(1, int(pts_i) ):

        angle= math.sqrt( angle*angle + 2.*d_arc/r_b )

        x_hold.append(r_b*(math.cos(angle) + angle*math.sin(angle)))

        y_hold.append(r_b*(math.sin(angle) - angle*math.cos(angle)))
    
    # Generate the addendum
    angle_step= (alpha_end-alpha_a)/(pts_a-1.)

    for i in range(0,int(pts_a)):

        angle= alpha_a + i*angle_step

        x_hold.append(r_a*math.cos(angle))

        y_hold.append(r_a*math.sin(angle))
    
    # Shift to align along the x-axis to facilitate the mirror
    x_coord= []

    y_coord= []

    for i in range(0,len(x_hold)):

        x_coord.append(
                          x_hold[i] * math.cos(-alpha_end) \
                        - y_hold[i] * math.sin(-alpha_end) \
                      )
        
        y_coord.append(
                          x_hold[i] * math.sin(-alpha_end) \
                        + y_hold[i] * math.cos(-alpha_end) \
                      )
    
    del x_hold

    del y_hold

    # Perform the mirror on reversed set of data to make contiguous for Blender
    x_top= [x for x in x_coord]

    x_top.reverse()
    
    y_top= [-x for x in y_coord]

    y_top.reverse()
    
    # Combine everything into a single tooth outline
    x_coord.extend(x_top)

    y_coord.extend(y_top)
    
    del x_top

    del y_top
    
    # Get the base edge bevel line end points
    alpha_edge= alpha_c - alpha_end

    x_edge= r_d * math.cos(alpha_edge)
    
    y_edge= r_d * math.sin(alpha_edge)
    
    edge= [ (x_edge, y_edge), (x_edge, -y_edge) ]
    
    ######################################
    ### Determine Bevel Specific Geometry
    ######################################
    
    # Determine the cone distance
    Re= r_p/math.sin(ba)

    # Actual face width used in the generation
    face_used=  min(face,face_factor*min(Re/3.,10.*m))
    
    # Front Pitch Information
    r_p_front=  r_p - face_used*math.sin(ba)

    z_p_front=  face_used*math.cos(ba)
    
    # Front addendum information
    r_a_front=  r_p_front + (r_a-r_p)*(Re-face_used)*math.cos(ba)/Re

    z_a_front=  z_p_front + (r_a-r_p)*(Re-face_used)*math.sin(ba)/Re

    # Front dedendum information
    r_d_front=  r_p_front - (r_p-r_d)*(Re-face_used)*math.cos(ba)/Re

    z_d_front=  z_p_front - (r_p-r_d)*(Re-face_used)*math.sin(ba)/Re

    # Front circle information
    r_circle_front= r_d_front - t_front*math.cos(ba)

    z_circle_front= z_d_front - t_front*math.sin(ba)
    
    scale_circle_front= r_circle_front/r_d_front

    # Back Pitch Information
    r_p_back=  r_p

    z_p_back=  0.0
    
    # Back addendum information
    r_a_back=  r_p_back + (r_a-r_p)*math.cos(ba)

    z_a_back=  z_p_back + (r_a-r_p)*math.sin(ba)

    # Back dedendum information
    r_d_back=  r_p_back - (r_p-r_d)*math.cos(ba)

    z_d_back=  z_p_back - (r_p-r_d)*math.sin(ba)

    # Back circle information
    # Check to make sure the requested thickness is not smaller than
    #    what is possible
    
    # Determine the two possible z_circle_back values
    # Minimum z_circle is for z_circle_back = z_d_back
    z_circle_back_min= z_d_back # @ t_back_out= 0    
    
    # Maximum z_circle is to limit circle to no smaller than inlet circle
    #   (i.e, r_circle_back = r_circle_front)
    z_circle_back_max= z_d_back + math.tan(ba)*(r_circle_front - r_d_back)

    # Based on these z_circle_back ranges, the range in base thickness is:
    base_thk_max=   z_circle_front - z_circle_back_max    

    base_thk_min=   z_circle_front - z_circle_back_min  
    
    # Based on these ranges, set the final base thickness
    base_thk_used= 0
    
    if base_thk<base_thk_min:

        base_thk_used= math.ceil(base_thk_min*10.)
        
        # Make thickness an even number for a 0.2 layer thickness
        if base_thk_used%2!=0:

            base_thk_used += 1
    
        base_thk_used /= 10.
        
    elif base_thk>base_thk_max:

        base_thk_used= math.floor(base_thk_max*10.)
        
        # Make thickness an even number for a 0.2 layer thickness
        if base_thk_used%2!=0:

            base_thk_used -= 1
    
        base_thk_used /= 10.
    
    else:    

        base_thk_used= base_thk
    
    # Now continue with evaluation
    z_circle_back= z_circle_front - base_thk_used

    t_back= (z_d_back - z_circle_back)/math.sin(ba)

    r_circle_back= r_d_back - t_back*math.cos(ba)
    
    scale_circle_back=  r_circle_back/r_d_back

    # Combine the radii and elevation data for return
    r_front=    [r_a_front, r_p_front, r_d_front, r_circle_front]

    r_back=     [r_a_back , r_p_back , r_d_back , r_circle_back]
    
    z_front=    [z_a_front, z_p_front, z_d_front, z_circle_front]

    z_back=     [z_a_back , z_p_back , z_d_back , z_circle_back]
    
    # Determine the maximum shaft diameter
    d_max_shaft= 2.*r_circle_front
    
    if r_circle_front>r_circle_back:

        d_max_shaft= 2.*r_circle_back
    
    # Generate the external gear in Blender
    vertex_count=   generateExteriorGearBlender(
                        N,
                        r_p,
                        ba,
                        x_coord,
                        y_coord,
                        edge,
                        Re,
                        face_used,
                        r_p_front,
                        z_p_front,
                        z_circle_back,
                        z_d_back,
                        scale_circle_back,
                        z_circle_front,
                        z_d_front,
                        scale_circle_front,
                        pts_a,
                        name
                    )
    
    # Exit and return geometry information
    return (r_p, r_a, r_d, r_b, Re, face_used, t_back, r_front, r_back, \
            z_front, z_back, d_max_shaft, base_thk_used, z_circle_back_max, \
            base_thk_min, base_thk_max, vertex_count, \
            (F_cmfr_used,r_cc,x_cc,y_cc,beta_cc) )


def main():

    ############################
    ### User input parameters 
    ############################
    
    s_in=           25    # Input speed, rpm (must be integer)
    s_out=          20    # Output speed, rpm (must be integer)
    pa_deg=         20.0  # Pressure angle, degrees (standard=20)
    cr_min=         1.4   # Minimum required contact ratio, normal=1.4
    m_system=       2.    # Module to utilize
    N_in_min_user=  -1    # Minimum number of teeth on input gear

    bl=             0.20  # Total backlash, evenly split between the gears
    k_a_in=         1.0   # Input gear addendum factor, standard= 1.0
    k_a_out=        1.0   # Output gear addendum factor, standard= 1.0
    k_d_in=         1.25  # Input gear dedendum factor, standard= 1.25
    k_d_out=        1.25  # Output gear dedendum factor, standard= 1.25
    
    F_cmfr=         0.5   # Chamfer factor.  Must be 0.0 <= F_cmfr <= 1.0
    
    shaftAngle_deg= 90.0  # Shaft angle in degrees (not checked at other angles)
    face=           13.0  # Requested face width
    faceFactor=     0.95  # Factor of maximum recommeded face

    # Note these points are used to generate only one tooth.
    # Not the complete gear. 
    pts_involute=   40    # Points used to generate half the involute
    pts_addendum=   10    # Points used to generate half the addendum
    pts_dedendum=   10    # Points used to generate half the dedendum
    pts_ded_base=   10    # Points used split from dedendum to base (if dedendum < base)
    pts_chamfer=    10    # Points used to generte the chamfer
    
    t_front_in=     2.0   # Inset thickness to top face
    t_front_out=    2.0   # Inset thickness to top face

    base_thk_in=    6.0   # Final base thickness of the input gear
    base_thk_out=   10.0   # Final base thickness of the input gear

    name=           'straight_bevel'    # Blender object primary name gears

    ### NOTE:   No check of existing Blendeer object/mesh names done.
    ###         Confirm no conflicts or unexpected behavior may occur 

    write_file=     False
    file_name=      'absolute_file_path'

    ############################
    ### Internal Calculations
    ############################

    name_in=    name + '_in'    # Input gear Blender name
    name_out=   name + '_out'   # Output gear Blender name
    
    if write_file:
        with open(file_name, mode='at') as fileout:
            fileout.write(f'\n\n{"*"*50}\n')
            fileout.write(f'**** Starting New Straight Bevel Gear ({name}) ****\n\n')
    
    # Internal program constants
    tol_rel=    0.01  # Iteration relative tolerance in percent
    pa=         pa_deg*math.pi/180.    # Pressure angle in radians
    shaftAngle= shaftAngle_deg*math.pi/180.  # Shaft angle in radians

    # Calculated parmaters
    sr= s_out/s_in  # speed ratio
    
    # Determine the minimum number of input gear teeth
    N_in_min_interference=  interfere_N_in_min(k_a_in,k_a_out,sr,pa)
    N_in_min_contactRatio=  contactRatio_N_in_min(sr,pa,cr_min,k_a_in, \
                                                    k_a_out,tol_rel)
    N_in_min=   max(N_in_min_interference,N_in_min_contactRatio, N_in_min_user)
    
    # Translate that minimum number to the speed ratio and determine actual
    # gear pair number of teeth
    sr_gcd=         math.gcd(s_in,s_out)
    N_in_base=      math.ceil(N_in_min*sr_gcd/s_out)
    N_in_actual=    N_in_base*s_out/sr_gcd
    N_out_actual=   N_in_actual/sr
    
    # Calculate the contact ratio just for the heck of it
    cr_act= contactRatioEq(sr,pa,k_a_in,k_a_out,N_in_actual)
    
    #Bevel angles
    ba_in=  math.atan(math.sin(shaftAngle) \
                        / ((N_out_actual/N_in_actual)+math.cos(shaftAngle)))
    
    ba_out= math.atan(math.sin(shaftAngle) \
                        / ((N_in_actual/N_out_actual)+math.cos(shaftAngle)))

    # Generate the input gear
    gear_data_in = generateGear_external(
                                            pa,
                                            N_in_actual,
                                            m_system,
                                            k_a_in,
                                            k_d_in,
                                            0.5*bl,
                                            ba_in,
                                            face,
                                            faceFactor,
                                            t_front_in,
                                            base_thk_in,
                                            pts_involute,
                                            pts_addendum,
                                            pts_dedendum,
                                            pts_ded_base,
                                            pts_chamfer,
                                            F_cmfr,
                                            name_in
                                        )

    
    r_p_in= gear_data_in[0]
    
    # Extract the chamfer information
    F_cmfr_used_in,r_cc_in,x_cc_in,y_cc_in,beta_cc_in= gear_data_in[-1]    

    # Generate the output gear
    gear_data_out=  generateGear_external(
                                            pa,
                                            N_out_actual,
                                            m_system,
                                            k_a_out,
                                            k_d_out,
                                            0.5*bl,
                                            ba_out,
                                            face,
                                            faceFactor,
                                            t_front_out,
                                            base_thk_out,
                                            pts_involute,
                                            pts_addendum,
                                            pts_dedendum,
                                            pts_ded_base,
                                            pts_chamfer,
                                            F_cmfr,
                                            name_out
                                        )
    
    # Extract the chamfer information
    F_cmfr_used_out,r_cc_out,x_cc_out,y_cc_out,beta_cc_out= gear_data_out[-1]    
    
    # Determine the required input gear rotation to mesh
    # If output gear has even number of teeth, rotate 1/2 circular pitch
    if N_out_actual%2==0:

        rot_in= math.pi*m_system/(2.*r_p_in)

    else:

        rot_in= 0.0
    
    # Determine the enclosing box:
    overall_width_in=   2.*gear_data_in[8][0]

    overall_height_in=  gear_data_in[9][0] - gear_data_in[10][3]
    
    overall_width_out=  2.*gear_data_out[8][0]

    overall_height_out= gear_data_out[9][0] - gear_data_out[10][3]
        
    if write_file:
        with open(file_name, mode='at') as fileout:
            ################# Blender output information ############
            fileout.write(f'{"#"*8} New Straight Bevel Gear Run = {name} {"#"*8}\n\n')
            fileout.write(f'USER INPUTS:\n')
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Input Speed (rpm) =', s_in))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Output Speed (rpm) =', s_out))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Pressure Angle (deg) =', pa_deg))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Contact Ratio Minimum =', cr_min))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Module =', m_system))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Number of Teeth Minimum =', N_in_min_user))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Backlash Total =', bl))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Input Gear k_a =', k_a_in))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Output Gear k_a =', k_a_out))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Input Gear k_d =', k_d_in))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Output Gear k_d =', k_d_out))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Shaft Angle (deg) =', shaftAngle_deg))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Requested Face Width =', face))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Maximum Face Factor =', faceFactor))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Chamfer Factor =', F_cmfr))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Number of Points: Involue =', pts_involute))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Number of Points: Addendum =', pts_addendum))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Number of Points: Dedendum =', pts_dedendum))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Number of Points: Ded->Base =', pts_ded_base))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Number of Points: Chamfer =', pts_chamfer))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Input Gear Top Face Inset Thickness =', t_front_in))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Output Gear Top Face Inset Thickness =', t_front_out))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Input Gear Base Thickness =', base_thk_in))
            fileout.write('\t{0:<40}{1:>12.4f}\n\n'.format('Output Gear Base Thickness =', base_thk_out))
            
            fileout.write(f'INTERNAL CALCULATIONS:\n')
            fileout.write(f'\tMinimum Number of Teeth Check:\n')

            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Interference =', N_in_min_interference))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Contact Ratio =', N_in_min_contactRatio))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('User Input =', N_in_min_user))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Contact Ratio Actual =', cr_act))
            fileout.write('\t{0:<40}{1:>12.4f}\n\n'.format('Speed Ratio =', sr))

            fileout.write(f'Input Gear Information:\n')
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Number of teeth =', N_in_actual))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Center Distance From Apex =', gear_data_out[0]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Cone Distance =', gear_data_in[4]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Face Width =', gear_data_in[5]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Maximum Shaft Diameter= ', gear_data_in[11]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Actual Base Thickness= ', gear_data_in[12]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Minimum Base Thickness= ', gear_data_in[14]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Maximum Base Thickness= ', gear_data_in[15]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Bottom Inset Thickness =', gear_data_in[6]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Maximum Inset Circle Elevation =', gear_data_in[13]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Bevel Angle (deg) =', ba_in*180./math.pi))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Overall Width =', overall_width_in))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Overall Height =', overall_height_in))
            fileout.write(f'\tTop Face Dimensions:\n')
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius - Addendum = ', gear_data_in[7][0]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius - Pitch = ', gear_data_in[7][1]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius - Dedendum = ', gear_data_in[7][2]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n\n'.format('Radius - Inset Circle = ', gear_data_in[7][3]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Elevation - Addendum = ', gear_data_in[9][0]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Elevation - Pitch = ', gear_data_in[9][1]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Elevation - Dedendum = ', gear_data_in[9][2]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n\n'.format('Elevation - Inset Circle = ', gear_data_in[9][3]))
            fileout.write(f'\tBottom Face Dimensions:\n')
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius - Addendum = ', gear_data_in[8][0]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius - Pitch = ', gear_data_in[8][1]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius - Dedendum = ', gear_data_in[8][2]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n\n'.format('Radius - Inset Circle = ', gear_data_in[8][3]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Elevation - Addendum = ', gear_data_in[10][0]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Elevation - Pitch = ', gear_data_in[10][1]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Elevation - Dedendum = ', gear_data_in[10][2]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n\n'.format('Elevation - Inset Circle = ', gear_data_in[10][3]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('k_a =', k_a_in))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('k_d =', k_d_in))
            fileout.write(f'\tChamfer Information:\n')
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Factor =', F_cmfr_used_in))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius =', r_cc_in))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Center: X-dir =', x_cc_in))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Center: Y-dir =', y_cc_in))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Angle to Center =', beta_cc_in*180./math.pi))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Number of vertex points =', gear_data_in[16]))
            fileout.write('\t{0:<40}{1:>12.4f}\n\n'.format('Rotation to mesh (deg) =', rot_in*180./math.pi))
            
            fileout.write(f'Output Gear Information:\n')
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Number of teeth =', N_out_actual))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Center Distance From Apex =', r_p_in))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Cone Distance =', gear_data_out[4]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Face Width =', gear_data_out[5]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Maximum Shaft Diameter= ', gear_data_out[11]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Actual Base Thickness= ', gear_data_out[12]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Minimum Base Thickness= ', gear_data_out[14]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Maximum Base Thickness= ', gear_data_out[15]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Bottom Inset Thickness =', gear_data_out[6]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Maximum Inset Circle Elevation =', gear_data_out[13]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Bevel Angle (deg) =', ba_out*180./math.pi))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Overall Width =', overall_width_out))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Overall Height =', overall_height_out))
            fileout.write(f'\tTop Face Dimensions:\n')
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius - Addendum = ', gear_data_out[7][0]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius - Pitch = ', gear_data_out[7][1]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius - Dedendum = ', gear_data_out[7][2]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n\n'.format('Radius - Inset Circle = ', gear_data_out[7][3]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Elevation - Addendum = ', gear_data_out[9][0]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Elevation - Pitch = ', gear_data_out[9][1]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Elevation - Dedendum = ', gear_data_out[9][2]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n\n'.format('Elevation - Inset Circle = ', gear_data_out[9][3]))
            fileout.write(f'\tBottom Face Dimensions:\n')
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius - Addendum = ', gear_data_out[8][0]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius - Pitch = ', gear_data_out[8][1]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius - Dedendum = ', gear_data_out[8][2]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n\n'.format('Radius - Inset Circle = ', gear_data_out[8][3]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Elevation - Addendum = ', gear_data_out[10][0]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Elevation - Pitch = ', gear_data_out[10][1]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Elevation - Dedendum = ', gear_data_out[10][2]))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n\n'.format('Elevation - Inset Circle = ', gear_data_out[10][3]))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('k_a =', k_a_out))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('k_d =', k_d_out))
            fileout.write(f'\tChamfer Information:\n')
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Factor =', F_cmfr_used_out))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Radius =', r_cc_out))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Center: X-dir =', x_cc_out))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Center: Y-dir =', y_cc_out))
            fileout.write('\t\t{0:<36}{1:>12.4f}\n'.format('Angle to Center =', beta_cc_out*180./math.pi))
            fileout.write('\t{0:<40}{1:>12.4f}\n'.format('Number of vertex points =', gear_data_out[11]))

            fileout.write(f'********** End Straight Bevel Gear Data = {name} ************\n')    
            ################# Blender output information ############

    ##############################
    ##############################
    ####### USE BPY TO ALIGN GEARS
    ##############################
    ##############################
    
    # Set the veiew layer to get a context
    view_layer=                 bpy.context.view_layer
    view_layer.objects.active=  bpy.data.objects[name_in]
    
    # Ensure in OBJECT mode
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(
                                mode=   'OBJECT',
                                toggle= False
                                )
    else:

        bpy.context.window_manager.popup_menu(
            e_mode_set, 
            title=  "Oops, Sorry! Aligning Gears:", 
            icon=   'ERROR'
        )
    
        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with mode_set.\n#####\n')
        
    # Deselect everything for a clean slate
    if bpy.ops.object.select_all.poll():
        bpy.ops.object.select_all(action='DESELECT')
    else:

        bpy.context.window_manager.popup_menu(
            e_deslection, 
            title=  "Oops, Sorry! Aligning Gears:", 
            icon=   'ERROR'
        )

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with deselection.\n#####\n')
    
    # Set the input gear as the selected object
    bpy.data.objects[name_in].select_set(state=True)
 
    # Move the input gear to the alignment location
    bpy.context.object.location[0]=         -gear_data_out[0] + r_p_in * math.sin(shaftAngle - 0.5*math.pi)
    bpy.context.object.location[2]=         r_p_in * math.cos(shaftAngle - 0.5*math.pi)
    bpy.context.object.rotation_euler[1]=   shaftAngle
    bpy.context.object.rotation_mode=       'ZXY'

    # Rotate the input gear if needed
    if rot_in>0.:
        bpy.context.object.rotation_euler[2]=   rot_in

    # Deselect everything for a clean slate
    if bpy.ops.object.select_all.poll():
        bpy.ops.object.select_all(action='DESELECT')
    else:

        bpy.context.window_manager.popup_menu(
            e_deslection, 
            title=  "Oops, Sorry! Aligning Gears:", 
            icon=   'ERROR'
        )

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with deselection.\n#####\n')
 
    # Indicate complete with gear generation    
    if write_file:
        with open(file_name, mode='at') as fileout:
            fileout.write(f'\n******** Ending New Straight Bevel Gear ({name}) *******\n')
            fileout.write(f'**********************************************************\n\n')
    
    return 0
    
# Call main if this file is executed as a script
if __name__ == '__main__':
    main()
