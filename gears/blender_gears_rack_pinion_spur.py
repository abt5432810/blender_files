"""
PROGRAM:
blender_gears_rack_pinion_spur.py

Copyright (C) 2024 Anders B. Thlenk

LICENSE:
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.


DESCRIPTION:
Utilize Blender <https://www.blender.org/> to auto generate a rack and pinion set based on spur gears given various user inputs.  Refer to the "User Inputs" section of main() below.

"""

import bpy
import bmesh
import math

#########################################################
#########  Just error boxes
#########################################################

def e_mode_set(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with mode_set.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

def e_deslection(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with deselection.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

#########################################################
#########  Starting Gear Stuff
#########################################################

def rp_contactRatio(Np,pa,k_p,k_r):
    """ Defining the contact ratio for rack and pinion gears.
    
        Inputs:
            Np      = Number of teeth on the pinion
            pa      = Pressure angle, radians
            k_p     = Pinion addendum factor, dimensionless (normally 1.0)
            k_r     = Rack addendum factor, dimensionless (normally 1.0)
            
        Output:
            Contact ratio
    """
   
    fac1 = math.sqrt(Np*Np*math.sin(pa)*math.sin(pa) +4.*k_p*Np +4.*k_p*k_p)
    
    return (fac1 + 2.*k_r/math.sin(pa)-Np*math.sin(pa))/(2*math.pi*math.cos(pa))
    

def rp_Np_min_cr(cr,pa,k_p,k_r,tol_rel,Np):
    """ Determine required number of pinion teeth to meet the indicated 
        contact ratio.
        
        Inputs:
            cr      = Contact ratio
            pa      = Pressure angle, radians
            k_p     = Pinion addendum factor, dimensionless (normally 1.0)
            k_r     = Rack addendum factor, dimensionless (normally 1.0)
            tol_rel = Relative tolerance (percent)
            Np      = Guess for the number of teeth on the pinion
            
        Output:
            Actual required pinion teeth values to meet the indicated 
            contact ratio
    """

    for i in range(0,5):
        fn = math.sqrt(Np*Np*math.sin(pa)*math.sin(pa) + 4.*k_p*Np \
                + 4.*k_p*k_p) + 2.*k_r/math.sin(pa) - Np*math.sin(pa) \
                - 2*cr*math.pi*math.cos(pa)
        
        fnPrime = 0.5*(2.*Np*math.sin(pa)*math.sin(pa) + 4./k_p) \
                   /math.sqrt(Np*Np*math.sin(pa)*math.sin(pa) \
                   + 4.*k_p*Np+4.*k_p*k_p) - math.sin(pa)  
        
        err_rel = fn / (Np*fnPrime)
        
        if(math.fabs(err_rel)<(tol_rel/100.)):
            return Np
            
        Np *= (1. - err_rel)

    return -10


def generateExteriorGearBlender(N,x,y,thk,fill,name):
    """ Generate a blender object for this one gear
    
        Inputs:
            N = Number of teeth for gear
            x = List of x-coordinate data for one tooth
            y = List of y-coordinate data for one tooth
            thk = If filled, provide some thickness. -1 if just keep as a plane
            fill = Fill in the gear, boolean
            name = String for Blender object name of gear
            
        Output:
            A Blender object:
            Float: Number of vertex points on the gear.
    """

    # Blender mesh name
    name_msh=   name+"_msh"
    
    # Make a new BMesh
    bm_gear= bmesh.new()
    
    # Create the single tooth outline
    ret=    bmesh.ops.create_vert(
                bm_gear,
                co= (x[0],y[0],0.0)
            )
            
    vert_pair= ret['vert']
    
    # Make edges as the new vertics are added
    for i in range(1,len(x)):
        ret=    bmesh.ops.create_vert(
                    bm_gear,
                    co= (x[i],y[i],0.0)
                )
        
        vert_pair.extend(ret['vert'])
        
        bmesh.ops.contextual_create(
            bm_gear,
            geom=       vert_pair,
            use_smooth= False
            )
            
        vert_pair= [vert_pair[-1]]

    del ret
    
    # Now remove doubles and merge ends to make loops
    bmesh.ops.remove_doubles(
        bm_gear,
        verts=  bm_gear.verts[:],
        dist=   0.0001)
    
    # Spin the tooth to make the gear
    ret = bmesh.ops.spin(
        bm_gear,
        geom=           bm_gear.verts[:] + bm_gear.edges[:],
        angle=          2.*math.pi*(N-1.)/N,
        steps=          int(N-1),
        axis=           (0.0, 0.0, 1.0),
        cent=           (0.0, 0.0, 0.0),
        dvec=           (0.0, 0.0, 0.0),
        use_merge=      True,
        use_duplicate=  True 
        )

    del ret
    
    # Now remove doubles and merge ends to make loops
    bmesh.ops.remove_doubles(
        bm_gear,
        verts=  bm_gear.verts[:],
        dist=   0.0001)
   
    if fill:
        # Enclose the gear outline with a face
        bmesh.ops.contextual_create(
            bm_gear,
            geom=       bm_gear.edges[:],
            use_smooth= False
            )

        if thk>0.:
            # Extrude the face the required thickness
            ret= bmesh.ops.extrude_face_region(
                    bm_gear,
                    geom=                       bm_gear.verts[:] \
                                                    + bm_gear.edges[:] \
                                                    + bm_gear.faces[:],
                    edges_exclude=              set(),
                    use_keep_orig=              False,
                    use_normal_flip=            False,
                    use_normal_from_adjacent=   False,
                    use_dissolve_ortho_edges=   False,
                    use_select_history=         False
                    )
                
            geom_hold= ret['geom']
            del ret
            
            # Extract the extruded vertices
            extrude_verts= [ele for ele in geom_hold if \
                                    isinstance(ele, bmesh.types.BMVert)]

            del geom_hold

            bmesh.ops.translate(
                bm_gear,
                vec=    (0.0,0.0,thk),
                verts=  extrude_verts
                )

    # Flip all the face normal to point outside of the gear
    bmesh.ops.reverse_faces(
        bm_gear,
        faces=          bm_gear.faces[:],
        flip_multires=  False
        )
    
    vertex_count= len(bm_gear.verts[:])
    
    # Finish up, write the bmesh into a new mesh
    msh_gear= bpy.data.meshes.new(name_msh)
    bm_gear.to_mesh(msh_gear)
    bm_gear.free()

    # Add the mesh to the scene
    obj_gear= bpy.data.objects.new(name, msh_gear)
    bpy.context.collection.objects.link(obj_gear)

    return vertex_count

def generateGear_external(
        pa,
        N,
        m,
        k_a,
        k_d,
        bl,
        pts_i,
        pts_a,
        pts_d,
        pts_d_b,
        pts_c,
        F_cmfr,
        thk,
        name
    ):
    
    """ Return the gear radii and Blender vertex count noted below for
        the external gear
    
        Inputs:
            pa  = Pressure angle, radians
            N   = Number of teeth
            m   = Module, mm/N
            k_a = Addendum height factor, h_a = k_a*m
            k_d = Dedendum height factor, h_d = k_d*m
            bl  = Backlash associated with this tooth, mm
            pts_i = Number of points for involue
            pts_a = Number of points for addendum
            pts_d = Number of points for dedendum
            pts_d_b= Number of points for dedendum -> base (if dedendum < base )
            thk   = Thickness to extrude the gear, mm
            name  = Blender object name for gear
            
        Outputs:
            Tuple, (r_p,r_a,r_d,r_b,vertex_count)
            
        Note, total tooth height h = h_a + h_d = (k_a + k_d)*m
        Note, Blender mesh name for gear will be name+'_msh'

        Circle Involute Parametric Equation from:
            Weisstein, Eric W. "Circle Involute." From MathWorld--A Wolfram Web Resource. 
            https://mathworld.wolfram.com/CircleInvolute.html
                
    """
    
    # Determine the relevant radii
    r_p= 0.5*m*N
    
    r_a= r_p + k_a*m
    
    r_d= r_p - k_d*m
    
    r_b= r_p*math.cos(pa)

    # Set chamber factor (value must be between 0 -> 1 )
    F_cmfr_used= max(min(F_cmfr, 1.0), 0.0)

    # Check bottom starting point. Base or dedendum
    r_start= r_b
    
    if r_b<r_d:
        
        r_start= r_d
        
        F_cmfr_used= 0.
    
    # Determine the involute angles to generate the involute
    theta_a= math.sqrt((r_a*r_a)/(r_b*r_b)-1.)
    
    theta_p= math.sqrt((r_p*r_p)/(r_b*r_b)-1.)
    
    theta_d= math.sqrt((r_start*r_start)/(r_b*r_b)-1.)
    
    # Translate these involute angles to circle angles
    alpha_a= theta_a - math.atan(theta_a)
    
    alpha_p= theta_p - math.atan(theta_p)
    
    alpha_d= theta_d - math.atan(theta_d)
    
    # Determine the backlash angle. Backlash basis is at the pitch
    alpha_bl= bl/r_p
    
    # Determine the tooth thickness.
    # Hard coded value of 0.5 circular pitch without backlash
    # The backlash reduces the tooth thickness
    del_alpha_tooth= math.pi/(2.*N) - alpha_bl
    
    # Generate the actual start and end angles around the involute angles
    # Prior to rotating, 0 = start point for involute on the base
    alpha_end=      alpha_p + del_alpha_tooth
    
    alpha_start=    alpha_end - math.pi/N

    # Check for chamfer
    alpha_c= alpha_d

    r_c= r_d

    x_cc= 0.0       # x-location of circle center
    
    y_cc= 0.0       # y-location of circle center
    
    r_cc= 0.0       # Chamfer circle radius
    
    beta_cc= 0.0    # Angle to chamfer center
    
    x_c_pts= []
    
    y_c_pts= []
    
    if F_cmfr_used > 0.:    # Add a chamfer
        # set a constant for the calculations
        K_hold= F_cmfr_used * ( r_b - r_d )
        
        # Chamfer dimensions
        r_cc= K_hold * ( 1. + K_hold / ( 2. * r_d ) )   
        
        y_cc= -r_cc  
        
        x_cc= r_d + K_hold  
        
        # Angle of line to the chamfer circle center
        beta_cc= math.atan2( y_cc, x_cc )
        
        # Limit to alpha start as needed
        if beta_cc < alpha_start:
        
            beta_cc= alpha_start 
            
            x_cc= r_d * (
                            math.sqrt(
                                         math.tan(beta_cc)*math.tan(beta_cc) \
                                       + 1. 
                                      ) \
                          - math.tan(beta_cc) 
                        )
                        
            F_cmfr_used= ( x_cc - r_d ) / ( r_b - r_d )
            
            if F_cmfr_used > 1.0:
            
                F_cmfr_used= 0.0
                
                x_cc= 0.0
                
                y_cc= 0.0
                
                r_cc= 0.0
                
                beta_cc= 0.0
                
            else:
            
                y_cc= x_cc * math.tan(beta_cc)
                
                r_cc= -y_cc
                        
        if F_cmfr_used <= 1.:
            
            angle_step_c= ( 0.5 * math.pi + beta_cc ) / ( pts_c - 1. )
            
            for i in range(0, int(pts_c)):
            
                angle_curr= math.pi + beta_cc - i*angle_step_c
                
                x_c_pts.append( r_cc * math.cos(angle_curr) + x_cc )
                
                y_c_pts.append( r_cc * math.sin(angle_curr) + y_cc )
            
            alpha_c= beta_cc

            r_c= x_cc
    
    # Begin genrating the tooth profile
    x_hold= []

    y_hold= []
    
    # Generate the dedendum
    angle_step= (alpha_c-alpha_start)/(pts_d-1.) 
    
    for i in range(0,int(pts_d)):
    
        angle= alpha_start + i*angle_step
    
        x_hold.append(r_d*math.cos(angle))
    
        y_hold.append(r_d*math.sin(angle))

    # Add the chamfer
    x_hold.extend(x_c_pts)
    
    y_hold.extend(y_c_pts)
    
    # If base > dedendum add points 
    if r_b > r_d:
    
        d_len= ( r_b - r_c ) / ( pts_d_b - 1. ) 
    
        for i in range(0, int(pts_d_b)):
        
            x_hold.append( r_c + i*d_len )  
        
            y_hold.append( y_hold[-1] )
    
    # Generate the involute
    # Determine constant arc length
    d_arc= r_b * ( theta_a*theta_a - theta_d*theta_d ) / ( 2. * ( pts_i - 1. ) )

    # First point
    angle= theta_d
    
    x_hold.append(r_b*(math.cos(angle) + angle*math.sin(angle)))
    
    y_hold.append(r_b*(math.sin(angle) - angle*math.cos(angle)))
    
    for i in range(1, int(pts_i) ):
    
        angle= math.sqrt( angle*angle + 2.*d_arc/r_b )
    
        x_hold.append(r_b*(math.cos(angle) + angle*math.sin(angle)))
    
        y_hold.append(r_b*(math.sin(angle) - angle*math.cos(angle)))
    
    # Generate the addendum
    angle_step= (alpha_end-alpha_a)/(pts_a-1.)
    
    for i in range(0,int(pts_a)):
    
        angle= alpha_a + i*angle_step
    
        x_hold.append(r_a*math.cos(angle))
    
        y_hold.append(r_a*math.sin(angle))
    
    # Shift to align along the x-axis to facilitate the mirror
    x_coord= []

    y_coord= []

    for i in range(0,len(x_hold)):

        x_coord.append(
                          x_hold[i]*math.cos(-alpha_end) \
                        - y_hold[i]*math.sin(-alpha_end) \
                      )
        
        y_coord.append(
                          x_hold[i]*math.sin(-alpha_end) \
                        + y_hold[i]*math.cos(-alpha_end) \
                      )
    
    del x_hold
    
    del y_hold

    # Perform the mirror on reversed set of data to make contiguous for Blender
    x_top= [x for x in x_coord]
    
    x_top.reverse()
    
    y_top= [-x for x in y_coord]
    
    y_top.reverse()
    
    # Combine everything into a single tooth outline
    x_coord.extend(x_top)
    
    y_coord.extend(y_top)
    
    del x_top
    
    del y_top

    # Generate the external gear in Blender
    vertex_count=   generateExteriorGearBlender(
                        N,
                        x_coord,
                        y_coord,
                        thk,
                        1,
                        name
                    )

    # Exit and return geometry information
    return (r_p,r_a,r_d,r_b,vertex_count, (F_cmfr_used,r_cc,x_cc,y_cc,beta_cc))


def generateRackBlender(R_x, R_y, pa, r_a_pin, thk, x_end, dx_end, name):

    # Blender mesh name
    name_msh=   name+"_msh"

    # Make a new BMesh
    bm_rack = bmesh.new()

    # Add a circle for modifying vertices for rack
    bmesh.ops.create_circle(
        bm_rack,
        cap_ends=   True,
        radius=     r_a_pin,
        segments=   len(R_x))

    # Modify vertices to make the gear
    for vert in bm_rack.verts:
        vert.co.x=  R_x[vert.index]
        vert.co.y=  R_y[vert.index]

    # Extrude the edges to make the gear three dimensional
    ret = bmesh.ops.extrude_edge_only(
                                        bm_rack,
                                        edges=  bm_rack.edges[:]
                                    )

    geom_extrude_top = ret["geom"]
    del ret

    # Collect the edges and then translates up.
    verts_extrude_top = [ele for ele in geom_extrude_top
                            if isinstance(ele, bmesh.types.BMVert)]
    
    edges_extrude_top = [ele for ele in geom_extrude_top
                            if isinstance(ele, bmesh.types.BMEdge) \
                                and ele.is_boundary]
    
    # This translation extrudes only the edges and not a face
    bmesh.ops.translate(
                        bm_rack,
                        verts=  verts_extrude_top,
                        vec=    (0.0, 0.0, thk)
                        )

    # This closes the extruded edges at the top to a face
    bmesh.ops.edgeloop_fill(
                            bm_rack,
                            edges=  edges_extrude_top
                            )

    # This is "merge by distance" to remove doubles
    bmesh.ops.remove_doubles(
                            bm_rack,
                            verts=  bm_rack.verts[:],
                            dist=   0.0001
                            )

    # Extrude end faces to the requested extension point
    face_xp= None    # +x face

    face_xm= None   # -x face
    
    # Obtain the end faces
    for face in bm_rack.faces:
    
        face_center= face.calc_center_median()

        if face.normal[1] > -0.98 and face_center.y < R_y[0]:
        
            if face.verts[0].co.x > 0.0:
            
                face_xp= face
                
            else:
            
                face_xm= face

    # As the positive face may need to move in the -x direction and the face
    # is separate from the tooth, just translate the face.
    bmesh.ops.translate(
        bm_rack,
        vec=    (dx_end, 0.0, 0.0),
        verts=  face_xp.verts[:]
    )
    
    del face_xp
    
    # As the negative face is tied to the tooth, it must be extruded.
    ret=    bmesh.ops.extrude_face_region(
                bm_rack,
                geom=                       [face_xm],
                edges_exclude=              set(),
                use_keep_orig=              False,
                use_normal_flip=            False,
                use_normal_from_adjacent=   False,
                use_dissolve_ortho_edges=   False,
                use_select_history=         False
                )
            
    h_geom = ret['geom']
    
    del ret
    
    # Extract the extruded vertices
    h_verts= [ele for ele in h_geom if isinstance(ele, bmesh.types.BMVert)]

    del h_geom

    # Set the x-position of the extruded face
    for vert in h_verts:
    
        vert.co.x= -x_end
        
    # Now delete the original face
    bmesh.ops.delete(
        bm_rack, 
        geom=       [face_xm],
        context=    'FACES'
    )
    
    # Clean up the bottom, up, and downs faces to reduce the geometry
    verts_bot= set()

    verts_up= set()
    
    verts_dwn= set()

    edges_bot= set()
    
    edges_up= set()
    
    edges_dwn= set()
    
    # Obtain the edges and vertics on the various faces    
    for face in bm_rack.faces:
    
        if face.normal[1] < -0.98:      # Get the bottom faces
        
            for vert in face.verts:
            
                verts_bot.add(vert)
                
            for edge in face.edges:
            
                edges_bot.add(edge)
                
        if face.normal[2] < -0.98:      # Get the downward faces
        
            for vert in face.verts:
            
                verts_dwn.add(vert)
                
            for edge in face.edges:
            
                edges_dwn.add(edge)
                
        if face.normal[2] > 0.98:      # Get the upward faces
        
            for vert in face.verts:
            
                verts_up.add(vert)
                
            for edge in face.edges:
            
                edges_up.add(edge)

    # Generate a list of lists to use in the loop
    verts_list= [ list(verts_bot), list(verts_up), list(verts_dwn) ]
    
    edges_list= [ list(edges_bot), list(edges_up), list(edges_dwn) ]
    
    del verts_bot

    del verts_up

    del verts_dwn

    del edges_bot

    del edges_up

    del edges_dwn

    # Limit dissolve the various faces to clean up the geometry
    for i in range(0,len(verts_list)):

        bmesh.ops.dissolve_limit(
            bm_rack, 
            angle_limit=                math.radians(3.0), 
            use_dissolve_boundaries=    False, 
            verts=                      verts_list[i], 
            edges=                      edges_list[i],
            delimit=                    {'NORMAL'}
        )
    
    del verts_list
    
    del edges_list

    vertex_count= len(bm_rack.verts[:])

    # Finish up, write the bmesh into a new mesh
    msh_rack=   bpy.data.meshes.new(name_msh)
    bm_rack.to_mesh(msh_rack)
    bm_rack.free()

    # Add the mesh to the scene
    obj_rack = bpy.data.objects.new(name, msh_rack)
    bpy.context.collection.objects.link(obj_rack)

    return vertex_count


def generateGear_rack(
        N, 
        pa, 
        m, 
        k_a, 
        k_d, 
        bl, 
        r_p_pin, 
        r_a_pin, 
        thk, 
        depth, 
        extension,
        name
    ):
    
    dl_0= k_d * m * math.tan(pa)
    dl_1= k_a * m * math.tan(pa)
    dl_2= 0.5*m*math.pi - 2.*(dl_1 + bl)
    dl_3= 0.5*m*math.pi - 2.*(dl_0 - bl)
    
    R_x=    [
                0., 
                dl_0 ,
                dl_0+dl_1  , 
                dl_0+dl_1+dl_2, 
                dl_0+2.*dl_1+dl_2, 
                2.*dl_0+2.*dl_1+dl_2, 
                2.*dl_0+2.*dl_1+dl_2+dl_3
            ]
    
    R_y=    [
                0., 
                k_d*m, 
                (k_a+k_d)*m, 
                (k_a+k_d)*m,    
                k_d*m, 
                0., 
                0.
            ]
    
    len_rx_orig = len(R_x)
    
    for i in range(0,len_rx_orig):
        R_x[i] += 0.5 * dl_3 - math.ceil(0.5*N)*math.pi*m
        R_y[i] -= r_p_pin + k_d*m

    for i in range(1,N):
        for j in range(0,len_rx_orig):
            R_x.append(R_x[j]+i*math.pi*m)
            R_y.append(R_y[j])
    
    # Add backside of the rack
    R_x.extend([R_x[-1],R_x[0],R_x[0]])
    R_y.extend([R_y[-1]-depth,R_y[-1]-depth,R_y[0]])
    
    # Output dimensions
    overall_length= R_x[-3] - R_x[0]
    height_d=       depth
    height_p=       depth+k_d*m
    height_a=       depth+(k_d+k_a)*m
    
    # End of rack in x-direction
    x_end= math.ceil(-R_x[0] + extension)
    
    dx_end= x_end - R_x[-3]     # Translation of positive end face

    vertex_count=   generateRackBlender(
                        R_x, 
                        R_y, 
                        pa, 
                        r_a_pin, 
                        thk, 
                        x_end, 
                        dx_end,
                        name
                    )
    
    return (-x_end, x_end, 2*x_end, height_d, height_p, height_a, vertex_count)


def main():
    """ Rack and Pinion Gear Geometry """
    
    ############################
    ### User input parameters
    ############################
    
    N_pin_min_user= 21    # Minimum number of teeth on pinion (input gear)
    N_rack=         16    # Number of rack teeth
    pa_deg=         20.0  # Pressure angle, degrees (standard=20)
    cr_min=         1.4   # Minimum required contact ratio, normal=1.4
    m_system=       2.    # Module to utilize

    bl=             0.15  # Total backlash, evenly split between the gears
    k_a_pin=        1.0   # Input gear addendum factor, standard= 1.0
    k_a_rack=       1.0   # Output gear addendum factor, standard= 1.0
    k_d_pin=        1.25  # Input gear dedendum factor, standard= 1.25
    k_d_rack=       1.25  # Output gear dedendum factor, standard= 1.25
    
    F_cmfr=         1.0   # Pinion Chamfer factor. Must be 0.0 <= F_cmfr <= 1.0

    # Note these points are used to generate only one tooth.
    # Not the complete gear. 
    pts_involute=   55    # Points used to generate half the involute
    pts_addendum=   20    # Points used to generate half the addendum
    pts_dedendum=   20    # Points used to generate half the dedendum
    pts_ded_base=   10    # Points used split from dedendum to base (if dedendum < base)
    pts_chamfer=    10    # Points used to generte the pinion chamfer

    thick_pin=      8.0   # Thickness of the input gear
    thick_rack=     8.0   # Thickness of the output gear
    depth_rack=     5.    # Depth of backside (i.e., opposite of teeth) of the rack
    end_extension=  3.    # Amount to extend base beyond largest dimension

    name_base=      'rp'    # Blender object base name rack and pinion set
    
    ### NOTE:   No check of existing Blendeer object/mesh names done.
    ###         Confirm no conflicts or unexpected behavior may occur 
    
    write_file=     False
    file_name=      'absolute_file_path'

    ############################
    ### Internal Calculations
    ############################
    
    name_pin=       name_base + '_pinion'    # Blender object name for input gear
    name_rack=      name_base + '_rack'   # Blender object name for output gear    
    
    if write_file:
        with open(file_name, mode='at') as fileout:
            fileout.write(f'\n\n{"*"*50}\n')
            fileout.write(f'**** Starting New Rack and Pinion ({name_base}) ****\n\n')
    
    # Internal program constants
    tol_rel=    0.01  # Iteration relative tolerance in percent
    pa=         pa_deg*math.pi/180.    # Pressure angle in radians

    # Determine the minimum number of pinion gear teeth
    N_pin_min_interference=  math.ceil(2.*k_a_rack/(math.sin(pa)*math.sin(pa)))
    
    N_pin_min_contact_ratio=    rp_Np_min_cr(
                                    cr_min,
                                    pa,
                                    k_a_pin,
                                    k_a_rack,
                                    tol_rel,
                                    0.1
                                )
    
    # Determine the used number of pinion gear teeth
    N_pin=  max(N_pin_min_interference,N_pin_min_contact_ratio,N_pin_min_user)
    
    # Determine actual contact ratio
    cr_act= rp_contactRatio(N_pin,pa,k_a_pin,k_a_rack)
    
    # Generate the pinion gear
    r_p_pin,r_a_pin,r_d_pin,r_b_pin,pts_pin,c_pin=  generateGear_external(
                                                        pa,
                                                        N_pin,
                                                        m_system,
                                                        k_a_pin,
                                                        k_d_pin,
                                                        0.5*bl,
                                                        pts_involute,
                                                        pts_addendum,
                                                        pts_dedendum,
                                                        pts_ded_base,
                                                        pts_chamfer,
                                                        F_cmfr,
                                                        thick_pin,
                                                        name_pin
                                                    )

    # Extract the chamfer information
    F_cmfr_used_pin,r_cc_pin,x_cc_pin,y_cc_pin,beta_cc_pin= c_pin    
    
    # Generate the rack
    r_x_start, r_x_end,r_oal, r_h_d, r_h_p, r_h_a, pts_rack= generateGear_rack(
                                                                N_rack,
                                                                pa,
                                                                m_system,
                                                                k_a_rack,
                                                                k_d_rack,
                                                                0.5*bl,
                                                                r_p_pin,
                                                                r_a_pin,
                                                                thick_rack,
                                                                depth_rack,
                                                                end_extension,
                                                                name_rack
                                                              )

    # Determine maximum shaft diameters
    d_shaft_max_pin= 2.0*min(r_d_pin,r_b_pin)
    
    if write_file:
        with open(file_name, mode='at') as fileout:
            ################# Blender output information ############
            fileout.write(f'{"#"*8} New Rack and Pinion Data = {name_base} {"#"*8}\n\n')
            fileout.write(f'USER INPUTS:\n')
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Minimum Pinion Teeth =', N_pin_min_user))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Number of Rack Teeth =', N_rack))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Pressure Angle (deg) =', pa_deg))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Contact Ratio Minimum =', cr_min))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Module =', m_system))
            
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Backlash Total =', bl))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Pinion Gear k_a =', k_a_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Rack Gear k_a =', k_a_rack))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Pinion Gear k_d =', k_d_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Rack Gear k_d =', k_d_rack))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Chamfer Factor =', F_cmfr))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Number of Points: Involue =', pts_involute))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Number of Points: Addendum =', pts_addendum))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Number of Points: Dedendum =', pts_dedendum))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Number of Points: Ded->Base =', pts_ded_base))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Number of Points: Chamfer =', pts_chamfer))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Pinion Gear Thickness =', thick_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Rack Gear Thickness =', thick_rack))
            fileout.write('\t{0:<29}{1:>12.4f}\n\n'.format('Rack Gear Depth =', depth_rack))
            
            fileout.write(f'INTERNAL CALCULATIONS:\n')
            fileout.write(f'\tMinimum Number of Pinion Teeth Check:\n')
            fileout.write('\t\t{0:<25}{1:>12.4f}\n'.format('Interference =', N_pin_min_interference))
            fileout.write('\t\t{0:<25}{1:>12.4f}\n'.format('Contact Ratio =', N_pin_min_contact_ratio))
            fileout.write('\t\t{0:<25}{1:>12.4f}\n'.format('User Input =', N_pin_min_user))
            
            fileout.write('\t{0:<29}{1:>12.4f}\n\n'.format('Contact Ratio Actual =', cr_act))

            fileout.write(f'Pinion Gear Information:\n')
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Pitch radius =', r_p_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Pitch diameter =', 2.*r_p_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Base diameter =', 2.*r_b_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Addendum Diameter =', 2.0*r_a_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Dedendum Diameter =', 2.0*r_d_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Number of teeth =', N_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('k_a =', k_a_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('k_d =', k_d_pin))
            fileout.write(f'\tChamfer Information:\n')
            fileout.write('\t\t{0:<25}{1:>12.4f}\n'.format('Factor =', F_cmfr_used_pin))
            fileout.write('\t\t{0:<25}{1:>12.4f}\n'.format('Radius =', r_cc_pin))
            fileout.write('\t\t{0:<25}{1:>12.4f}\n'.format('Center: X-dir =', x_cc_pin))
            fileout.write('\t\t{0:<25}{1:>12.4f}\n'.format('Center: Y-dir =', y_cc_pin))
            fileout.write('\t\t{0:<25}{1:>12.4f}\n'.format('Angle to Center =', beta_cc_pin*180./math.pi))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Thickness =', thick_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Maximum Shaft Diameter =', d_shaft_max_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Number of vertex points =', pts_pin))
            fileout.write('\t{0:<29}{1:>12.4f}\n\n'.format('Rotation to mesh (deg) =', -90))
            
            fileout.write(f'Rack Gear Information:\n')
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('X-Dir Start Dimension =', r_x_start))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('X-Dir End Dimension =', r_x_end))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Overall length =', r_oal))
            fileout.write(f'\tHeights from Base/Bottom to:\n')
            fileout.write('\t\t{0:<25}{1:>12.4f}\n'.format('Dedendum =', r_h_d))
            fileout.write('\t\t{0:<25}{1:>12.4f}\n'.format('Pitch =', r_h_p))
            fileout.write('\t\t{0:<25}{1:>12.4f}\n'.format('Addendum =', r_h_a))

            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Number of teeth =', N_rack))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('k_a =', k_a_rack))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('k_d =', k_d_rack))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Thickness =', thick_rack))
            fileout.write('\t{0:<29}{1:>12.4f}\n'.format('Depth =', depth_rack))
            fileout.write('\t{0:<29}{1:>12.4f}\n\n'.format('Number of vertex points =', pts_rack))

            fileout.write(f'{"*"*8} End Rack and Pinion Data = {name_base} {"*"*8}\n')    
            ################# Blender output information ############

    ##############################
    ##############################
    ####### USE BPY TO ALIGN GEARS
    ##############################
    ##############################
    
    # Set the veiew layer to get a context
    view_layer = bpy.context.view_layer
    view_layer.objects.active = bpy.data.objects[name_pin]
    
    # Ensure in OBJECT mode
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(
            mode = 'OBJECT',
            toggle = False
            )
    else:
        
        bpy.context.window_manager.popup_menu(
            e_mode_set, 
            title=  "Oops, Sorry! Aligning Gears:", 
            icon=   'ERROR'
        )        

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with mode_set.\n#####\n')
        
    # Deselect everything for a clean slate
    if bpy.ops.object.select_all.poll():
        bpy.ops.object.select_all(action='DESELECT')
    else:
        
        bpy.context.window_manager.popup_menu(
            e_deslection, 
            title=  "Oops, Sorry! Aligning Gears:", 
            icon=   'ERROR'
        )

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with deselection.\n#####\n')
    
    # Set the output gear as the selected object
    bpy.data.objects[name_pin].select_set(state=True)

    # Rotate -90 deg to make with rack
    bpy.context.object.rotation_euler[2] = -0.5*math.pi

    # Deselect everything for a clean slate
    if bpy.ops.object.select_all.poll():
        bpy.ops.object.select_all(action='DESELECT')
    else:
        
        bpy.context.window_manager.popup_menu(
            e_deslection, 
            title=  "Oops, Sorry! Aligning Gears:", 
            icon=   'ERROR'
        )

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with deselection.\n#####\n')
 
    # Indicate complete with gear generation    
    if write_file:
        with open(file_name, mode='at') as fileout:
            fileout.write(f'\n****** Ending Rack and Pinion ({name_base}) ******\n')
            fileout.write(f'{"*"*50}\n\n')

    return 0
    
# Call main if this file is executed as a script
if __name__ == '__main__':
    main()
