# blender_files

## Description
These are Python script files which can be used in [Blender](https://www.blender.org/) to generate the various components (e.g, gears, rotary lobes) given user inputs which vary per component.  The files rely upon the [Blender Python API Reference](https://docs.blender.org/api/current/) and mainly utilize the [Bmesh](https://docs.blender.org/api/current/bmesh.html) module, with some usage of the [Operators](https://docs.blender.org/api/current/bpy.ops.html) module.  

Each file is stand alone and does not rely upon any other files.  This means there is duplication of code in these files (e.g, the starting initial tooth involute).  So if you Fork these, changes in one file that is common to multiple files will not automatically transfer to other componets.  That needs to done manually.  This was done to simplify usage per component without the need to use any other files.  Granted, not the most efficient from a changes standpoint, but that is what was done.

## Using
These are not [Blender](https://www.blender.org/) Add-ons and therefore not used by loading through Edit -> Preferences -> Add-ons.  These are used by going to the Scripting Workspace found at the very right of the list of Workspaces on the top bar.  Once there, click on Open to search for the file on your system and load into the workspace as shown in the following:

<div align="center">
  <img src="snapshots/blender_scripting.png" alt="Screenshot_scripting"/>
</div>

The location for the user inputs to generate user specific components is located at the beginning of the `main()` function.  This function is located near the bottom of the file or search for `def main():`.  It will look like this:

<div align="center">
  <img src="snapshots/user_inputs.png" alt="Screenshot_scripting"/>
</div>

## Common Features
There are common features for all the files which are discussed below.  Component specific features are provided in the README files in those folders.  

### Blender Object/Mesh Names
[Blender](https://www.blender.org/) requires a unique name for each object.  The user input `name_base` is used as the Blender object name.  For files that generate only a single component (e.g, springs), this `name_base` will be the Blender object name for the component.  

For files that generate mulitple components (e.g, gears in pairs), the `name_base` will be the the leading part of all the generated objects followed by an `_` followed by an additional component specific identifier.  For example, when generating spur gears an input and output gear will be generated.  The two component object names will be `name_base_in` and `name_base_out`, respectively.

The mesh names for all the components generated are the complete object name followed by `_msh`.  Using the input spur gear example above, the corresponding mesh name will be `name_base_in_msh`.

**Caution:**  Each time the script is used to generate new components, verify an existing object with the `name_base` input is not currently utilized.  **_This check is not done by the script._** If the same `name_base` is used again, there may be errors by potentially working on the wrong mesh files.

### Writing Output Data
Based on the given user data, various calculations are performed.  If using these files to generate sub-assemblies for a larger components, for example a [Roots blower](https://en.wikipedia.org/wiki/Roots_blower) or a [car differential](https://www.youtube.com/watch?v=yYAw79386WI), information on items such as the diameters of addendums and dedendums along with other information is useful.  Instead of measuring these in Blender, set `write_file= True` and then provide an absolute path of the file to be generated in the `file_name` variable.  If this file does not yet exist, it will be created.  If the file already exists, the new information will be appended to the file. 

The written output file will also include all the user inputs.  This is helpful when generating multiple components or when going back to look at how something was generated if wanting to regenerate it.

### Error Notifications
As this is not a [Blender](https://www.blender.org/) Add-on, errors are not shown as `Info` text at the bottom of Blender.  

Instead, a pop up window will appear next to the current cursor location if an error has occurred.  It will look similar to the following:

<div align="center">
  <img src="snapshots/errors.png" alt="Screenshot_scripting"/>
</div>

To read the error, do not move the cursor.  When finished reading the error message, simply move the cursor and the pop up window will go away.

All the errors that generate pop up windows are also written to the output file, if that variable is set to `True`.  

## Component Specific Usage
More information on inputs to each component are provided in their respective folders.

## 3D Printing
These files generate closed manifolds which can be exported to an STL file (through File->Export->STL) and then used in slicer programs (e.g, [Prusa Slicer](https://www.prusa3d.com/en/page/prusaslicer_424/), [Cura](https://ultimaker.com/software/ultimaker-cura/)).  

Without changing any [Blender](https://www.blender.org/) settings, the units drawn in Blender will be in meters.  However, when the STL is generated and imported into [Prusa Slicer](https://www.prusa3d.com/en/page/prusaslicer_424/), it will scale the STL to millimeters.  How the STL file imports into other slicer programs has not be tested.

**Caution:** The user specifies how detailed a model will be based on various inputs such as the number of points to generate per involute, number steps in a full rotation, etc.  Making models very finely detailed will take time to generate in [Blender](https://www.blender.org/) and will also export a much larger STL file that a slicer program will need to process.

## License
To be consistent with the use of the Blender Python API, all these scripts are licensed under the GNU GENERAL PUBLIC LICENSE Version 3 or later.

## The End
Hopefully some of these files may be useful in your projects.
