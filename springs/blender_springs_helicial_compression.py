"""
PROGRAM:
blender_springs_helicial_compression.py

Copyright (C) 2024 Anders B. Thlenk

LICENSE:
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.


DESCRIPTION:
Utilize Blender <https://www.blender.org/> to auto generate a helical compression spring given various user inputs.  Refer to the "User Inputs" section of main() below.

"""

import bpy
import bmesh
import mathutils
import math

#########################################################
#########  Just error boxes
#########################################################

def e_mode_set(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with mode_set.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

def e_sub_mode(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with mode_set_with_submode.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

def e_deslection(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with deselection.'
    
    self.layout.label(text=message, icon='INFO')
    
    return
    
def e_se_ltzero_end_angle(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'End Pitch Angle < 0.0 deg'
    
    self.layout.label(text=message, icon='INFO')
    
    return
    
def e_se_ltzero_trans_angle(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Transition Angle < 0.0 deg'
    
    self.layout.label(text=message, icon='INFO')
    
    return
    
def e_verts_too_low(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Wire Diameter Vertices Must Be > 2'
    
    self.layout.label(text=message, icon='INFO')
    
    return



#########################################################
#########  Starting Spring Stuff
#########################################################

def blnd_joining(viewLayer, lstObjs, strJoinType, removeDoubles, write_file, file_name):
    """ Consolidating steps for joining items in Blender """
    
    baseObj = lstObjs[0]
    baseMsh = bpy.data.objects[baseObj].data.name_full
    
    # Set the active object and swtich to object mode.
    viewLayer.objects.active = bpy.data.objects[baseObj]

    # Ensure in OBJECT mode
    if bpy.ops.object.mode_set.poll():

        bpy.ops.object.mode_set(
            mode = 'OBJECT',
            toggle = False
        )

    else:
        
        bpy.context.window_manager.popup_menu(
            e_mode_set, 
            title=  "Oops, Sorry! While Joining:", 
            icon=   'ERROR'
        )

        if write_file:

            with open(file_name, mode='at') as fileout:

                fileout.write('\n####\nBlendJoin: Issue with mode_set.\n#####\n')

    # Deselect everything for a clean slate
    if bpy.ops.object.select_all.poll():
        
        bpy.ops.object.select_all(action='DESELECT')

    else:

        bpy.context.window_manager.popup_menu(
            e_deslection, 
            title=  "Oops, Sorry! While Joining:", 
            icon=   'ERROR'
        )

        if write_file:

            with open(file_name, mode='at') as fileout:

                fileout.write('\n####\nBlendJoin: Issue with deselection.\n#####\n')

    # Store mesh names for future removal after joining
    lstMshNames = []
    
    # Select the objects to join and store mesh names
    for obj in lstObjs:

        bpy.data.objects[obj].select_set(state=True)

        lstMshNames.append(bpy.data.objects[obj].data.name_full)

    # Get the original number of polygons in the base mesh
    polys_orig = len(bpy.data.meshes[baseMsh].polygons)
    
    # Now join the objects.
    bpy.ops.object.join()

    # Determine new quantity of polygons.  Added polygons at the end
    # of the original object
    polys_joined = len(bpy.data.meshes[baseMsh].polygons)

    # Select non-active object items in the joined mesh
    for poly in bpy.data.meshes[baseMsh]\
                    .polygons[polys_orig:polys_joined]:
        poly.select = True

    # Shift into edit mode with face select active
    if bpy.ops.object.mode_set_with_submode.poll():

        bpy.ops.object.mode_set_with_submode(
            mode='EDIT',
            toggle=False,
            mesh_select_mode={'FACE'}
        )

    else:

        bpy.context.window_manager.popup_menu(
            e_sub_mode, 
            title=  "Oops, Sorry! While Joining:", 
            icon=   'ERROR'
        )

        if write_file:

            with open(file_name, mode='at') as fileout:

                fileout.write('\n####\nBlendJoin: Issue with mode_set_with_submode.\n#####\n')

    # Perform the user input joining    
    bpy.ops.mesh.intersect_boolean(
        operation=strJoinType,
        use_swap=False,
        use_self=False,
        threshold=1e-06,
        solver='EXACT'
    )
        
    # Remove the no longer needed joined meshes
    for idx in range(1,len(lstMshNames)):
        bpy.data.meshes.remove(bpy.data.meshes[lstMshNames[idx]])

    if removeDoubles:
        # Remvove potential doubles
        bpy.ops.mesh.remove_doubles(
            threshold=0.0001,
            use_unselected=False,
            use_sharp_edge_from_normals=False
        )

    # Go back to OBJECT mode
    if bpy.ops.object.mode_set.poll():

        bpy.ops.object.mode_set(
            mode = 'OBJECT',
            toggle = False
        )

    else:

        bpy.context.window_manager.popup_menu(
            e_mode_set, 
            title=  "Oops, Sorry! While Joining:", 
            icon=   'ERROR'
        )

        if write_file:

            with open(file_name, mode='at') as fileout:

                fileout.write('\n####\nBlendJoin: Issue with mode_set -end.\n#####\n')
        
    # Deselect everything for a clean slate
    if bpy.ops.object.select_all.poll():

        bpy.ops.object.select_all(action='DESELECT')

    else:

        bpy.context.window_manager.popup_menu(
            e_deslection, 
            title=  "Oops, Sorry! While Joining:", 
            icon=   'ERROR'
        )

        if write_file:

            with open(file_name, mode='at') as fileout:

                fileout.write('\n####\nBlendJoin: Issue with deselection - end.\n#####\n')

    return 0

def main():

    ############################
    ### User input parameters
    ############################    

    d                   = 1.0       # wire diameter of the spring
    L_w                 = 15.0      # Useable working length required
    x_len_pct           = 75.       # Percent of available working length which is useable
    N_a                 = 4         # Number of active coils
    
    use_square_end      = True      # Should a square end be used (instead of plain end)

                                    # The following two are for squared ends only
    
    N_e                 = 2         # Number of end coils on each end
    end_trans_angle_deg = 360.0      # Main to end pitch transition angle        
    

    use_ground_end      = True      # Should the ends be ground (instead of free)
    use_D_o             = False     # Basis for coil diameter
    
    D_i                 = 6.05      # Clear internal diameter of coil
    D_o                 = 8.00      # Overall outside diameter of coil
    
    hand                = "R"       # Right or left hand
    rot_steps           = 180       # Number of steps per 360 deg turn
    circ_segments       = 72        # Segments for the spring circle (must be > 2)
    
    name_base           = 'spring_seg'  # Blender object name
    
    write_file          = False
    file_name           = 'absolute_file_path'

    ############################
    ### Internal Calculations
    ############################

    if write_file:
        with open(file_name, mode='at') as fileout:
            fileout.write(f'\n\n{"*"*50}\n')
            fileout.write(f'**** Starting New Spring ({name_base}) ****\n\n')

    if circ_segments < 2:
    
        bpy.context.window_manager.popup_menu(
            e_verts_too_low, 
            title=  "Oops, Sorry! Wire Diameter Vertices Issue", 
            icon=   'ERROR'
        )
        
        if write_file:
            
            with open(file_name, mode='at') as fileout:
            
                fileout.write(f'\n\n***** Error: Input: Wire Diameter Vertices ******')
            
                fileout.write(f'\n\t*** Condition: Wire Diameter Vertices Must Be > 2 ***')
            
        return 0
    
    # Blender mesh name
    name_base_msh = name_base+'_msh'

    # Internal constants
    end_gap= 0.02       # Used to avoid touching on squared ends
    
    if not use_square_end:
    
        end_trans_angle_deg= 0.0
        
        N_e= 0.0
        
    # Ends are defined as non-active and so include both the transition
    # angle and the end pitch angles
    
    ang_end_trans= end_trans_angle_deg * math.pi / 180.
    
    ang_end_total= N_e * 2. * math.pi   # Total angle for ends
    
    ang_end_pitch= ang_end_total - ang_end_trans # Total angle for end pitch
    
    if use_square_end:
    
        if end_trans_angle_deg < 0.0:
            
            bpy.context.window_manager.popup_menu(
                e_se_ltzero_trans_angle, 
                title=  "Oops, Sorry! While Generating Squared Ends", 
                icon=   'ERROR'
            )
            
            if write_file:
                
                with open(file_name, mode='at') as fileout:
                
                    fileout.write(f'\n\n***** Error: While Generating Squared Ends. ******')
                
                    fileout.write(f'\n\t*** Condition: Transition Angle Must Be >= 0.0 deg ***')
                
            return 0

        if ang_end_pitch<0.0:    # Error
        
            bpy.context.window_manager.popup_menu(
                e_se_ltzero_end_angle, 
                title=  "Oops, Sorry! While Generating Squared Ends", 
                icon=   'ERROR'
            )
            
            if write_file:
                
                with open(file_name, mode='at') as fileout:
                
                    fileout.write(f'\n\n***** Error: While Generating Squared Ends. ******')
                
                    fileout.write(f'\n\t*** Condition: End Pitch Angle < 0.0 deg ***')
                
            return 0

    # Determine the step angles
    ang_step= 2. * math.pi / rot_steps
    
    # Set the handedness of the screw
    twist = 1.0

    if hand=="L":

        twist = -1.0

    # Get diameters and radii
    if use_D_o:
    
        D_i= D_o - 2 * d
        
    else:
    
        D_o= D_i + 2 * d
        
    D_m= D_i + d
    
    spring_index= D_m / d
    
    r_id= 0.5 * D_i

    # Determine turns independent properties
    r_s = 0.5 * d

    x_center = -( r_id + r_s )
    
    # Determine the pitches
    P_main= (100.*L_w) / ( x_len_pct * (N_a + ang_end_trans/(2.*math.pi)) ) + d

    P_ends= ( not use_square_end ) * P_main + use_square_end * ( d + end_gap )
        
    # Determine elevation starting point of wire diameter center
    z_start_end = r_s - P_ends * use_ground_end
    
    # Determine steps
    steps_main= math.ceil( N_a * rot_steps )
    
    steps_trans= math.ceil( ang_end_trans / ang_step )
    
    steps_end_pitch=   math.ceil( ang_end_pitch / ang_step ) \
                     + rot_steps * use_ground_end
                     
    steps_end_pitch_dz=   math.ceil( ang_end_pitch / ang_step )

    # Update angles based on ceiling function above
    ang_tot_main= steps_main * ang_step
    
    ang_tot_trans= steps_trans * ang_step
    
    ang_tot_end_pitch= steps_end_pitch * ang_step
    
    ang_tot_end_pitch_dz= steps_end_pitch_dz * ang_step
    
    # Determine ground cutting planes
    z_ground_base= 0.
    
    dz_main= N_a * P_main
    
    dz_end= ang_tot_end_pitch_dz * P_ends / ( 2. * math.pi )
    
    dz_transition= ( ang_tot_trans * ( P_main + P_ends ) ) / ( 4. * math.pi ) 
    
    z_ground_top= dz_main + d + 2 * ( dz_end + dz_transition ) 
    
    steps_total= steps_main + 2. * ( steps_trans + steps_end_pitch_dz )
    
    L_free= z_ground_top - z_ground_base
    
    L_solid= d * ( steps_total * ang_step / ( 2. * math.pi ) + 1. )
    
    dH_avail= L_free - L_solid
    
    dh_used= dH_avail * x_len_pct / 100. 

    ## If requestee, write information to output file    
    if write_file:

        with open(file_name, mode='at') as fileout:

            ################# Blender output information ############
            fileout.write(f'{"#"*8} New Spring Run = {name_base} {"#"*8}\n\n')

            fileout.write(f'USER INPUTS:\n')
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Wire Diameter of Spring =', d))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Actual Working Length Required =', L_w))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Usable Part of Total Length (%) =', x_len_pct))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Number of Active Coils =', N_a))
            fileout.write('\t{0:<35}{1:>12}\n'.format('Use Squared Ends =', 'Yes' if use_square_end else 'No'))
            fileout.write('\t{0:<35}{1:>12}\n'.format('Use Ground Ends =', 'Yes' if use_ground_end else 'No'))
            fileout.write('\t{0:<35}{1:>12}\n'.format('Use Outside Diameter =', 'Yes' if use_D_o else 'No'))
            
            if use_D_o:

                fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Outside Diameter =', D_o))

            else:
            
                fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Inside Diameter =', D_i))
            
            
            if use_square_end:
            
                fileout.write('\t{0:<35}{1:>12.4f}\n'.format('End Transition Angle (deg) =', end_trans_angle_deg))
                
                fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Number of End Coils Per End =', N_e))

            fileout.write('\t{0:<35}{1:>12}\n'.format('Spring Rotation =', 'Right' if hand.upper()=='R' else 'Left'))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Rotation Steps Per Full Rotation =', rot_steps))
            fileout.write('\t{0:<35}{1:>12.4f}\n\n'.format('Wire Circle Segments =', circ_segments))
            
            fileout.write(f'INTERNAL CALCULATIONS:\n')
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Angle Step =', ang_step * 180. / math.pi))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Outside Diameter =', D_o))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Inside Diameter =', D_i))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Mean Diameter =', D_m))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Spring Index =', spring_index))

            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Free Length =', L_free))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Solid Length =', L_solid))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Available Working Length =', dH_avail))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Used Working Length =', dh_used))

            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Main Spring: Pitch =', P_main))
            fileout.write('\t{0:<35}{1:>12.4f}\n'.format('Main Spring: Height =', dz_main))
            
            if use_square_end:
            
                fileout.write('\t{0:<35}{1:>12.4f}\n'.format('End Spring: Pitch =', P_ends))
                
                fileout.write('\t{0:<35}{1:>12.4f}\n\n'.format('End Spring: Height =', dz_end + dz_transition))

            fileout.write(f'\n{"*"*8} End Spring Data = {name_base} {"*"*8}\n')    
            ################# Blender output information ############
        
    ##**********************************##
    ##**********************************##
    ## STARTING BLENDER PART
    ## STARTING BLENDER PART
    ##**********************************##
    ##**********************************##

    #####################################
    ## GENERATING THE SPRING
    #####################################

    # Create a new bmesh
    bm_spring= bmesh.new()

    # Create wire diameter circle with closed end
    bmesh.ops.create_circle(
        bm_spring,
        cap_ends=   True,
        radius=     r_s,
        segments=   circ_segments
    )

    # Move to proper location
    bmesh.ops.translate(
        bm_spring,
        vec=    (x_center,0.0,z_start_end),
        verts=  bm_spring.verts[:]
    )

    # Rotate along x-axis +90 
    bmesh.ops.rotate(
        bm_spring,
        verts=  bm_spring.verts[:],
        cent=   (x_center, 0.0, z_start_end),
        matrix= mathutils.Matrix.Rotation(math.radians(90.0), 3, 'X')
    )
    
    if twist==1:    # Flip face on right hand spring
    
        bmesh.ops.reverse_faces(
            bm_spring, 
            faces=          bm_spring.faces[:], 
            flip_multires=  False
        )
    
    if circ_segments%2 == 0:    # If even sides, rotate to get flat on bottom
    
        # Rotate along y-axis half the angle of the number of sides
        bmesh.ops.rotate(
            bm_spring,
            verts=  bm_spring.verts[:],
            cent=   (x_center, 0.0, z_start_end),
            matrix= mathutils.Matrix.Rotation(math.pi/circ_segments, 3, 'Y')
        )
    
    # Holding containers for the verts and edges to spin
    verts= bm_spring.verts[:]
    
    edges= bm_spring.edges[:]
    
    if use_square_end:  # Generate end and transition pitches
        
        if ang_end_pitch>0.0:   # Generate the end pitch
    
            # Generate end pitch
            ret=    bmesh.ops.spin(
                        bm_spring,
                        geom=   verts + edges,
                        angle=  ang_tot_end_pitch,
                        steps=  steps_end_pitch,
                        axis=   (0.0, 0.0, twist),
                        cent=   (0.0, 0.0, 0.0),
                        dvec=   (0.0, 0.0, P_ends/rot_steps) 
                    )

            geom_last= ret['geom_last']
            del ret
            
            # Extract the last circle of the spring to continue generation
            verts= [ele for ele in geom_last if isinstance(ele, \
                            bmesh.types.BMVert)]
            edges= [ele for ele in geom_last if isinstance(ele, \
                            bmesh.types.BMEdge) and ele.is_boundary]

        
        else:   # Extend down one pitch when ground end and no actual end pitch
    
            if use_ground_end:
            
                # Generate end pitch to be ground off
                ret=    bmesh.ops.spin(
                            bm_spring,
                            geom=   verts + edges,
                            angle=  2. * math.pi,
                            steps=  rot_steps,
                            axis=   (0.0, 0.0, twist),
                            cent=   (0.0, 0.0, 0.0),
                            dvec=   (0.0, 0.0, P_ends/rot_steps) 
                        )

                geom_last= ret['geom_last']
                del ret
                
                # Extract the last circle of the spring to continue generation
                verts= [ele for ele in geom_last if isinstance(ele, \
                                bmesh.types.BMVert)]
                edges= [ele for ele in geom_last if isinstance(ele, \
                                bmesh.types.BMEdge) and ele.is_boundary]

        # Now perform the transition angle
        for i in range(0,steps_trans):

            # Get the transition pitch.  Use a linear transition.
            P_curr= ( (P_main-P_ends)/steps_trans ) * (i+1) + P_ends
            
            # Perform the unit spin
            ret=    bmesh.ops.spin(
                        bm_spring,
                        geom=   verts + edges,
                        angle=  ang_step,
                        steps=  1,
                        axis=   (0.0, 0.0, twist),
                        cent=   (0.0, 0.0, 0.0),
                        dvec=   (0.0, 0.0, P_curr/rot_steps) 
                    )

            geom_last= ret['geom_last']
            del ret

            # Extract the last circle of the spring to the end cap pitch portion
            verts= [ele for ele in geom_last if isinstance(ele, \
                            bmesh.types.BMVert)]
            edges= [ele for ele in geom_last if isinstance(ele, \
                            bmesh.types.BMEdge) and ele.is_boundary]

    
    else:   # Plain ends, only use main pitch
    
        if use_ground_end:
    
            # Extend the spring spin down with the main pitch
            ret=    bmesh.ops.spin(
                        bm_spring,
                        geom=   verts + edges,
                        angle=  2. * math.pi,
                        steps=  rot_steps,
                        axis=   (0.0, 0.0, twist),
                        cent=   (0.0, 0.0, 0.0),
                        dvec=   (0.0, 0.0, P_main/rot_steps) 
                    )

            geom_last= ret['geom_last']
            del ret

            # Extract the last circle of the spring to the end cap pitch portion
            verts= [ele for ele in geom_last if isinstance(ele, \
                            bmesh.types.BMVert)]
            edges= [ele for ele in geom_last if isinstance(ele, \
                            bmesh.types.BMEdge) and ele.is_boundary]
    
    # Continue the spring spin with the main pitch
    ret=    bmesh.ops.spin(
                bm_spring,
                geom=   verts + edges,
                angle=  ang_tot_main,
                steps=  steps_main,
                axis=   (0.0, 0.0, twist),
                cent=   (0.0, 0.0, 0.0),
                dvec=   (0.0, 0.0, P_main/rot_steps) 
            )

    geom_last= ret['geom_last']
    del ret

    # Extract the last circle of the spring to the end cap pitch portion
    verts= [ele for ele in geom_last if isinstance(ele, \
                    bmesh.types.BMVert)]
    edges= [ele for ele in geom_last if isinstance(ele, \
                    bmesh.types.BMEdge) and ele.is_boundary]
    
    # Now add tops ends as needed
    if use_square_end:  # Generate end and transition pitches
        
        # Perform the transition from the main spring back to end spring
        for i in range(0,steps_trans):
            # Get the transition pitch.  Use a linear transition.
            P_curr= ( (P_ends-P_main)/steps_trans ) * (i+1) + P_main
            
            # Perform the unit spin
            ret=    bmesh.ops.spin(
                        bm_spring,
                        geom=   verts + edges,
                        angle=  ang_step,
                        steps=  1,
                        axis=   (0.0, 0.0, twist),
                        cent=   (0.0, 0.0, 0.0),
                        dvec=   (0.0, 0.0, P_curr/rot_steps) 
                    )

            geom_last= ret['geom_last']
            del ret

            # Extract the last circle of the spring to the end cap pitch portion
            verts= [ele for ele in geom_last if isinstance(ele, \
                            bmesh.types.BMVert)]
            edges= [ele for ele in geom_last if isinstance(ele, \
                            bmesh.types.BMEdge) and ele.is_boundary]

        if ang_end_pitch>0.0:   # Generate the end pitch
    
            # Generate end pitch
            ret=    bmesh.ops.spin(
                        bm_spring,
                        geom=   verts + edges,
                        angle=  ang_tot_end_pitch,
                        steps=  steps_end_pitch,
                        axis=   (0.0, 0.0, twist),
                        cent=   (0.0, 0.0, 0.0),
                        dvec=   (0.0, 0.0, P_ends/rot_steps) 
                    )

            geom_last= ret['geom_last']
            del ret
            
            # Extract the last circle of the spring to continue generation
            verts= [ele for ele in geom_last if isinstance(ele, \
                            bmesh.types.BMVert)]
            edges= [ele for ele in geom_last if isinstance(ele, \
                            bmesh.types.BMEdge) and ele.is_boundary]
    
    
        else:   # Extend down one pitch when ground end and no actual end pitch
    
            if use_ground_end:
            
                # Generate end pitch to be ground off
                ret=    bmesh.ops.spin(
                            bm_spring,
                            geom=   verts + edges,
                            angle=  2. * math.pi,
                            steps=  rot_steps,
                            axis=   (0.0, 0.0, twist),
                            cent=   (0.0, 0.0, 0.0),
                            dvec=   (0.0, 0.0, P_ends/rot_steps) 
                        )

                geom_last= ret['geom_last']
                del ret
                
                # Extract the last circle of the spring to continue generation
                verts= [ele for ele in geom_last if isinstance(ele, \
                                bmesh.types.BMVert)]
                edges= [ele for ele in geom_last if isinstance(ele, \
                                bmesh.types.BMEdge) and ele.is_boundary]

    else:   # Plain ends, only use main pitch
    
        if use_ground_end:
    
            # Continue the spring spin with the main pitch
            ret=    bmesh.ops.spin(
                        bm_spring,
                        geom=   verts + edges,
                        angle=  2. * math.pi,
                        steps=  rot_steps,
                        axis=   (0.0, 0.0, twist),
                        cent=   (0.0, 0.0, 0.0),
                        dvec=   (0.0, 0.0, P_main/rot_steps) 
                    )

            geom_last= ret['geom_last']
            del ret

            # Extract the last circle of the spring to the end cap pitch portion
            verts= [ele for ele in geom_last if isinstance(ele, \
                            bmesh.types.BMVert)]
            edges= [ele for ele in geom_last if isinstance(ele, \
                            bmesh.types.BMEdge) and ele.is_boundary]

    # Cap the final end
    bmesh.ops.contextual_create(
        bm_spring, 
        geom=       edges, 
        mat_nr=     0, 
        use_smooth= False
    )

    # Clean up memory
    del verts
    
    del edges
    
    # This is "merge by distance" to remove doubles
    bmesh.ops.remove_doubles(
        bm_spring,
        verts=      bm_spring.verts[:],
        dist=       0.0001
    )

    # Create an empty mesh and move the bmesh into it
    spring_msh = bpy.data.meshes.new(name_base_msh)
    bm_spring.to_mesh(spring_msh)
    bm_spring.free()

    # Place the mesh into an object and then into the collection
    spring = bpy.data.objects.new(name_base, spring_msh)
    bpy.context.collection.objects.link(spring)
        
    ######################################
    ####   CREATE GROUND END PLANES
    ######################################
    
    if use_ground_end:
    
        ## BOTTOM END
        bm_end_bot = bmesh.new()
        
        bmesh.ops.create_grid(
            bm_end_bot,
            x_segments = 1,
            y_segments = 1,
            size = 2.0*(r_id+d+4),
            matrix = mathutils.Matrix.Translation((0.0, 0.0, z_ground_base))
        )
            
        # Create an empty mesh and move the bmesh into it
        spring_end_bot_msh = bpy.data.meshes.new("spring_end_bot_msh")
        bm_end_bot.to_mesh(spring_end_bot_msh)
        bm_end_bot.free()

        # Place the mesh into an object and then into the collection
        spring_end_bot = bpy.data.objects.new("spring_end_bot", spring_end_bot_msh)
        bpy.context.collection.objects.link(spring_end_bot)

        # TOP END
        bm_end_top = bmesh.new()
        
        bmesh.ops.create_grid(
            bm_end_top,
            x_segments = 1,
            y_segments = 1,
            size = 2.0*(r_id+d+4),
            matrix = mathutils.Matrix.Translation((0.0, 0.0, z_ground_top))
        )   
            
        # Making this top have a z = -1.0 face normal for boolean difference
        bmesh.ops.reverse_faces(
            bm_end_top, 
            faces=          bm_end_top.faces[:], 
            flip_multires=  False
        )
        
        # Create an empty mesh and move the bmesh into it
        spring_end_top_msh = bpy.data.meshes.new("spring_end_top_msh")
        bm_end_top.to_mesh(spring_end_top_msh)
        bm_end_top.free()

        # Place the mesh into an object and then into the collection
        spring_end_top = bpy.data.objects.new("spring_end_top", spring_end_top_msh)
        bpy.context.collection.objects.link(spring_end_top)
        
        #####################################
        #####################################
        ## END BMESH OPERATIONS
        ## USE BPY.OPS TO INTERSECT BOOLEAN
        #####################################
        #####################################
        
        # Select the view layer context to use for joining
        view_layer = bpy.context.view_layer
        
        # Join the bottom end cap and spring
        lstObjs =[name_base, 'spring_end_bot']
                     
        blnd_joining(view_layer, lstObjs, 'DIFFERENCE', True, write_file, file_name)
        
        # Join the top end cap and spring
        lstObjs =[name_base, 'spring_end_top']
                     
        blnd_joining(view_layer, lstObjs, 'DIFFERENCE', True, write_file, file_name)
    
    return 0
    
# Call main if this file is executed as a script
if __name__ == '__main__':
    main()

