"""
PROGRAM:
blender_rotary_lobes.py

Copyright (C) 2024 Anders B. Thlenk

LICENSE:
This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.


DESCRIPTION:
Utilize Blender <https://www.blender.org/> to auto generate a pair of rotary lobes given various user inputs.  Refer to the "User Inputs" section of main() below.

"""

import bpy
import bmesh
import math
import mathutils

#########################################################
#########  Just error boxes
#########################################################

def e_mode_set(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with mode_set.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

def e_deslection(self,context):
    
    self.layout.scale_x = 4
    
    self.layout.scale_y = 3
    
    message= 'Issue with deselection.'
    
    self.layout.label(text=message, icon='INFO')
    
    return

#########################################################
#########  Starting Rotary Lobe Stuff
#########################################################


def geom_rotaryLobe(r_b,lobes, pts_per_lobe):
    """ Generate the perimeter of the lobe 
    
    Epicycloid Information from:
        Weisstein, Eric W. "Epicycloid." From MathWorld--A Wolfram Web Resource. 
        https://mathworld.wolfram.com/Epicycloid.html
        
        
    Hypocycloid Information from:
        Weisstein, Eric W. "Hypocycloid." From MathWorld--A Wolfram Web Resource. 
        https://mathworld.wolfram.com/Hypocycloid.html
    
    """
    
    n_cusps=    2*lobes
    b=          r_b/n_cusps
    
    offsetAngle=    math.pi/lobes
    delta_angle=    math.pi/(lobes*pts_per_lobe)
    angles=         [i*delta_angle for i in range(0,pts_per_lobe)]
    
    x= [j for j in range(0,int(n_cusps*pts_per_lobe))]
    y= [j for j in range(0,int(n_cusps*pts_per_lobe))]
    
    for loop in range(0,lobes):
        offsetEpi=      2*loop*offsetAngle
        offsetHypo=     (2*loop+1)*offsetAngle
        offsetIndex=    int(2*pts_per_lobe*loop)
    
        for i, angle in enumerate(angles):
            # Epicycloid
            x[offsetIndex+i]= (r_b+b)*math.cos(angle+offsetEpi) \
                                    - b*math.cos((r_b+b)*(angle+offsetEpi)/b)
            
            y[offsetIndex+i]= (r_b+b)*math.sin(angle+offsetEpi) \
                                    - b*math.sin((r_b+b)*(angle+offsetEpi)/b)
            
            # Hypocycloid
            x[offsetIndex+i+pts_per_lobe]= (r_b-b)*math.cos(angle+offsetHypo) \
                                            + b*math.cos((r_b-b)*(angle \
                                                +offsetHypo)/b)
                                                
            y[offsetIndex+i+pts_per_lobe]= (r_b-b)*math.sin(angle+offsetHypo) \
                                            - b*math.sin((r_b-b)*(angle \
                                                +offsetHypo)/b)
        
    return (x,y)
 
def main():
    
    #####################################
    ##########  USER INPUTS ###
    #####################################

    r_b=            20.         # Radius of base circle
    nLobes=         4
    lenLobe=        100.        # Length of a lobe
    angHelix_deg=   35.         # Helical angle, degrees
    gap=            0.20        # Gap between lobes
    steps_basis=    180.        # Number of rotation steps per 360 deg (2 pi)
    pts_per_lobe=   90          # Number of points per lobe
    
    name_base=      'lobes'     # Base name for Blender Objects

    ### NOTE:   No check of existing Blendeer object/mesh names done.
    ###         Confirm no conflicts or unexpected behavior may occur 

    write_file=     False
    file_name=      'absolute_file_path'
    
    #####################################
    ##########  INTERNAL CALCULATIONS ###
    #####################################
    
    name_lobe_lh=   name_base + '_lh'   # Blender object name for lh lobe
    name_lobe_rh=   name_base + '_rh'   # Blender object name for rh lobe
    
    if write_file:
        with open(file_name, mode='at') as fileout:
            fileout.write(f'\n\n{"*"*50}\n')
            fileout.write(f'**** Starting New Rotary Lobs ({name_base}) ****\n\n')
    
    # Miscellaneous
    name_lobe_msh_lh= name_lobe_lh+'_msh'
    name_lobe_msh_rh= name_lobe_rh+'_msh'
    
    # Calculations
    n_cusps=    2*nLobes                        # Number of cusps
    angHelix=   angHelix_deg*math.pi/180.         # Helical angle in radians
    rotEnd=     math.tan(angHelix)*lenLobe/r_b    # Total geometry rotation
    nSteps=     max(1,math.ceil(rotEnd*steps_basis/(2.*math.pi)))
    cd=         2.*r_b + gap                    # Center to center dimension
    
    lobe= geom_rotaryLobe(r_b,nLobes,pts_per_lobe)
    
    # Rotate to align with mating lobe
    angleRot= 2.*math.pi/(4.*nLobes)
    
    meshLobe_l_x= []
    meshLobe_l_y= []
    meshLobe_r_x= []
    meshLobe_r_y= []
    
    radius_max= -1.e+10        # Maximum radius of a single lobe
    radius_min= 1.e+10        # Minimum radius of a single lobe
    
    for i in range(0,len(lobe[0])):
        x_hold= lobe[0][i]*math.cos(angleRot) - lobe[1][i]*math.sin(angleRot)
        y_hold= lobe[0][i]*math.sin(angleRot) + lobe[1][i]*math.cos(angleRot)
        
        meshLobe_l_x.append(x_hold)
        meshLobe_l_y.append(y_hold)

        # Determine radius from x and y
        radius_chk= math.sqrt(x_hold*x_hold + y_hold*y_hold)
        
        # Update maximum if needed
        if radius_chk>radius_max:
            radius_max= radius_chk
            
        # Update minimum if needed
        if radius_chk<radius_min:
            radius_min= radius_chk
        
        # Make the second lobe
        if(nLobes%2==0):
            meshLobe_r_x.append(lobe[0][i]*math.cos(angleRot) \
                                + lobe[1][i]*math.sin(angleRot)+cd)
                                
            meshLobe_r_y.append(-lobe[0][i]*math.sin(angleRot) \
                                + lobe[1][i]*math.cos(angleRot) )
        else:
            meshLobe_r_x.append(meshLobe_l_x[i]+cd)
            meshLobe_r_y.append(meshLobe_l_y[i])

    
    # Maximum envelope dimensions
    width_max= 2.*(radius_max+r_b) + gap
    depth_max= 2.*radius_max
    d_max_shaft= 2.*radius_min
    
    ##############################################################
    ##############################################################
    ###                  Blender Part    #########################
    ##############################################################
    ##############################################################

    # Make a new BMesh
    bm_lobe= bmesh.new()

    # Add a circle for modifying vertices for gear
    # should return all geometry created, not just verts.
    bmesh.ops.create_circle(
                            bm_lobe,
                            cap_ends=   True,
                            radius=     r_b,
                            segments=   len(meshLobe_l_x)
                            )
        
    # Modify vertices to make the gear
    for vert in bm_lobe.verts:
        vert.co.x= meshLobe_l_x[vert.index]
        vert.co.y= meshLobe_l_y[vert.index]

    # Spin and deal with geometry on side 'a'
    edges_bot= bm_lobe.edges[:]
    geom_lobe= bm_lobe.verts[:] + edges_bot
    ret= bmesh.ops.spin(
                        bm_lobe,
                        geom=   geom_lobe,
                        angle=  rotEnd,
                        steps=  nSteps,
                        axis=   (0.0, 0.0, 1.0),  # Right handed (i.e, z= +1)
                        cent=   (0.0, 0.0, 0.0),
                        dvec=   (0.0, 0.0, lenLobe/nSteps) 
                        )
    
    edges_top= [ele for ele in ret["geom_last"]
                   if isinstance(ele, bmesh.types.BMEdge)]
    del ret

    # This closes the extruded edges at the top to a face
    bmesh.ops.edgeloop_fill(
                            bm_lobe,
                            edges=  edges_top
                            )
                        
    # This is "merge by distance" to remove doubles
    bmesh.ops.remove_doubles(
                            bm_lobe,
                            verts=  bm_lobe.verts[:],
                            dist=   0.0001
                            )

    # Done with creating the mesh, simply link it into the scene so we can see it

    # Finish up, write the bmesh into a new mesh
    msh_lobe= bpy.data.meshes.new(name_lobe_msh_rh)
    bm_lobe.to_mesh(msh_lobe)
    bm_lobe.free()

    # Add the mesh to the scene
    obj_lobe= bpy.data.objects.new(name_lobe_rh, msh_lobe)
    bpy.context.collection.objects.link(obj_lobe)

    ### Second lobe, left handed
    # Make a new BMesh
    bm_lobe= bmesh.new()

    # Add a circle for modifying vertices for gear
    # should return all geometry created, not just verts.
    bmesh.ops.create_circle(
                            bm_lobe,
                            cap_ends= True,
                            radius= r_b,
                            segments= len(meshLobe_r_x)
                            )
        
    # Modify vertices to make the gear
    for vert in bm_lobe.verts:
        vert.co.x= meshLobe_r_x[vert.index]
        vert.co.y= meshLobe_r_y[vert.index]

    # Spin and deal with geometry on side 'a'
    edges_bot= bm_lobe.edges[:]
    geom_lobe= bm_lobe.verts[:] + edges_bot
    ret= bmesh.ops.spin(
                        bm_lobe,
                        geom=   geom_lobe,
                        angle=  rotEnd,
                        steps=  nSteps,
                        axis=   (0.0, 0.0, -1.0),  # Left handed (i.e, z= -1)
                        cent=   (cd, 0.0, 0.0),
                        dvec=   (0.0, 0.0, lenLobe/nSteps) 
                        )

    edges_top= [ele for ele in ret["geom_last"]
                   if isinstance(ele, bmesh.types.BMEdge)]
    del ret

    # This closes the extruded edges at the top to a face
    bmesh.ops.edgeloop_fill(
                            bm_lobe,
                            edges=  edges_top
                            )
    
    # This is "merge by distance" to remove doubles
    bmesh.ops.remove_doubles(
                            bm_lobe,
                            verts=  bm_lobe.verts[:],
                            dist=   0.0001
                            )

    # Done with creating the mesh, simply link it into the scene so we can see it

    # Finish up, write the bmesh into a new mesh
    msh_lobe= bpy.data.meshes.new(name_lobe_msh_lh)
    bm_lobe.to_mesh(msh_lobe)
    bm_lobe.free()

    # Add the mesh to the scene
    obj_lobe= bpy.data.objects.new(name_lobe_lh, msh_lobe)
    bpy.context.collection.objects.link(obj_lobe)

    ##############################
    ##############################
    ####### USE BPY TO SET LH LOBE CENTER
    ##############################
    ##############################
    
    # Set the veiew layer to get a context
    view_layer= bpy.context.view_layer
    view_layer.objects.active= bpy.data.objects[name_lobe_lh]
    
    # Ensure in OBJECT mode
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(
            mode= 'OBJECT',
            toggle= False
            )
    else:

        bpy.context.window_manager.popup_menu(
            e_mode_set, 
            title=  "Oops, Sorry! Aligning Lobes:", 
            icon=   'ERROR'
        )        

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with mode_set.\n#####\n')

        
    # Deselect everything for a clean slate
    if bpy.ops.object.select_all.poll():
        bpy.ops.object.select_all(action='DESELECT')
    else:

        bpy.context.window_manager.popup_menu(
            e_deslection, 
            title=  "Oops, Sorry! Aligning Lobes:", 
            icon=   'ERROR'
        )

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with deselection.\n#####\n')
    
    # Set the cursor to the center of the output gear
    bpy.context.scene.cursor.location[0]= cd
    bpy.context.scene.cursor.location[1]= 0.0
    bpy.context.scene.cursor.location[2]= 0.0

    # Set the output gear as the selected object
    bpy.data.objects[name_lobe_lh].select_set(state=True)
    
    # Set origin to cursor
    bpy.ops.object.origin_set(
                                type=   'ORIGIN_CURSOR',
                                center= 'MEDIAN'
                            )
    
    # Deselect everything for a clean slate
    if bpy.ops.object.select_all.poll():
        bpy.ops.object.select_all(action='DESELECT')
    else:

        bpy.context.window_manager.popup_menu(
            e_deslection, 
            title=  "Oops, Sorry! Aligning Lobes:", 
            icon=   'ERROR'
        )

        if write_file:
            with open(file_name, mode='at') as fileout:
                fileout.write('\n####\nIssue with deselection.\n#####\n')

    if write_file:
        with open(file_name, mode='at') as fileout:
            ################# Blender output information ############
            fileout.write(f'{"#"*8} New Rotary Lobe Run = {name_base} {"#"*8}\n\n')
            fileout.write(f'USER INPUTS:\n')
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Radius of Base Circle =', r_b))
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Number of Lobes =', nLobes))
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Length of Lobes =', lenLobe))
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Helical Angle (deg) =', angHelix_deg))
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Gap Between Lobes =', gap))
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Steps Per 360 deg =', steps_basis))
            fileout.write('\t{0:<30}{1:>12.4f}\n\n'.format('Points Per Lobe Face =', pts_per_lobe))
            
            fileout.write(f'INTERNAL CALCULATIONS:\n')
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Number of Cusps =', n_cusps))
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Center Distance =', cd))
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Overall Width =', width_max))
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Overall Depth =', depth_max))
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Maximum Shaft Diameter =', d_max_shaft))
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Maximum Radius =', radius_max))
            fileout.write('\t{0:<30}{1:>12.4f}\n'.format('Minimum Radius =', radius_min))
            fileout.write('\t{0:<30}{1:>12.4f}\n\n'.format('Steps Per Spin =', nSteps))

            fileout.write(f'********** End Rotary Lobe Data = {name_base} ************\n')

            fileout.write(f'\n******** Ending Rotary Lobe Output ({name_base}) *******\n')
            fileout.write(f'**********************************************************\n\n')

    return 0
    
# Call main if this file is executed as a script
if __name__ == '__main__':
    main()    
