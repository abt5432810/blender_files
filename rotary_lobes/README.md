# Rotary Lobes

## Description
This file allows for the generation of straight or helical rotary lobes with a user input number of lobes.  The rotary lobes are generated in pairs.  When a helical angle is provided, one will have right hand spin while the other will have left hand spin.  For example,

<div align="center">
  <img src="snapshots/rl_general.png" alt="Screenshot_general" width="378" height="381"/>
</div>

The input of [Blender](https://www.blender.org/) object/mesh names and writing of output information to a file is as described in the [main README.md of this repository](/README.md).

## Usage

The following provides a brief description of the various user inputs availalbe.  As described in the [main README.md of this repository](/README.md), this is not a [Blender](https://www.blender.org/) Add-on and instead is utilized through the Scripting Workspace.

### Base Circle (r_b)

This sets the radius of the base circle.  The base circle sets the perimeter location where the change from the [epicycloids](https://mathworld.wolfram.com/Epicycloid.html) to the [hypocycloids](https://mathworld.wolfram.com/Hypocycloid.html) occur.  It is not the interior circle which contacts the base of the [hypocycloids](https://mathworld.wolfram.com/Hypocycloid.html).  This interior circle dimension, which can be used to understand how large a connection piece can be (e.g, a shaft), is part of the outputs written to file if `write_file` is set to `True`.  The following provides a visual of the base circle relative to a generated lobe.

<div align="center">
  <img src="snapshots/rl_base_circle.png" alt="Screenshot_base_circle" width="462" height="380"/>
</div>

### Number of Lobes (nLobes)

This sets the number of lobes.  This is the number of [epicycloids](https://mathworld.wolfram.com/Epicycloid.html) the rotary lobe will have.  An equivalent number of [hypocycloids](https://mathworld.wolfram.com/Hypocycloid.html) will be generated.  Therefore, this value is not the total number of both [epicycloids](https://mathworld.wolfram.com/Epicycloid.html) and [hypocycloids](https://mathworld.wolfram.com/Hypocycloid.html).  The following demonstrates this visually.

<div align="center">
  <img src="snapshots/rl_nlobes.png" alt="Screenshot_number_of_lobes" width="724" height="230" />
</div>

### Overall Height of the Rotary Lobes (lenLobe)

This value sets the total height of the rotary lobes.  As discussed in the [main README.md to this repository](/README.md), without any modifications in [Blender](https://www.blender.org/), the units will be meters.

### Helical Angle (angHelix_deg)

This sets the amount of twist or spin of the rotary lobes.  The input value is to be in degrees (not radians).  Setting a value of zero will generate a straight rotary lobe (i.e, no spin).  Typically, the maximum value is 45 degrees, but play around with it to see what happens at larger values.  The following shows rotary lobes with varying levels of helical angle.

<div align="center">
  <img src="snapshots/rl_angle_helix.png" alt="Screenshot_helix_angle" width="701" height="283"/>
</div>

### Gap (gap)

This values sets additional center-to-center distance between the pair of rotary lobes.  If this is set to zero, the rotary lobes will just touch each other.  This gap can be used to set some tolerance when using these rotary lobes as a sub-assembly to a larger overall component (e.g, a [Roots blower](https://en.wikipedia.org/wiki/Roots_blower)).

### Rotation Increments (steps_basis)

When a helical angle greater than zero is provided, this indicates how many steps are to be used when making the rotations.  The higher the number of steps, the higher the model detail and the more time and memory it will take to generate the rotary lobes.  The input value is the number of steps in a full rotation (i.e, 360°).  For example, if a value of 180 is used, this will generate a detail every 2° of rotation.

### Lobe Detail (pts_per_lobe)

This is another parameter which allows the user to modify the detail of the model.  This is the number of points that will be used to generate a single lobe (e.g, epicycloid or hypocycloid).  This value is used in generating both the epicycloids and the hypocycloids.  The following demonstrates this visually.

<div align="center">
  <img src="snapshots/rl_pts_per_lobe.png" alt="Screenshot_number_of_lobes" width="446" height="418"/>
</div>

## The End

That summaries the user inputs for this file.  Hopefully it will provide some value in your projects.
